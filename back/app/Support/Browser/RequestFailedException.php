<?php

namespace App\Support\Browser;

use RuntimeException;

class RequestFailedException extends RuntimeException
{
    /**
     * @var Response
     */
    private $response;

    /**
     * @var Request
     */
    private $request;

    public function __construct($message, Request $request, Response $response)
    {
        $this->response = $response;
        $this->request = $request;

        $isJson = (bool)json_decode($response->getContent());

        $message = $message."\n".'- Request:';
        $message = $message."\n".'-- '.strtoupper($request->getMethod()) .' '. $request->getRoute();
        $message = $message."\n".'-- Header: '.json_encode($request->getHeaders());
        $message = $message."\n".'-- Body: '.json_encode($request->getBody());

        $content = $isJson ? $response->getContent() : 'Content is too large. Please see logs for more details.';

        $message = $message."\n".'- Response:'."\n".$content;

        parent::__construct($message, $response->getStatusCode());
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}
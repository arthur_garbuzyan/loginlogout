<?php

namespace App\Support\Browser;

class Session
{
    /**
     * @var string
     */
    private $token;
    public function getToken(): ?string { return $this->token; }

    /**
     * @var array
     */
    private $data = [];
    public function getData(): array { return $this->data; }

    /**
     * @param string $token
     * @param array $data
     */
    public function __construct(string $token, array $data = [])
    {
        $this->token = $token;
        $this->data = $data;
    }
}
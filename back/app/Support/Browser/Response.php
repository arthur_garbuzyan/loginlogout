<?php

namespace App\Support\Browser;

use Psr\Http\Message\ResponseInterface;

class Response
{
    const HTTP_OK = 200;
    const HTTP_NO_CONTENT = 204;

    const HTTP_BAD_REQUEST = 400;
    const HTTP_NOT_FOUND = 404;

    const UNPROCESSABLE_ENTRY = 422;
    const FORBIDDEN = 403;

    /**
     * @var ResponseInterface
     */
    private $psrResponse;

    /**
     * @var array
     */
    private $array;

    /**
     * @var string
     */
    private $content;

    /**
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->psrResponse = $response;
    }

    /**
     * @return array
     */
    public function getBody()
    {
        $data = $this->toArray();

        return array_take($data ?: [], 'data', $data);
    }

    /**
     * @return array
     */
    public function getMeta()
    {
        $data = $this->toArray();
        return array_take($data, 'meta', []);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        if (!$this->array) {
            $this->array = json_decode($this->getContent(), true);
        }

        return $this->array;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->psrResponse->getStatusCode();
    }

    /**
     * @return bool
     */
    public function isBlank()
    {
        return $this->getContent() === '';
    }

    /**
     * @return string
     */
    public function getContent()
    {
        if (!$this->content) {
            $this->content = $this->psrResponse->getBody()->getContents();
        }

        return $this->content;
    }
}
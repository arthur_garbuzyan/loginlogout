<?php

namespace App\Support\Browser;

use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use RuntimeException;

class Request
{
    const AUTH_HEADER = 'Token';

    /**
     * @var Method
     */
    private $method;
    public function setMethod(string $method): void { $this->method = new Method($method); }
    public function getMethod(): ?Method { return $this->method; }

    /**
     * @var string
     */
    private $route;
    public function getRoute(): ?string { return $this->route; }
    public function setRoute(string $route): void
    {
        if(starts_with($route, '/')) {
            $route = str_replace_first('/', '', $route);
        }

        $this->route = $route;
    }

    /**
     * @var array
     */
    private $body = [];
    public function setBody(array $body): void { $this->body = $body; }
    public function getBody(): array { return $this->body; }

    /**
     * @var array
     */
    private $options = [];
    public function setOptions(array $options): void { $this->options = $options; }
    public function getOptions(): array { return $this->options; }

    /**
     * @var array
     */
    private $includes = [];
    public function setIncludes(array $includes): void { $this->includes = $includes; }
    public function getIncludes(): array { return $this->includes; }

    /**
     * @var string
     */
    private $auth;
    public function setAuth(string $auth): void { $this->auth = $auth; }
    public function getAuth(): ?string { return $this->auth; }

    /**
     * @var array
     */
    private $files = [];
    public function setFiles(array $files): void { $this->files = $files; }
    public function getFiles(): array { return $this->files; }

    /**
     * @var array
     */
    private $parameters = [];
    public function setParameters(array $parameters): void { $this->parameters = $parameters; }
    public function getParameters(): array { return $this->parameters; }

    /**
     * @var array
     */
    private $headers = [];
    public function setHeaders(array $headers): void { $this->headers = $headers; }

    /**
     * @param string $method
     * @param string $route
     */
    public function __construct(string $method, string $route)
    {
        if (!Method::has($method)) {
            throw new RuntimeException('Unknown request method.');
        }

        $this->setMethod($method);
        $this->setRoute($route);
    }

    /**
     * @param string $route
     * @param array $parameters
     * @return Request
     */
    public static function get($route, array $parameters = []): Request
    {
        $request = new static(Method::GET, $route);
        $request->setParameters($parameters);
        return $request;
    }

    /**
     * @param string $route
     * @param array $data
     * @param array $files
     * @return Request
     */
    public static function post($route, array $data = [], $files = []): Request
    {
        $request = new static(Method::POST, $route);
        $request->setBody($data);
        $request->setFiles($files);
        return $request;
    }

    /**
     * @param string $route
     * @param array $data
     * @return Request
     */
    public static function put($route, array $data = []): Request
    {
        $request = new static(Method::PUT, $route);
        $request->setBody($data);
        return $request;
    }

    /**
     * @param string $route
     * @param array $parameters
     * @return Request
     */
    public static function delete($route, array $parameters = []): Request
    {
        $request = new static(Method::DELETE, $route);
        $request->setParameters($parameters);
        return $request;
    }

    /**
     * @param string $specString
     * @return Request
     */
    public static function createFromSpecString(string $specString): Request
    {
        $matches = [];

        if (preg_match('/^(GET|POST|PATCH|PUT|DELETE)\s+(.*)$/iu', $specString, $matches)) {
            return new static($matches[1], $matches[2]);
        }

        throw new RuntimeException("Could not create a request object from incorrect spec string [$specString].");
    }

    /**
     * @return GuzzleRequest
     */
    public function toGuzzle(): GuzzleRequest
    {
        return new GuzzleRequest(
            $this->getMethod(),
            $this->getUri(),
            $this->getHeaders(),
            $this->prepareBody()
        );
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        $headers = $this->headers;

        if ($this->options) {
            $headers['Options'] = json_encode($this->options);
        }

        if ($this->includes) {
            $headers['Include'] = implode(',', $this->includes);
        }

        if ($this->auth){
            $headers[static::AUTH_HEADER] = $this->auth;
        }

        return $headers;
    }

    /**
     * @return string
     */
    private function getUri(): string
    {
        return $this->route . '?'. http_build_query($this->parameters);
    }

    /**
     * @return MultipartStream|string
     */
    private function prepareBody()
    {
        if ($files = $this->getFiles()) {

            $elements = [];

            foreach (array_dot($this->getBody()) as $name => $item){
                $elements[] = [
                    'name' => $this->prepareName($name),
                    'contents' => $item
                ];
            }

            foreach (array_dot($files) as $name => $item){
                $elements[] = [
                    'name' => $this->prepareName($name),
                    'contents' => fopen($item, 'r')
                ];
            }

            return new MultipartStream($elements);
        } else {
            return json_encode($this->body);
        }
    }

    /**
     * @param string $name
     * @return string
     */
    private function prepareName(string $name): string
    {
        $name = explode('.', $name);

        $first = array_shift($name);

        $name = array_map(function($v){
            return '['.$v.']';
        }, $name);

        array_unshift($name, $first);

        return implode('', $name);
    }
}
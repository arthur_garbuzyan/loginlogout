<?php

namespace App\Support\Browser;

use ArrayAccess;
use RuntimeException;

class SessionManager implements ArrayAccess
{
    /**
     * @var array
     */
    private $sessions = [];

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->sessions);
    }

    /**
     * @param mixed $offset
     * @return Session
     */
    public function offsetGet($offset)
    {
        return $this->sessions[$offset];
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        if (!($value instanceof Session)) {
            $expected = Session::class;
            $got = get_class($value);
            throw new RuntimeException("Expected '{$expected}', got '{$got}'.");
        }

        $this->sessions[$offset] = $value;
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->data);
    }
}
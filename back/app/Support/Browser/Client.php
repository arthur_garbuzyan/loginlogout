<?php

namespace App\Support\Browser;

use GuzzleHttp\Client as GuzzleClient;
use RuntimeException;

class Client
{
    /**
     * @var GuzzleClient
     */
    private $guzzleClient;

    /**
     * @var SessionManager
     */
    private $sessions;

    /**
     * @var string
     */
    private $baseUri;

    /**
     * @var Session
     */
    private $nextSession = null;

    /**
     * @var array
     */
    private $interceptors = [
        'before' => [],
        'after' => []
    ];

    /**
     * @param array $configs
     */
    public function __construct(array $configs = [])
    {
        $this->sessions = new SessionManager();

        $this->baseUri = array_take($configs, 'base_uri');

        if (!$this->baseUri) {
            throw new RuntimeException('Undefined base url.');
        }

        $this->interceptors['before'] = array_get($configs, 'interceptors.before', []);
        $this->interceptors['after'] = array_get($configs, 'interceptors.after', []);

        $this->guzzleClient = new GuzzleClient([
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'exceptions' => false
            ] + $configs);
    }

    /**
     * @param string $actor
     * @return Client
     */
    public function as(string $actor): self
    {
        if (!$this->sessions[$actor]) {
            throw new RuntimeException("No Session found for '{$actor}'.");
        }

        $this->nextSession = $this->sessions[$actor];
        return $this;
    }

    /**
     * @param Request $request
     * @param bool $stopOnFail
     * @param string $error
     * @return Response
     */
    public function send(Request $request, $stopOnFail = true, string $error = 'Request failed.'): Response
    {
        if ($this->nextSession && !$request->getAuth()) {
            $request->setAuth($this->nextSession->getToken());
            $this->nextSession = null;
        }

        foreach ($this->interceptors['before'] as $interceptor) {
            $request = call_user_func([$interceptor, $request]);
        }

        $response = new Response(
            $this->guzzleClient->send($request->toGuzzle())
        );

        if ($stopOnFail && $response->getStatusCode() >= 300) {
            throw new RequestFailedException(
                $error,
                $request,
                $response
            );
        }

        foreach ($this->interceptors['after'] as $interceptor) {
            $response = call_user_func($interceptor, $request, $response);
        }

        return $response;
    }

    /**
     * @param string $name
     * @param Session $session
     */
    public function addSession(string $name, Session $session): void
    {
        $this->sessions[$name] = $session;
    }

    /**
     * @param SessionManager $sessions
     */
    public function setSessions(SessionManager $sessions): void
    {
        $this->sessions = $sessions;
    }

    /**
     * @return SessionManager
     */
    public function getSessions(): SessionManager
    {
        return $this->sessions;
    }
}
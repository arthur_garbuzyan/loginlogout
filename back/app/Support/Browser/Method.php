<?php

namespace App\Support\Browser;

use InscopeRest\Enum\Enum;

class Method extends Enum
{
    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
}
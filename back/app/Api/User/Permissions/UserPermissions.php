<?php

namespace App\Api\User\Permissions;

use InscopeRest\Permissions\AbstractActionsPermissions;

class UserPermissions extends AbstractActionsPermissions
{
    /**
     * @return array
     */
    protected function permissions(): array
    {
        return [
            'index' => 'all',
            'show' => 'all',
            'store' => 'admin',
            'signUp' => 'all',
            'update' => 'all',
            'delete' => ['owner', 'admin']
        ];
    }
}
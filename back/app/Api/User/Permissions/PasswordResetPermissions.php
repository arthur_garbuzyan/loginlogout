<?php

namespace App\Api\User\Permissions;

use InscopeRest\Permissions\AbstractActionsPermissions;

class PasswordResetPermissions extends AbstractActionsPermissions
{
    /**
     * @return array
     */
    protected function permissions(): array
    {
        return [
            'sendNotification' => 'all',
            'changePassword' => 'all'
        ];
    }
}
<?php

namespace App\Api\User\Processors;

use App\Api\Support\BaseProcessor;
use App\Api\Support\Validation\Rules\File;
use App\Core\User\Enums\Gender;
use App\Core\User\Persistables\UserPersistable;
use InscopeRest\Validation\Rules\BooleanCast;
use InscopeRest\Validation\Rules\Enum;

class UserProcessor extends BaseProcessor
{
    protected function configuration(): array
    {
        return [
            'email' => 'string',
            'password' => 'string',
            'confirmPassword' => 'string',

            'firstName' => 'string',
            'subject' => 'string',
            'reason' => 'string',
            'lastName' => 'string',

            'gender' => new Enum(Gender::class),

            'avatar' => new File(),

            'hasAgreedToTerms' => new BooleanCast(true),
        ];
    }

    /**
     * @return UserPersistable
     */
    public function createPersistable(): UserPersistable
    {
        return $this->populate(new UserPersistable());
    }
}
<?php

namespace App\Api\User\Processors;

use App\Api\Support\Searchable\BaseSearchableProcessor;
use App\Api\Support\Searchable\SortableTrait;
use App\Core\Support\Criteria\Constraint;
use App\Core\Support\Criteria\Sorting\Sortable;
use App\Core\User\Enums\Gender;
use App\Core\User\Services\UserService;
use InscopeRest\Validation\Rules\Enum;

class UserSearchableProcessor extends BaseSearchableProcessor
{
    use SortableTrait;

    /**
     * @return array
     */
    protected function configuration(): array
    {
        return [
            'filter' => [
                'email' => Constraint::SIMILAR,
                'loggedOut' => Constraint::SIMILAR,
                'loggedIn' => Constraint::SIMILAR,
                'gender' => [
                    'constraint' => Constraint::EQUAL,
                    'type' => new Enum(Gender::class)
                ],
                'createdAt' => [
                    'constraint' => Constraint::BETWEEN,
                    'type' => 'dateinterval'
                ],
            ],
            'search' => [
                'email' => Constraint::EQUAL,

            ]
        ];
    }

    /**
     * @param Sortable $sortable
     * @return bool
     */
    protected function isResolvable(Sortable $sortable): bool
    {
        /**
         * @var UserService $userService
         */
        $userService = $this->container->make(UserService::class);
        return $userService->canResolveSortable($sortable);
    }
}
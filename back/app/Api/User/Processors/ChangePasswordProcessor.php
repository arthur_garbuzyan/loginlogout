<?php

namespace App\Api\User\Processors;

use App\Api\Support\BaseProcessor;
use App\Core\User\Objects\PasswordsPair;

class ChangePasswordProcessor extends BaseProcessor
{
    /**
     * @return array
     */
    protected function configuration(): array
    {
        return [
           'token' => 'string',
           'password' => 'string',
           'confirmPassword' => 'string'
        ];
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->getRequest()->get('token');
    }

    /**
     * @return PasswordsPair
     */
    public function getPasswordsPair()
    {
        return $this->populate(new PasswordsPair());
    }
}
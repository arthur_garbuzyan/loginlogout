<?php

namespace App\Api\User\Processors;

use App\Api\Support\BaseProcessor;

class PasswordResetProcessor extends BaseProcessor
{
    /**
     * @return array
     */
    protected function configuration(): array
    {
        return [
            'email' => 'string'
        ];
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->getRequest()->get('email');
    }
}
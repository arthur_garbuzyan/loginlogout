<?php

namespace App\Api\User\Protectors;

use App\Api\Shared\Protectors\AuthProtector;
use App\Core\User\Entities\User;
use App\Core\Session\Entities\Session;

class UserProtector extends AuthProtector
{
    /**
     * @return bool
     */
    public function grants() : bool
    {
        if (!parent::grants()) {
            return false;
        }

        /**
         * @var Session $session
         */
        $session = $this->container->make(Session::class);

        return $session->getUser() instanceof User;
    }
}
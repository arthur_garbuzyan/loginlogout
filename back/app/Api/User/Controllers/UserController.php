<?php

namespace App\Api\User\Controllers;

use App\Api\Support\DefaultPaginatorAdapter;
use App\Api\User\Processors\UserProcessor;
use App\Api\Support\BaseController;
use App\Api\User\Processors\UserSearchableProcessor;
use App\Core\Shared\Options\DefaultFetchOptions;
use App\Core\User\Services\UserService;
use Illuminate\Http\Response;
use InscopeRest\Koala\Pagination\PaginationOptions;

class UserController extends BaseController
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $userService
     */
    public function initialize(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param UserSearchableProcessor $processor
     * @return Response
     */
    public function index(UserSearchableProcessor $processor): Response
    {
        $adapter = new DefaultPaginatorAdapter([
            'getAll' => function ($page, $perPage) use ($processor) {
                $options = new DefaultFetchOptions();
                $options->setPagination(new PaginationOptions($page, $perPage));
                $options->setCriteria($processor->getCriteria());
                $options->setSortables($processor->createSortables());
                return $this->userService->getAll($options);
            },
            'getTotal' => function () use ($processor) {
                return $this->userService->getTotal($processor->getCriteria());
            }
        ]);

        return $this->resource->makeAll(
            $this->paginator($adapter),
            $this->transformer()
        );
    }

    /**
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        return $this->resource->make(
            $this->userService->get($id),
            $this->transformer()
        );
    }

    /**
     * @param UserProcessor $processor
     * @return Response
     */
    public function store(UserProcessor $processor): Response
    {
        return $this->resource->make(
            $this->userService->create($processor->createPersistable()),
            $this->transformer()
        );
    }

    /**
     * @param UserProcessor $processor
     * @return Response
     */
    public function signUp(UserProcessor $processor): Response
    {
        return $this->resource->make(
            $this->userService->create($processor->createPersistable(), true),
            $this->transformer()
        );
    }

    /**
     * @param int $id
     * @param UserProcessor $processor
     * @return Response
     */
    public function update(int $id, UserProcessor $processor): Response
    {
        $this->userService->update($id, $processor->createPersistable());
        return $this->resource->blank();
    }

    /**
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        $this->userService->delete($id);
        return $this->resource->blank();
    }

    /**
     * @param UserService $userService
     * @param int $id
     * @return bool
     */
    public static function verifyAction(UserService $userService, int $id): bool
    {
        return $userService->exists($id);
    }
}
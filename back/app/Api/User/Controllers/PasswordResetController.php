<?php

namespace App\Api\User\Controllers;

use App\Api\Support\BaseController;
use App\Api\User\Processors\PasswordResetProcessor;
use App\Api\User\Processors\ChangePasswordProcessor;
use App\Core\User\Exceptions\InvalidTokenException;
use App\Core\User\Exceptions\UserNotFoundException;
use App\Core\User\Services\PasswordResetService;
use InscopeRest\Validation\Error;
use InscopeRest\Validation\ErrorsThrowableCollection;

class PasswordResetController extends BaseController
{
    /**
     * @var PasswordResetService
     */
    protected $passwordResetService;

    /**
     * @param PasswordResetService $passwordResetService
     */
    public function initialize(PasswordResetService $passwordResetService)
    {
        $this->passwordResetService = $passwordResetService;
    }

    /**
     * @param PasswordResetProcessor $processor
     * @return \Illuminate\Http\Response
     */
    public function sendNotification(PasswordResetProcessor $processor)
    {
        try {
            $this->passwordResetService->sendNotification($processor->getEmail());
        } catch (UserNotFoundException $exception) {
            $this->throwEmailError('user-not-found', $exception->getMessage());
        }

        return $this->resource->blank();
    }

    /**
     * @param ChangePasswordProcessor $processor
     * @return \Illuminate\Http\Response
     */
    public function changePassword(ChangePasswordProcessor $processor)
    {
        try {
            $this->passwordResetService->changePassword(
                $processor->getToken(),
                $processor->getPasswordsPair()
            );
        } catch (InvalidTokenException $exception) {
            $this->throwTokenError('invalid-token', $exception->getMessage());
        }

        return $this->resource->blank();
    }

    /**
     * @param string $identifier
     * @param string $message
     */
    private function throwEmailError($identifier, $message)
    {
        $errors = new ErrorsThrowableCollection();

        $errors['email'] = new Error($identifier, $message);

        throw $errors;
    }

    /**
     * @param string $identifier
     * @param string $message
     */
    private function throwTokenError($identifier, $message)
    {
        $errors = new ErrorsThrowableCollection();

        $errors['token'] = new Error($identifier, $message);

        throw $errors;
    }
}
<?php

namespace App\Api\User\Routes;

use App\Api\User\Controllers\PasswordResetController;
use InscopeRest\Routing\RouteInterface;
use Illuminate\Contracts\Routing\Registrar;

class PasswordReset implements RouteInterface
{
    /**
     * @param Registrar $registrar
     */
    public function register(Registrar $registrar) : void
    {
        $registrar->post('users/password/reset', PasswordResetController::class.'@sendNotification');
        $registrar->post('users/password/change', PasswordResetController::class.'@changePassword');
    }
}
<?php

namespace App\Api\User\Routes;

use App\Api\User\Controllers\UserController;
use Illuminate\Contracts\Routing\Registrar;
use InscopeRest\Routing\RouteInterface;

class Users implements RouteInterface
{
    /**
     * @param Registrar $registrar
     */
    public function register(Registrar $registrar): void
    {
        $registrar->get('users', UserController::class.'@index');
        $registrar->get('users/{idOrSiteName}', UserController::class.'@show');
        $registrar->post('users', UserController::class .'@store');
        $registrar->post('users/sign-up', UserController::class .'@signUp');
        $registrar->put('users/{idOrSiteName}', UserController::class.'@update');
        $registrar->delete('users/{idOrSiteName}', UserController::class.'@delete');
    }
}
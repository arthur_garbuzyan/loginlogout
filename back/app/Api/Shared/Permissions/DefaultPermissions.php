<?php

namespace App\Api\Shared\Permissions;

use InscopeRest\Permissions\AbstractActionsPermissions;

class DefaultPermissions extends AbstractActionsPermissions
{
    /**
     * @return array
     */
    protected function permissions() : array
    {
        return [
            'server' => 'all',
            'api' => 'all'
        ];
    }
}
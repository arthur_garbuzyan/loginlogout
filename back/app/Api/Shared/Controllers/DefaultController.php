<?php

namespace App\Api\Shared\Controllers;

use App\Api\Support\BaseController;

class DefaultController extends BaseController
{
    /**
     * @return string
     */
    public function server()
    {
        $appName = config('app.name');
        return "You've reached the {$appName} server.";
    }

    /**
     * @return string
     */
    public function api()
    {
        $appName = config('app.name');
        return "You've reached the {$appName} API server.";
    }
}
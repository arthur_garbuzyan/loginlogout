<?php

namespace App\Api\Shared\Protectors;

use App\Core\Session\Entities\Session;
use App\Core\User\Entities\User;
use InscopeRest\Permissions\OptionsExpectableInterface;
use InscopeRest\Permissions\OptionsExpectableTrait;
use Illuminate\Http\Request;

class OwnerProtector extends AuthProtector implements OptionsExpectableInterface
{
    use OptionsExpectableTrait;

    /**
     * @return bool
     */
    public function grants() : bool
    {
        if (! parent::grants()) {
            return false;
        }

        /**
         * @var Session $session
         */
        $session = $this->container->make(Session::class);

        if (!($session->getUser() instanceof User)) {
            return false;
        }

        return $session->getUser()->getId() == $this->getOwnerId();
    }

    /**
     *
     * @return int|null
     */
    protected function getOwnerId() : ?int
    {
        /**
         * @var Request $request
         */
        $request = $this->container->make('request');

        $parameters = array_values($request->route()->parameters());

        $index = array_take($this->getOptions(), 'index', 0);

        return $parameters[$index];
    }
}
<?php

namespace App\Api\Shared\Protectors;

use App\Core\Session\Entities\Session;
use InscopeRest\Permissions\ProtectorInterface;
use DateTime;
use Illuminate\Contracts\Container\Container;

class AuthProtector implements ProtectorInterface
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return bool
     */
    public function grants() : bool
    {
        /**
         * @var Session $session
         */
        $session = $this->container->make(Session::class);

        return $session->getId() !== null && $session->getExpireAt() > new DateTime();
    }
}
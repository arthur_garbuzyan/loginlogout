<?php

namespace App\Api\Administrator\Protectors;

use App\Api\Shared\Protectors\AuthProtector;
use App\Core\Administrator\Entities\Administrator;
use App\Core\Session\Entities\Session;

class AdministratorProtector extends AuthProtector
{
    /**
     * @return bool
     */
    public function grants() : bool
    {
        if (!parent::grants()) {
            return false;
        }

        /**
         * @var Session $session
         */
        $session = $this->container->make(Session::class);

        return $session->getUser() instanceof Administrator;
    }
}
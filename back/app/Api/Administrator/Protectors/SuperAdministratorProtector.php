<?php

namespace App\Api\Administrator\Protectors;

use App\Api\Shared\Protectors\AuthProtector;
use App\Core\Administrator\Entities\Administrator;
use App\Core\Administrator\Enums\Role;
use App\Core\Session\Entities\Session;

class SuperAdministratorProtector extends AuthProtector
{
    /**
     * @return bool
     */
    public function grants() : bool
    {
        if (!parent::grants()) {
            return false;
        }

        /**
         * @var Session $session
         */
        $session = $this->container->make(Session::class);

        $user = $session->getUser();

        if (!$user instanceof Administrator) {
            return false;
        }

        return $user->getRole()->is(Role::SUPERUSER);
    }
}
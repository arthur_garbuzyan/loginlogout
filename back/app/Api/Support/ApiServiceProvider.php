<?php

namespace App\Api\Support;

use App\Api\Support\Converter\Populator\PopulatorFactory;
use App\Core\Session\Entities\ApiClient;
use App\Core\Session\Entities\GuestSession;
use App\Core\Session\Entities\Session;
use App\Core\Session\Objects\ClientCredentials;
use App\Core\Session\Services\ApiClientService;
use App\Core\Session\Services\SessionService;
use InscopeRest\Koala\Transformation\AbstractTransformer;
use InscopeRest\Processor\AbstractProcessor;
use InscopeRest\Processor\PopulatorFactoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use InscopeRest\Processor\SharedModifiers as ProcessorModifiers;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->app->singleton(PopulatorFactoryInterface::class, PopulatorFactory::class);

        /**
         * Registers shared modifiers for all processors
         */
        AbstractProcessor::setSharedModifiersProvider(new ProcessorModifiers());

        /**
         * Registers shared modifiers for all transformers
         */
        AbstractTransformer::setSharedModifiersProvider(new TransformerModifiers($this->app));
    }

    /**
     *
     */
    public function register() : void
    {
        $this->app->singleton(Session::class, function() {
            /**
             * @var Request $request
             */
            $request = $this->app->make('request');

            $token = $request->header('Token');

            if (! $token && $request->isMethod(Request::METHOD_POST)) {
                $token = $request->get('Token');
            }

            if (!$token) {
                return new GuestSession();
            }

            /**
             * @var SessionService $sessionService
             */
            $sessionService = $this->app->make(SessionService::class);

            $session = $sessionService->getByToken($token);

            if (!$session){
                return new GuestSession();
            }

            return $session;
        });

        $this->app->singleton(ApiClient::class, function() {
            /**
             * @var Request $request
             */
            $request = $this->app->make('request');
            $credentials = new ClientCredentials();

            if ($id = $request->header('Client-Id') && $secret = $request->header('Client-Secret')) {
                $credentials->setId($id);
                $credentials->setSecret($secret);

                /**
                 * @var ApiClientService $clientService
                 */
                $clientService = $this->app->make(ApiClientService::class);
                return $clientService->getByCredentials($credentials);
            }

            return null;
        });
    }
}
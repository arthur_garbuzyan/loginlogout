<?php

namespace App\Api\Support;

use Asm89\Stack\CorsService;
use Exception;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Kernel extends HttpKernel
{
    protected $middleware = [
        // It causes issues with CORS when responses with non 200 HTTP code
    ];

    protected $routeMiddleware = [
        'cors' => \Barryvdh\Cors\HandleCors::class
    ];

    /**
     * @param Request $request
     * @param Exception $e
     * @return Response
     */
    protected function renderException($request, Exception $e)
    {
        $response = parent::renderException($request, $e);

        /**
         * @var CorsService $corsService
         */
        $corsService = $this->app->make(CorsService::class);

        return $corsService->addActualRequestHeaders($response, $request);
    }
}

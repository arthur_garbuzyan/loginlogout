<?php

namespace App\Api\Support;

use App\Api\Shared\Controllers\DefaultController;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class RouteServiceProvider extends ServiceProvider
{
    protected $globalPrefix = '';

    /**
     * @param Router $router
     */
    protected function define(Router $router) : void
    {
        $router->get($this->globalPrefix . '/', DefaultController::class.'@server');

        $router->group([
            'prefix' => $this->globalPrefix . '/api',
            'middleware' => ['cors']
        ], function (Router $router) {

            $router->get('/', DefaultController::class.'@api');

            $packages = $this->app->make('config')
                ->get('app.packages');

            foreach ($packages as $package) {
                $path = app_path('Api' .DIRECTORY_SEPARATOR. str_replace('\\', '/', $package) .DIRECTORY_SEPARATOR. 'Routes');
                $routesNamespace = 'App\Api\\' . $package . '\Routes';

                if (!file_exists($path)) {
                    continue;
                }

                $finder = new Finder();

                /**
                 * @var SplFileInfo[] $files
                 */
                $files = $finder->in($path)
                    ->files()
                    ->name('*.php');

                foreach ($files as $file) {
                    $name = str_replace_last('.php', '', $file->getFilename());

                    $this->app->make($routesNamespace . '\\' . $name)
                        ->register($router);
                }
            }
        });
    }

    /**
     * @param Router $router
     */
    public function map(Router $router) : void
    {
        $this->define($router);
    }
}

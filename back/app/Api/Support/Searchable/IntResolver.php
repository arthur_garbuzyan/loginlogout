<?php

namespace App\Api\Support\Searchable;

use InscopeRest\Validation\Rules\IntegerCast;

class IntResolver
{
    /**
     * @param string $value
     * @return bool
     */
    public function isProcessable($value) : bool
    {
        return !(new IntegerCast(true))->check($value);
    }

    /**
     * @param string $value
     * @return int
     */
    public function resolve($value) : int
    {
        return (int) $value;
    }
}
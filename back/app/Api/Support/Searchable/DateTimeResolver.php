<?php

namespace App\Api\Support\Searchable;

use InscopeRest\Validation\Rules\Moment;
use DateTime;
use DateTimeZone;

class DateTimeResolver
{
    /**
     * @param string $datetime
     * @return bool
     */
    public function isProcessable($datetime) : bool
    {
        return !(new Moment())->check($datetime);
    }

    /**
     * @param string $datetime
     * @return DateTime
     */
    public function resolve($datetime) : DateTime
    {
        $datetime = new DateTime($datetime);

        $datetime->setTimezone(new DateTimeZone('UTC'));

        return $datetime;
    }
}
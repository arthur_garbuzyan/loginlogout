<?php

namespace App\Api\Support\Searchable;

use App\Core\Shared\Objects\Interval;
use InscopeRest\Validation\Rules\FloatCast;

class IntervalResolver
{
    /**
     * @param array $values
     * @return bool
     */
    public function isProcessable($values) : bool
    {
        if (!is_array($values)) {
            return false;
        }

        if (array_key_exists('min', $values) && !(new FloatCast())->check($values['from'])) {
            return true;
        }

        if (array_key_exists('max', $values) && !(new FloatCast())->check($values['to'])) {
            return true;
        }

        return false;
    }

    /**
     * @param array $values
     * @return Interval
     */
    public function resolve(array $values) : Interval
    {
        $interval = new Interval();

        if (array_key_exists('min', $values) && !(new FloatCast())->check($values['min'])) {
            $interval->setMin($values['min']);
        }

        if (array_key_exists('max', $values) && !(new FloatCast())->check($values['max'])) {
            $interval->setMax($values['max']);
        }

        return $interval;
    }
}
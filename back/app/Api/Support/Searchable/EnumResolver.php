<?php

namespace App\Api\Support\Searchable;

use InscopeRest\Enum\Enum;
use InscopeRest\Enum\EnumCollection;

class EnumResolver
{
    /**
     * @param string|array $values
     * @param string $class
     * @return bool
     */
    private function isCollectionProcessable($values, string $class): bool
    {
        if (is_string($values)) {
            $values = array_map('trim', explode(',', $values));
        }

        $enumClass = call_user_func([$class, 'getEnumClass']);

        foreach ($values as $value) {
            if (!$this->isProcessable($value, $enumClass)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string|array $value
     * @param string $class
     * @return bool
     */
    public function isProcessable($value, string $class): bool
    {
        if (is_subclass_of($class, EnumCollection::class)) {
            return $this->isCollectionProcessable($value, $class);
        }

        return call_user_func([$class, 'has'], $value);
    }

    /**
     * @param string|array $value
     * @param string $class
     * @return Enum|EnumCollection
     */
    public function resolve($value, string $class)
    {
        if (is_subclass_of($class, EnumCollection::class)) {
            if (is_string($value)) {
                $value = array_map('trim', explode(',', $value));
            }
            return call_user_func([$class, 'make'], $value);
        }

        return new $class($value);
    }
}
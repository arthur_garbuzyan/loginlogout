<?php

namespace App\Api\Support\Searchable;

use App\Core\Shared\Enums\FixedDateInterval;
use App\Core\Shared\Objects\DateInterval;
use DateTime;
use DateTimeZone;

class FixedDateIntervalResolver
{
    /**
     * @param string $value
     * @return bool
     */
    public function isProcessable($value): bool
    {
        return FixedDateInterval::has($value);
    }

    /**
     * @param string $value
     * @return DateInterval
     */
    public function resolve($value): DateInterval
    {
        $interval = new DateInterval();

        if ($value === FixedDateInterval::TODAY) {
            $from = new DateTime('today');
            $from->setTimezone(new DateTimeZone('UTC'));
            $interval->setFrom($from);

            $to = new DateTime();
            $to->setTimezone(new DateTimeZone('UTC'));
            $interval->setTo($to);
        }

        if ($value === FixedDateInterval::YESTERDAY) {
            $from = new DateTime('1 day ago 00:00:00');
            $from->setTimezone(new DateTimeZone('UTC'));
            $interval->setFrom($from);

            $to = new DateTime('1 day ago 23:59:59');
            $to->setTimezone(new DateTimeZone('UTC'));
            $interval->setTo($to);
        }

        if ($value === FixedDateInterval::THIS_WEEK) {
            $from = new DateTime('7 days ago 00:00:00');
            $from->setTimezone(new DateTimeZone('UTC'));
            $interval->setFrom($from);

            $to = new DateTime();
            $to->setTimezone(new DateTimeZone('UTC'));
            $interval->setTo($to);
        }

        if ($value === FixedDateInterval::THIS_MONTH) {
            $from = new DateTime('1 month ago 00:00:00');
            $from->setTimezone(new DateTimeZone('UTC'));
            $interval->setFrom($from);

            $to = new DateTime();
            $to->setTimezone(new DateTimeZone('UTC'));
            $interval->setTo($to);
        }

        return $interval;
    }
}
<?php

namespace App\Api\Support\Searchable;

use App\Core\Shared\Objects\DateInterval;
use DateTime;
use DateTimeZone;
use InscopeRest\Validation\Rules\Moment;

class DateIntervalResolver
{
    /**
     * @param array $values
     * @return bool
     */
    public function isProcessable($values) : bool
    {
        if (!is_array($values)) {
            return false;
        }

        if (array_key_exists('from', $values) && !(new Moment())->check($values['from'])) {
            return true;
        }

        if (array_key_exists('to', $values) && !(new Moment())->check($values['to'])) {
            return true;
        }

        return false;
    }

    /**
     * @param array $values
     * @return DateInterval
     */
    public function resolve(array $values) : DateInterval
    {
        $interval = new DateInterval();

        if (array_key_exists('from', $values) && !(new Moment())->check($values['from'])) {
            $from = new DateTime($values['from']);
            $from->setTimezone(new DateTimeZone('UTC'));
            $interval->setFrom($from);
        }

        if (array_key_exists('to', $values) && !(new Moment())->check($values['to'])) {
            $to = new DateTime($values['to']);
            $to->setTimezone(new DateTimeZone('UTC'));
            $interval->setTo($to);
        }

        return $interval;
    }
}
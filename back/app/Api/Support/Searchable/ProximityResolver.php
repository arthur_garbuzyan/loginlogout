<?php

namespace App\Api\Support\Searchable;

use App\Core\Location\Objects\Coordinates;
use App\Core\Location\Objects\Proximity;
use InscopeRest\Validation\Rules\FloatCast;

class ProximityResolver
{
    /**
     * @param array $values
     * @return bool
     */
    public function isProcessable($values) : bool
    {
        if (!is_array($values)) {
            return false;
        }

        if ((new FloatCast(true))->check(array_get($values, 'latitude', null))) {
            return false;
        }

        if ((new FloatCast(true))->check(array_get($values, 'longitude', null))) {
            return false;
        }

        if ((new FloatCast(true))->check(array_get($values, 'radius', null))) {
            return false;
        }

        return true;
    }

    /**
     * @param array $values
     * @return Proximity
     */
    public function resolve(array $values): Proximity
    {
        $coordinates = new Coordinates();
        $coordinates->setLatitude(array_get($values, 'latitude'));
        $coordinates->setLongitude(array_get($values, 'longitude'));

        $proximity = new Proximity();
        $proximity->setRadius(array_get($values, 'radius'));
        $proximity->setCoordinates($coordinates);

        return $proximity;
    }
}
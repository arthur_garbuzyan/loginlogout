<?php

namespace App\Api\Support\Searchable;

class BoolResolver
{
    /**
     * @param string $value
     * @return bool
     */
    public function isProcessable($value) : bool
    {
        return in_array($value, ['true', 'false'], true);
    }

    /**
     * @param string $value
     * @return bool
     */
    public function resolve($value) : bool
    {
        return $value === 'true';
    }
}
<?php

namespace App\Api\Support;

use InscopeRest\Koala\Pagination\AdapterInterface;
use InscopeRest\Koala\Pagination\Paginator;
use InscopeRest\Koala\Resource\ResourceManager;
use InscopeRest\Koala\Transformation\AbstractTransformer;
use InscopeRest\Permissions\PermissionsRequirableInterface;
use InscopeRest\Verifier\ActionVerifiableInterface;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as IlluminateController;
use RuntimeException;

class BaseController extends IlluminateController implements PermissionsRequirableInterface, ActionVerifiableInterface
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var ResourceManager
     */
    protected $resource;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->resource = $this->container->make(ResourceManager::class);

        if (method_exists($this, 'initialize')) {
            $this->container->call([$this, 'initialize']);
        }
    }

    /**
     * @param string $class
     * @return AbstractTransformer
     * @throws RuntimeException
     */
    protected function transformer($class = DefaultTransformer::class) : AbstractTransformer
    {
        /**
         * @var TransformerFactory $factory
         */
        $factory = $this->container->make(TransformerFactory::class);

        /**
         * @var Request $request
         */
        $request = $this->container->make(Request::class);
        $input = $request->header('Include');

        $fields = $input ? array_map('trim', explode(',', $input)) : [];

        $config = $this->container->make('config')->get('transformer');

        return $factory->create($class, $config)->setIncludes($fields);
    }

    /**
     * @param AdapterInterface $adapter
     * @return Paginator
     */
    protected function paginator(AdapterInterface $adapter) : Paginator
    {
        Paginator::setRequest($this->container->make(Request::class));

        return new Paginator($adapter);
    }
}
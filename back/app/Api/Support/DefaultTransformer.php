<?php

namespace App\Api\Support;

class DefaultTransformer extends BaseTransformer
{
    /**
     * @param object $item
     * @return array
     * @throws \InscopeRest\Converter\Extractor\ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    public function transform($item) : array
    {
        return $this->extract($item);
    }
}
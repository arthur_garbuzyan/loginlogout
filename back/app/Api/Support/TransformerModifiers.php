<?php

namespace App\Api\Support;

use InscopeRest\Koala\Transformation\SharedModifiers;
use Illuminate\Contracts\Container\Container;

class TransformerModifiers extends SharedModifiers
{
    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct([
            'stringable' => $container->make('config')->get('transformer.stringable', [])
        ]);
    }

    /**
     * @param string $value
     * @return string
     */
    public function purifier(string $value) : string
    {
        return $value;
    }

    /**
     * @param string $value
     * @return string
     */
    public function mask(string $value) : string
    {
        if ($value === null){
            return $value;
        }

        return substr($value, -4, 4);
    }
}
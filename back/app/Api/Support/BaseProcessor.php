<?php

namespace App\Api\Support;

use InscopeRest\Processor\AbstractProcessor;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\RuleInterface;
use InscopeRest\Validation\Rules\BooleanCast;
use InscopeRest\Validation\Rules\Callback;
use InscopeRest\Validation\Rules\Each;
use InscopeRest\Validation\Rules\FloatCast;
use InscopeRest\Validation\Rules\IntegerCast;
use InscopeRest\Validation\Rules\Moment;
use InscopeRest\Validation\Rules\StringCast;
use InscopeRest\Validation\Rules\Walk;
use Illuminate\Http\Request;
use RuntimeException;

abstract class BaseProcessor extends AbstractProcessor
{
    /**
     * @param Binder $binder
     */
    protected function rules(Binder $binder) : void
    {
        foreach ($this->configuration() as $name => $rule) {
            $binder->bind($name, $this->createRootInflator($rule));
        }
    }

    /**
     * @param string|array $rule
     * @return callable
     */
    private function createRootInflator($rule) : callable
    {
        return function (Property $property) use ($rule) {

            if (is_string($rule) && ends_with($rule, '[]')) {
                $rule = [cut_string_right($rule, '[]')];
            }

            if (is_array($rule) && count($rule) === 1 && array_key_exists(0, $rule)){
                $property->addRule(new Each(function() use ($rule) {
                    return $this->resolveRule(current($rule));
                }));
            } else {
                $property->addRule($this->resolveRule($rule));
            }
        };
    }

    /**
     *
     * @param mixed $rule
     * @return RuleInterface
     */
    private function resolveRule($rule) : RuleInterface
    {
        if (is_string($rule)) {
            $rule = $this->mapRules()[$rule];
        }

        if (is_callable($rule)) {
            return call_user_func($rule);
        }

        if (is_object($rule)) {
            return $rule;
        }

        if (is_string($rule)) {
            return new $rule();
        }

        if (is_array($rule)) {
            return new Walk(function(Binder $binder) use ($rule) {
                foreach ($rule as $key => $value) {
                    $binder->bind($key, $this->createRootInflator($value));
                }
            });
        }

        throw new RuntimeException('Unable to resolve a validation rule.');
    }

    /**
     *
     * @return array
     */
    protected function mapRules() : array
    {
        return [
            'string' => StringCast::class,
            'bool' => BooleanCast::class,
            'int' => IntegerCast::class,
            'float' => FloatCast::class,
            'datetime' => Moment::class,
            'array' => (new Callback(function($v) {
                return is_array($v);
            }))->setIdentifier('cast')
                ->setMessage('The field should be an array.')
        ];
    }

    /**
     *
     * @return array
     */
    protected function configuration() : array
    {
        return [];
    }

    /**
     *
     * @return array
     */
    protected function allowable() : array
    {
        return array_keys($this->configuration());
    }

    /**
     * @return bool
     */
    protected function isPatch() : bool
    {
        return strtolower($this->getRequest()->method()) == strtolower(Request::METHOD_PATCH);
    }
}
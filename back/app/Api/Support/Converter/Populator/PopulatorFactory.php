<?php

namespace App\Api\Support\Converter\Populator;

use App\Api\Support\Converter\Populator\Resolvers\FilePersistableCollectionHintResolver;
use App\Api\Support\Converter\Populator\Resolvers\FilePersistableResolver;
use InscopeRest\Converter\Populator\Populator;
use InscopeRest\Modifier\Manager;
use InscopeRest\Processor\DefaultPopulatorFactory;

class PopulatorFactory extends DefaultPopulatorFactory
{
    /**
     * @param array $options
     * @param Manager $modifierManager
     * @return Populator
     */
    public function create(array $options = [], Manager $modifierManager = null) : Populator
    {
        $populator = parent::create($options, $modifierManager);

        $populator->addGlobalResolver(new FilePersistableResolver());
        $populator->addGlobalResolver(new FilePersistableCollectionHintResolver());

        return $populator;
    }
}
<?php

namespace App\Api\Support\Converter\Populator\Resolvers;

use App\Core\Shared\Persistables\FilePersistable;
use InscopeRest\Converter\Populator\Resolvers\AbstractResolver;
use ReflectionParameter;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FilePersistableResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $class = $parameter->getClass();

        return $class && (($class->getName() === FilePersistable::class)
                || $class->isSubclassOf(FilePersistable::class)) && ($value instanceof UploadedFile);
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $file
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return mixed
     */
    public function resolve(string $field, $file, $oldValue, ReflectionParameter $parameter) : FilePersistable
    {
        $class = $parameter->getClass();

        /**
         * @var FilePersistable $persistable
         */
        $persistable = $class->newInstance();

        return static::populate($persistable, $file);
    }

    /**
     *
     * @param FilePersistable $persistable
     * @param UploadedFile $file
     * @return FilePersistable
     */
    public static function populate(FilePersistable $persistable, UploadedFile $file) : FilePersistable
    {
        $persistable->setLocation($file->getPathname());
        $persistable->setSuggestedName($file->getClientOriginalName());
        $persistable->setMimeType($file->getMimeType());
        return $persistable;
    }
}
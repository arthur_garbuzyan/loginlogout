<?php

namespace App\Api\Support\Converter\Populator\Resolvers;

use App\Core\Shared\Persistables\FilePersistable;
use InscopeRest\Converter\Populator\Resolvers\AbstractResolver;
use ReflectionParameter;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FilePersistableCollectionHintResolver extends AbstractResolver
{
    const CONFIG_KEY = 'hint';
    const TOKEN = 'collection:';

    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return array
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : array
    {
        $array = [];

        foreach ($value as $item) {
            $array[] = FilePersistableResolver::populate(new FilePersistable(), $item);
        }

        return $array;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $hint = $this->config->get(self::CONFIG_KEY . ".{$field}", '');

        if (!is_array($value)) {
            return false;
        }

        foreach ($value as $item) {
            if (!$item instanceof UploadedFile) {
                return false;
            }
        }

        return $hint === self::TOKEN.FilePersistable::class;
    }
}
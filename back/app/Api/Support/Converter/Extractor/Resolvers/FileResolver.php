<?php

namespace App\Api\Support\Converter\Extractor\Resolvers;

use App\Core\Shared\Interfaces\StorageInterface;
use App\Core\Shared\Objects\File;
use InscopeRest\Converter\Extractor\Resolvers\AbstractResolver;
use InscopeRest\Converter\Extractor\Root;

class FileResolver extends AbstractResolver
{
    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @param StorageInterface $storage
     * @param array $config
     */
    public function __construct(StorageInterface $storage, array $config = [])
    {
        parent::__construct($config);
        $this->storage = $storage;
    }

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null): bool
    {
        return $value instanceof File;
    }

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param File $value
     * @param Root $root
     * @return mixed
     */
    public function resolve(string $scope, $value, Root $root = null)
    {
        return $this->storage->getFilePublicUri($value->getLocation());
    }
}
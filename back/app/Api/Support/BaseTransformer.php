<?php

namespace App\Api\Support;

use App\Api\Support\Converter\Extractor\Resolvers\FileResolver;
use App\Core\Shared\Interfaces\StorageInterface;
use InscopeRest\Converter\Extractor\Extractor;
use InscopeRest\Koala\Transformation\AbstractTransformer;

abstract class BaseTransformer extends AbstractTransformer
{
    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @param StorageInterface $storage
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param mixed $value
     * @param string|array $modifier
     * @return mixed
     */
    public function modify($value, $modifier)
    {
        return $this->getModifierManager()->modify($value, $modifier);
    }

    /**
     * @param array $options
     * @return Extractor
     */
    protected function createExtractor(array $options = []) : Extractor
    {
        $extractor = parent::createExtractor($options);

        $extractor->addGlobalResolver(new FileResolver($this->storage));

        return $extractor;
    }
}

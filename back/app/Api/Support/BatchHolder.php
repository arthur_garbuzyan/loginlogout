<?php

namespace App\Api\Support;

class BatchHolder
{
    /**
     * @var object[]
     */
    private $data = [];

    /**
     * @return object[]
     */
    public function getData() : array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }
}
<?php

namespace App\Api\Support\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Rules\AbstractRule;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class File extends AbstractRule
{
    /**
     * File constructor.
     */
    public function __construct()
    {
        $this->setIdentifier('file');
        $this->setMessage('File is not attached.');
    }

    /**
     *
     * @param mixed $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        if (! $value instanceof UploadedFile) {
            return $this->getError();
        }

        return null;
    }
}
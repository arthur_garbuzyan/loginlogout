<?php

namespace App\Api\Session\Processors;

use App\Api\Support\BaseProcessor;
use App\Core\Facebook\Objects\FacebookCredentials;
use App\Core\Session\Objects\ClientCredentials;
use InscopeRest\Validation\Rules\IntegerCast;

class FacebookSessionProcessor extends BaseProcessor
{
    /**
     * @return array
     */
    protected function configuration(): array
    {
        return [
            'id' => 'string',
            'token' => 'string',
            'clientId' => new IntegerCast(true),
            'clientSecret' => 'string'
        ];
    }

    /**
     * @return FacebookCredentials
     */
    public function createCredentials(): FacebookCredentials
    {
        return $this->populate(new FacebookCredentials());
    }

    /**
     * @return ClientCredentials
     */
    public function createClientCredentials(): ClientCredentials
    {
        return $this->populate(new ClientCredentials(), [
            'map' => [
                'clientId' => 'id',
                'clientSecret' => 'secret'
            ]
        ]);
    }
}
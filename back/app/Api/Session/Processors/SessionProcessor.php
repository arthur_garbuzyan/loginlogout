<?php

namespace App\Api\Session\Processors;

use App\Api\Support\BaseProcessor;
use App\Core\Session\Objects\ClientCredentials;
use App\Core\Session\Objects\Credentials;
use InscopeRest\Validation\Rules\IntegerCast;

class SessionProcessor extends BaseProcessor
{
    /**
     * @return array
     */
    protected function configuration() : array
    {
        return [
            'username' => 'string',
            'password' => 'string',
            'clientId' => new IntegerCast(true),
            'clientSecret' => 'string'
        ];
    }

    /**
     * @return Credentials
     */
    public function createCredentials() : Credentials
    {
        return $this->populate(new Credentials());
    }

    /**
     * @return ClientCredentials
     */
    public function createClientCredentials() : ClientCredentials
    {
        return $this->populate(new ClientCredentials(), [
            'map' => [
                'clientId' => 'id',
                'clientSecret' => 'secret'
            ]
        ]);
    }
}
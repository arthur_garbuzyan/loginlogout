<?php

namespace App\Api\Session\Protectors;

use App\Core\Session\Services\SessionService;
use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use InscopeRest\Permissions\ProtectorInterface;

class SessionProtector implements ProtectorInterface
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return bool
     */
    public function grants() : bool
    {
        /**
         * @var Request $request
         */
        $request = $this->container->make('request');

        $refreshToken = $request->header('RefreshToken');

        if (!$refreshToken && $request->isMethod(Request::METHOD_POST)) {
            $refreshToken = $request->get('RefreshToken');
        }

        /**
         * @var SessionService $sessionService
         */
        $sessionService = $this->container->make(SessionService::class);

        /**
         * @var Route $route
         */
        $route = $request->route();

        $sessionId = array_take(array_values($route->parameters()), 0);

        return $sessionService->existsWithRefreshToken($sessionId, $refreshToken);
    }
}
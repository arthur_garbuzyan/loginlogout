<?php

namespace App\Api\Session\Transformers;

use App\Api\Support\BaseTransformer;
use App\Core\Session\Entities\Session;

class SessionTransformer extends BaseTransformer
{
    /**
     * @param Session $session
     * @return array
     * @throws \InscopeRest\Converter\Extractor\ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    public function transform($session) : array
    {
        return $this->extract($session);
    }
}
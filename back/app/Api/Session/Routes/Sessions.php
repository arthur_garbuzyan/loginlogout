<?php

namespace App\Api\Session\Routes;

use App\Api\Session\Controllers\SessionsController;
use App\Api\Session\Controllers\SocialSessionsController;
use InscopeRest\Routing\RouteInterface;
use Illuminate\Contracts\Routing\Registrar;

class Sessions implements RouteInterface
{
    /**
     * @param Registrar $registrar
     */
    public function register(Registrar $registrar) : void
    {
        $registrar->get('sessions', SessionsController::class . '@show');
        $registrar->post('sessions', SessionsController::class . '@store');
        $registrar->delete('sessions', SessionsController::class . '@destroy');

        $registrar->put('sessions/refresh', SessionsController::class . '@autoRefresh');
        $registrar->put('sessions/{id}/refresh', SessionsController::class . '@refresh');

        $registrar->post('sessions/facebook', SocialSessionsController::class . '@storeFacebook');
    }
}
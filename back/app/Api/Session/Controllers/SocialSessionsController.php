<?php

namespace App\Api\Session\Controllers;

use App\Api\Session\Processors\FacebookSessionProcessor;
use App\Api\Session\Transformers\SessionTransformer;
use App\Api\Support\BaseController;
use App\Core\Facebook\Services\FacebookSessionService;
use Illuminate\Http\Response;

class SocialSessionsController extends BaseController
{
    /**
     * @var FacebookSessionService
     */
    private $facebookSessionService;

    /**
     * @param FacebookSessionService $facebookSessionService
     */
    public function initialize(FacebookSessionService $facebookSessionService)
    {
        $this->facebookSessionService = $facebookSessionService;
    }

    /**
     * @param FacebookSessionProcessor $processor
     * @return Response
     */
    public function storeFacebook(FacebookSessionProcessor $processor): Response
    {
        $session = $this->facebookSessionService->create(
            $processor->createCredentials(),
            $processor->createClientCredentials()
        );

        return $this->resource->make($session, $this->transformer(SessionTransformer::class));
    }
}
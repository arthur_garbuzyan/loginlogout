<?php

namespace App\Api\Session\Controllers;

use App\Api\Session\Processors\SessionProcessor;
use App\Api\Session\Transformers\SessionTransformer;
use App\Api\Support\BaseController;
use App\Core\Session\Entities\Session;
use App\Core\Session\Services\SessionService;
use Illuminate\Http\Response;

class SessionsController extends BaseController
{
    /**
     * @var SessionService
     */
    private $sessionService;

    /**
     * @param SessionService $sessionService
     */
    public function initialize(SessionService $sessionService)
    {
        $this->sessionService = $sessionService;
    }

    /**
     * @return Response
     */
    public function show()
    {
        /**
         * @var Session $session
         */
        $session = $this->container->make(Session::class);
        return $this->resource->make($session, $this->transformer(SessionTransformer::class));
    }

    /**
     * @param SessionProcessor $processor
     * @return Response
     */
    public function store(SessionProcessor $processor): Response
    {
        $session = $this->sessionService->create(
            $processor->createCredentials(),
            $processor->createClientCredentials()
        );

        return $this->resource->make($session, $this->transformer(SessionTransformer::class));
    }

    /**
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function autoRefresh(): Response
    {
        /**
         * @var Session $session
         */
        $session = $this->container->make(Session::class);

        return $this->resource->make(
            $this->sessionService->refresh($session->getId()),
            $this->transformer(SessionTransformer::class)
        );
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function refresh(int $id): Response
    {
        return $this->resource->make(
            $this->sessionService->refresh($id),
            $this->transformer(SessionTransformer::class)
        );
    }

    /**
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function destroy()
    {
        /**
         * @var Session $session
         */
        $session = $this->container->make(Session::class);

        $this->sessionService->delete($session->getId());
        return $this->resource->blank();
    }

    /**
     * @return bool
     */
    public static function verifyAction()
    {
        return true;
    }
}
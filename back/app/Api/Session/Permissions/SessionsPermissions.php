<?php

namespace App\Api\Session\Permissions;

use App\Api\Session\Protectors\SessionProtector;
use InscopeRest\Permissions\AbstractActionsPermissions;

class SessionsPermissions extends AbstractActionsPermissions
{
    /**
     * @return array
     */
    protected function permissions() : array
    {
        return [
            'store' => 'all',
            'show' => 'auth',
            'destroy' => 'auth',
            'autoRefresh' => 'auth',
            'refresh' => SessionProtector::class
        ];
    }
}
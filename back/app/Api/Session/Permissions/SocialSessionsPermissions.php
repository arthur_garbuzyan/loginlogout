<?php

namespace App\Api\Session\Permissions;

use InscopeRest\Permissions\AbstractActionsPermissions;

class SocialSessionsPermissions extends AbstractActionsPermissions
{
    /**
     * @return array
     */
    protected function permissions() : array
    {
        return [
            'storeFacebook' => 'all'
        ];
    }
}
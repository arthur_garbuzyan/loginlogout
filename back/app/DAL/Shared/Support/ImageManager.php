<?php

namespace App\DAL\Shared\Support;

use App\Core\Shared\Interfaces\ImageManagerInterface;
use App\Core\Shared\Persistables\FilePersistable;
use Imagick;
use ImagickException;

class ImageManager implements ImageManagerInterface
{
    /**
     * @param FilePersistable $file
     * @return bool
     */
    public function validate(FilePersistable $file): bool
    {
        try {
            $image = new Imagick($file->getLocation());

            return $image->valid();
        } catch (ImagickException $e) {
            return false;
        }
    }

    /**
     * @param string $path
     * @return string
     */
    protected function getFileExtension(string $path) : string
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

    /**
     * @param Imagick $image
     * @param FilePersistable $file
     * @param string $suffix
     * @param string $extension
     * @return FilePersistable
     */
    protected function createNewFile(Imagick $image, FilePersistable $file, string $suffix = '', string $extension = 'png') : FilePersistable
    {
        $oldExtension = $this->getFileExtension($file->getSuggestedName());
        $newName = str_replace('.'.$oldExtension, $suffix.'.'.$extension, $file->getSuggestedName());

        $image->writeImage(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $newName);

        $newFile = new FilePersistable();
        $newFile->setSuggestedName($newName);
        $newFile->setLocation(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $newName);
        $newFile->setMimeType($file->getMimeType());

        return $newFile;
    }
}
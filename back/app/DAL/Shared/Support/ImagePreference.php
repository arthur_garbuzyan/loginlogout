<?php

namespace App\DAL\Shared\Support;

use App\Core\Shared\Interfaces\ImagePreferencesInterface;
use App\Core\Shared\Objects\Dimensions;
use Illuminate\Contracts\Config\Repository as Config;
use InscopeRest\Validation\PresentableException;

class ImagePreference implements ImagePreferencesInterface
{
    private $config;

    /**
     * ImagePreference constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $key
     * @return Dimensions
     */
    public function getDimensions(string $key): Dimensions
    {
        $conf = $this->config->get("images.dimensions.$key", null);

        if ($conf === null) {
            throw new PresentableException("Unknown template key {$key}.");
        }

        $dimensions = new Dimensions();
        $dimensions->setWidth($conf['width']);
        $dimensions->setHeight($conf['height']);

        return $dimensions;
    }
}
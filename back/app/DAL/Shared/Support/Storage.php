<?php

namespace App\DAL\Shared\Support;

use App\Core\Shared\Interfaces\StorageInterface;
use Exception;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Illuminate\Contracts\Filesystem\Filesystem;

class Storage implements StorageInterface
{
    /**
     * @var Filesystem
     */
    private $disk;

    /**
     * Storage constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        /**
         * @var FilesystemFactory $storage
         */
        $storage = $container->make(FilesystemFactory::class);
        $this->disk = $storage->disk();
    }

    /**
     * @param string|resource $location
     * @param string $dest
     */
    public function putFileIntoRemoteStorage($location, string $dest) : void
    {
        $this->disk->put(
            $dest,
            file_get_contents($this->normalizeLocation($location)),
            Filesystem::VISIBILITY_PUBLIC
        );
    }

    /**
     * @param array $locations
     */
    public function removeFilesFromRemoteStorage(array $locations) : void
    {
        foreach ($locations as $location) {
            try {
                $this->disk->delete($location);
            } catch (Exception $ex){

            }
        }
    }

    /**
     * @param string|resource $location
     * @return bool
     */
    public function isFileReadable($location) : bool
    {
        return is_readable($this->normalizeLocation($location));
    }

    /**
     * @param string|resource $location
     * @return string
     */
    public function getFilePublicUri($location) : string
    {
        return url(
            'storage'.
            str_replace(DIRECTORY_SEPARATOR, '/', $location)
        );
    }

    /**
     * @param string|resource $location
     * @return string
     */
    public function getFileName($location) : string
    {
        return basename($location);
    }

    /**
     * @param string|resource $location
     * @return string
     */
    public function getFileFormat($location) : string
    {
        return strtolower(pathinfo($location, PATHINFO_EXTENSION));
    }

    /**
     * @param string|resource $location
     * @return string
     */
    private function normalizeLocation($location) : string
    {
        if (is_resource($location)) {
            return stream_get_meta_data($location)['uri'];
        }

        return $location;
    }
}
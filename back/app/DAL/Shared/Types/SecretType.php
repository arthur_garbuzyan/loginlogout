<?php

namespace App\DAL\Shared\Types;

use App\DAL\Support\AbstractType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Crypt;

class SecretType extends AbstractType
{
    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) : string
    {
        return 'VARCHAR(255)';
    }

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) : string
    {
        if ($value === null){
            return null;
        }

        return Crypt::encrypt($value);
    }

    /**
     * @param string $value
     * @param AbstractPlatform $platform
     * @return string
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) : string
    {
        if ($value === null){
            return null;
        }

        return Crypt::decrypt($value);
    }
}
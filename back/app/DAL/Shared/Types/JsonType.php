<?php

namespace App\DAL\Shared\Types;

use App\DAL\Support\AbstractType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class JsonType extends AbstractType
{
    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) : string
    {
        return 'JSON';
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return json_decode($value, true)['value'];
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) : string
    {
        return json_encode(['value' => $value]);
    }

//    /**
//     * Modifies the SQL expression (identifier, parameter) to convert to a database value.
//     *
//     * @param string                                    $sqlExpr
//     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
//     *
//     * @return string
//     */
//    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform)
//    {
//        return $sqlExpr;
//    }
//
//    /**
//     * Modifies the SQL expression (identifier, parameter) to convert to a PHP value.
//     *
//     * @param string                                    $sqlExpr
//     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform
//     *
//     * @return string
//     */
//    public function convertToPHPValueSQL($sqlExpr, $platform)
//    {
//        return $sqlExpr;
//    }
}
<?php

namespace App\DAL\Shared\Types;

use App\Core\Shared\Objects\File;
use App\DAL\Support\AbstractType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class FileType extends AbstractType
{
    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'VARCHAR(500)';
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?File
    {
        if ($value === null) {
            return null;
        }

        return new File($value);
    }

    /**
     * @param File $value
     * @param AbstractPlatform $platform
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if ($value === null) {
            return null;
        }

        return $value->getLocation();
    }

}
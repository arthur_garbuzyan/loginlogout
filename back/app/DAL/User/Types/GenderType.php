<?php

namespace App\DAL\User\Types;

use App\Core\User\Enums\Gender;
use App\DAL\Support\Types\EnumType;

class GenderType extends EnumType
{
    /**
     * @return string
     */
    protected function getEnumClass() : string
    {
        return Gender::class;
    }
}
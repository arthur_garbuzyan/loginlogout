<?php

namespace App\DAL\User\Support;

use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

trait LocationPropertiesDefiner
{
    /**
     * @param ClassMetadataBuilder $builder
     */
    protected function defineLocationProperties(ClassMetadataBuilder $builder)
    {
        $builder
            ->createField('address', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('city', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('country', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('state', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('postcode', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('latitude', 'float')
            ->nullable()
            ->build();

        $builder
            ->createField('longitude', 'float')
            ->nullable()
            ->build();
    }
}
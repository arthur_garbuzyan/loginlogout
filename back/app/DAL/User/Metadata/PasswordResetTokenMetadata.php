<?php

namespace App\DAL\User\Metadata;

use App\Core\User\Entities\User;
use App\DAL\Support\Metadata\AbstractMetadataProvider;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

class PasswordResetTokenMetadata extends AbstractMetadataProvider
{
    /**
     * @param ClassMetadataBuilder $builder
     * @return void
     */
    public function define(ClassMetadataBuilder $builder): void
    {
        $builder->setTable('password_reset_tokens');

        $this->defineId($builder);

        $builder
            ->createManyToOne('user', User::class)
            ->addJoinColumn('user_id', 'id', false, false, 'CASCADE')
            ->build();

        $builder
            ->createField('token', 'string')
            ->length(100)
            ->build();

        $builder
            ->createField('expiresAt', 'datetime')
            ->build();
    }
}
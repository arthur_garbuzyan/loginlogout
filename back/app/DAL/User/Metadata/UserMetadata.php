<?php

namespace App\DAL\User\Metadata;

use App\Core\Admins\Entities\Admins;
use App\Core\CheckInAndOut\Entities\CheckInAndOut;
use App\Core\Teachers\Entities\Teachers;
use App\Core\User\Enums\UserType;
use App\DAL\Session\Types\SocialNetworkProviderType;
use App\DAL\Shared\Types\FileType;
use App\DAL\Support\Metadata\AbstractMetadataProvider;
use App\DAL\User\Types\GenderType;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

class UserMetadata extends AbstractMetadataProvider
{
    /**
     * @param ClassMetadataBuilder $builder
     * @return void
     */
    public function define(ClassMetadataBuilder $builder): void
    {
        $builder->setTable('users')->setSingleTableInheritance()
            ->setDiscriminatorColumn('user_type', 'string', 100)
            ->addDiscriminatorMapClass(UserType::TEACHER, Teachers::class)
            ->addDiscriminatorMapClass(UserType::ADMIN, Admins::class);

        $this->defineId($builder);

        $builder
            ->createField('email', 'string')
            ->length(static::EMAIL_LENGTH)
            ->nullable()
            ->build();

        $builder->createField('password', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('firstName', 'string')
            ->length(static::FIRST_NAME_LENGTH)
            ->build();

        $builder
            ->createField('lastName', 'string')
            ->length(static::LAST_NAME_LENGTH)
            ->nullable()
            ->build();

        $builder
            ->createField('gender', GenderType::class)
            ->nullable()
            ->build();

        $builder
            ->createField('avatar', FileType::class)
            ->nullable()
            ->build();

        $builder
            ->createField('isActive', 'boolean')
            ->build();

        $builder
            ->createField('hasAgreedToTerms', 'boolean')
            ->build();

        $builder->createField('provider', SocialNetworkProviderType::class)
            ->nullable()
            ->build();

        $builder->createField('providerId', 'string')
            ->nullable()
            ->build();

        $builder
            ->createOneToMany('checkInAndOut', CheckInAndOut::class)
            ->mappedBy('user')
            ->build();

        $builder
            ->createField('reason', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('subject', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('loggedIn', 'datetime')
            ->nullable()
            ->build();

        $builder
            ->createField('loggedOut', 'datetime')
            ->nullable()
            ->build();

        $this->defineCreatedAt($builder);
        $this->defineUpdatedAt($builder);
    }
}
<?php

namespace App\DAL\Support;

use Doctrine\DBAL\Types\Type;

abstract class AbstractType extends Type
{
    /**
     * @return string
     */
    public function getName() : string
    {
        return get_called_class();
    }
}
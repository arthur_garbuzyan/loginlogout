<?php

namespace App\DAL\Support\Types;

use App\DAL\Support\AbstractType;
use InscopeRest\Enum\Enum;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use InscopeRest\Enum\EnumCollection;

abstract class EnumType extends AbstractType
{
    /**
     * @return string
     */
    abstract protected function getEnumClass() : string;

    /**
     * @return string
     */
    protected function getEnumCollectionClass() : ?string
    {
        return null;
    }

    /**
     * @return bool
     */
    protected function isArray() : bool
    {
        return $this->getEnumCollectionClass() !== null;
    }

    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) : string
    {
        if ($this->isArray()) {
            return 'VARCHAR(1000)';
        }

        return 'VARCHAR(100)';
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return Enum|EnumCollection
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        if ($this->isArray()){
            $result = json_decode($value);

            $class = $this->getEnumCollectionClass();

            $collection = new $class();

            foreach ($result as $value){
                $collection->push($this->createEnum($value));
            }

            return $collection;
        }

        return $this->createEnum($value);
    }

    /**
     * @param string $value
     * @return Enum
     */
    private function createEnum(string $value) : ?Enum
    {
        $class = $this->getEnumClass();
        return new $class($value);
    }

    /**
     * @param Enum $value
     * @param AbstractPlatform $platform
     * @return string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) : ?string
    {
        if ($value === null) {
            return null;
        }

        if ($this->isArray()){
            $result = [];

            $values = $value;

            foreach ($values as $value){
                $result[] = $value->value();
            }

            return json_encode($result);
        }

        return $value->value();
    }
}
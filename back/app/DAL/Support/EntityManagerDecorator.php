<?php

namespace App\DAL\Support;

use Doctrine\ORM\Decorator\EntityManagerDecorator as AbstractEntityManagerDecorator;

class EntityManagerDecorator extends AbstractEntityManagerDecorator
{
    /**
     * @param string $entityName
     * @param mixed $id
     * @return object
     */
    public function getReference($entityName, $id)
    {
        return parent::getReference($entityName, $id);
    }
}
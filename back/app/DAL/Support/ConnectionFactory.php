<?php

namespace App\DAL\Support;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\Configuration;

class ConnectionFactory
{
    /**
     * @param array $dbConfig
     * @param Configuration $configuration
     * @return Connection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __invoke(array $dbConfig, Configuration $configuration) : Connection
    {
        return DriverManager::getConnection($dbConfig, $configuration, new EventManager());
    }
}
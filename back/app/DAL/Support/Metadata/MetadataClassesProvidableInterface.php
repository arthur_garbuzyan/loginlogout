<?php

namespace App\DAL\Support\Metadata;

interface MetadataClassesProvidableInterface
{
    /**
     * @return array
     */
    public function getMetadataClasses() : array;
}
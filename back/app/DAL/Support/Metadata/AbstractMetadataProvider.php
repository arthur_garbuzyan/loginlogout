<?php

namespace App\DAL\Support\Metadata;

use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

abstract class AbstractMetadataProvider implements MetadataProviderInterface
{
    const PHONE_LENGTH = 20;
    const EMAIL_LENGTH = 50;
    const MIDDLE_NAME_LENGTH = 50;
    const FIRST_NAME_LENGTH = 50;
    const LAST_NAME_LENGTH = 50;
    const ADDRESS_LENGTH = 100;
    const ZIP_LENGTH = 10;
    const CITY_LENGTH = 100;
    const LICENSE_NUMBER_LENGTH = 50;
    const LATITUDE_LENGTH = 100;
    const LONGITUDE_LENGTH = 100;
    const FILE_NUMBER_LENGTH = 100;
    const LOAN_NUMBER_LENGTH = 100;
    const SECRET_LENGTH = 100;
    const DIRECTORY_LENGTH = 255;

    /**
     * @param ClassMetadataBuilder $builder
     */
    protected function defineId(ClassMetadataBuilder $builder) : void
    {
        $builder->createField('id', 'integer')
            ->makePrimaryKey()
            ->generatedValue()
            ->build();
    }

    /**
     * @param ClassMetadataBuilder $builder
     */
    protected function defineCreatedAt(ClassMetadataBuilder $builder) : void
    {
        $builder
            ->createField('createdAt', 'datetime')
            ->build();
    }

    /**
     * @param ClassMetadataBuilder $builder
     */
    protected function defineUpdatedAt(ClassMetadataBuilder $builder) : void
    {
        $builder
            ->createField('updatedAt', 'datetime')
            ->nullable(true)
            ->build();
    }
}
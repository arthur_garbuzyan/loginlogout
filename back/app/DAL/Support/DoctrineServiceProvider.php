<?php

namespace App\DAL\Support;

use App\DAL\Support\Metadata\CompositeDriver;
use App\DAL\Support\Metadata\PackageDriver;
use App\DAL\Support\Metadata\SimpleDriver;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\Tools\Setup;
use DoctrineExtensions\Query\Mysql\Acos;
use DoctrineExtensions\Query\Mysql\Cos;
use DoctrineExtensions\Query\Mysql\Radians;
use DoctrineExtensions\Query\Mysql\Sin;
use Illuminate\Contracts\Container\Container as ContainerInterface;
use Illuminate\Support\ServiceProvider;
use RuntimeException;
use SplFileInfo;
use Symfony\Component\Finder\Finder;
use Syslogic\DoctrineJsonFunctions\Query\AST\Functions\Mysql\JsonContains;
use Syslogic\DoctrineJsonFunctions\Query\AST\Functions\Mysql\JsonSearch;

class DoctrineServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('doctrine:configuration', function (ContainerInterface $container) {
            return $this->createConfiguration($this->getSettings($container), $container);
        });

        $this->app->singleton('doctrine:connection', function (ContainerInterface $container) {

            /**
             *
             * @var Configuration $configuration
             */
            $configuration = $container->make('doctrine:configuration');

            $factory = new ConnectionFactory();

            $settings = $this->getSettings($container);
            $dbConfig = $settings['connections'][$settings['db']];

            return $factory($dbConfig, $configuration);
        });

        $this->app->singleton(EntityManagerInterface::class, function (ContainerInterface $container) {

            /**
             * @var Connection $connection
             */
            $connection = $container->make('doctrine:connection');

            /**
             * @var Configuration $configuration
             */
            $configuration = $container->make('doctrine:configuration');

            return $this->createEntityManager($connection, $configuration, $container);
        });
    }

    /**
     *
     * @param ContainerInterface $container
     * @return array
     */
    private function getSettings(ContainerInterface $container)
    {
        return $container->make('config')->get('doctrine', []);
    }

    /**
     *
     * @param array $config
     * @param ContainerInterface $container
     * @return Configuration
     * @throws \Doctrine\ORM\ORMException
     */
    private function createConfiguration(array $config, ContainerInterface $container)
    {
        $setup = Setup::createConfiguration();

        $cache = new $config['cache']();

        $setup->setMetadataCacheImpl($cache);
        $setup->setQueryCacheImpl($cache);

        $setup->setProxyDir($config['proxy']['dir']);
        $setup->setProxyNamespace($config['proxy']['namespace']);
        $setup->setAutoGenerateProxyClasses(array_get($config, 'proxy.auto', false));

        $packages = $container->make('config')->get('app.packages');

        $setup->setMetadataDriverImpl(new CompositeDriver([
            new PackageDriver($packages),
            new SimpleDriver(array_take($config, 'entities', []))
        ]));

        $setup->setNamingStrategy(new UnderscoreNamingStrategy());
        $setup->setDefaultRepositoryClassName(DefaultRepository::class);

        $driver = $config['connections'][$config['db']]['driver'];

        if ($driver == 'pdo_mysql'){
            $setup->addCustomNumericFunction('acos', Acos::class);
            $setup->addCustomNumericFunction('cos', Cos::class);
            $setup->addCustomNumericFunction('sin', Sin::class);
            $setup->addCustomNumericFunction('radians', Radians::class);

            $setup->addCustomStringFunction('json_search', JsonSearch::class);
        } else {
            throw new RuntimeException('Unable to add functions under unknown driver "'.$driver.'".');
        }

        return $setup;
    }

    /**
     *
     * @param Connection $connection
     * @param Configuration $configuration
     * @param ContainerInterface $container
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\DBAL\DBALException
     */
    private function createEntityManager(Connection $connection, Configuration $configuration, ContainerInterface $container)
    {
        $packages = $container->make('config')->get('app.packages');

        $em = EntityManager::create($connection, $configuration);

        $this->registerTypes($em->getConnection(), $packages, $container->make('config')->get('doctrine.types', []));

        return new EntityManagerDecorator($em);
    }

    /**
     *
     * @param Connection $connection
     * @param array $packages
     * @param array $extra
     * @throws \Doctrine\DBAL\DBALException
     */
    private function registerTypes(Connection $connection, array $packages, array $extra = [])
    {
        foreach ($packages as $package) {
            $path = app_path('DAL/' . str_replace('\\', '/', $package) . '/Types');
            $typeNamespace = 'App\DAL\\' . $package . '\Types';

            if (! file_exists($path)) {
                continue;
            }

            $finder = new Finder();

            /**
             *
             * @var SplFileInfo[] $files
             */
            $files = $finder->in($path)
                ->files()
                ->name('*.php');

            foreach ($files as $file) {
                $name = cut_string_right($file->getFilename(), '.php');

                $typeClass = $typeNamespace . '\\' . $name;

                if (! class_exists($typeClass)) {
                    continue;
                }

                if (Type::hasType($typeClass)) {
                    Type::overrideType($typeClass, $typeClass);
                } else {
                    Type::addType($typeClass, $typeClass);
                }

                $connection->getDatabasePlatform()->registerDoctrineTypeMapping($typeClass, $typeClass);
            }
        }

        foreach ($extra as $type){
            if (Type::hasType($type)) {
                Type::overrideType($type, $type);
            } else {
                Type::addType($type, $type);
            }
        }
    }
}
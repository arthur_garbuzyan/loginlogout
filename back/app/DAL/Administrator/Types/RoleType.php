<?php

namespace App\DAL\Administrator\Types;

use App\Core\Administrator\Enums\Role;
use App\DAL\Support\Types\EnumType;

class RoleType extends EnumType
{
    /**
     * @return string
     */
    protected function getEnumClass(): string
    {
        return Role::class;
    }
}
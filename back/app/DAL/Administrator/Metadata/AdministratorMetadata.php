<?php

namespace App\DAL\Administrator\Metadata;

use App\DAL\Administrator\Types\RoleType;
use App\DAL\Support\Metadata\AbstractMetadataProvider;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

class AdministratorMetadata extends AbstractMetadataProvider
{
    /**
     * @param ClassMetadataBuilder $builder
     * @return void
     */
    public function define(ClassMetadataBuilder $builder) : void
    {
        $builder->setTable('administrators');

        $this->defineId($builder);

        $builder
            ->createField('username', 'string')
            ->build();

        $builder->createField('password', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('role', RoleType::class)
            ->build();
    }
}
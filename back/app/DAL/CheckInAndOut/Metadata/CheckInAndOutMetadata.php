<?php

namespace App\DAL\CheckInAndOut\Metadata;
use App\Core\User\Entities\User;
use App\DAL\Support\Metadata\AbstractMetadataProvider;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

class CheckInAndOutMetadata extends AbstractMetadataProvider
{

    /**
     * @param ClassMetadataBuilder $builder
     * @return void
     */
    public function define(ClassMetadataBuilder $builder): void
    {
        $builder->setTable('checkInAndOuts');
        $this->defineId($builder);

        $builder
            ->createManyToOne('user', User::class)
            ->addJoinColumn('user_id', 'id', false, false, 'CASCADE')
            ->build();

        $builder
            ->createField('reason', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('subject', 'string')
            ->nullable()
            ->build();

        $builder
            ->createField('loggedIn', 'datetime')
            ->nullable()
            ->build();

        $builder
            ->createField('loggedOut', 'datetime')
            ->nullable()
            ->build();

        $this->defineCreatedAt($builder);
    }
}
<?php

namespace App\DAL\Session\Types;

use App\Core\Session\Enums\AuthenticableUserType;
use App\DAL\Support\Types\EnumType;

class AuthenticableUserTypeType extends EnumType
{
    /**
     * @return string
     */
    protected function getEnumClass() : string
    {
        return AuthenticableUserType::class;
    }
}
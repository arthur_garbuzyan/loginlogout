<?php

namespace App\DAL\Session\Types;

use App\Core\Session\Enums\SocialNetworkProvider;
use App\DAL\Support\Types\EnumType;

class SocialNetworkProviderType extends EnumType
{
    public function getEnumClass(): string
    {
        return SocialNetworkProvider::class;
    }
}
<?php

namespace App\DAL\Session\Support;

use App\Core\Session\Interfaces\SessionPreferenceInterface;
use Illuminate\Contracts\Config\Repository as Config;

class SessionPreference implements SessionPreferenceInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return int
     */
    public function getLifetime()
    {
        return $this->config->get('session.lifetime', 60);
    }
}
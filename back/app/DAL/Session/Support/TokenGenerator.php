<?php

namespace App\DAL\Session\Support;

use App\Core\Session\Interfaces\TokenGeneratorInterface;

class TokenGenerator implements TokenGeneratorInterface
{
    /**
     * @return string
     */
    public function generate()
    {
        return str_random(64);
    }
}
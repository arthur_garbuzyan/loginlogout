<?php

namespace App\DAL\Session\Support;

use App\Core\Session\Interfaces\PasswordEncryptorInterface;

class PasswordEncryptor implements PasswordEncryptorInterface
{
    /**
     * @param string $password
     * @return string
     */
    public function encrypt(string $password) : string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public function verify(string $password, string $hash) : bool
    {
        return password_verify($password, $hash);
    }
}
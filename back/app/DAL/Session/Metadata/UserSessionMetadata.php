<?php

namespace App\DAL\Session\Metadata;

use App\Core\User\Entities\User;
use App\DAL\Support\Metadata\AbstractMetadataProvider;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

class UserSessionMetadata extends AbstractMetadataProvider
{
    /**
     * @param ClassMetadataBuilder $builder
     */
    public function define(ClassMetadataBuilder $builder) : void
    {
        $builder
            ->createManyToOne('user', User::class)
            ->addJoinColumn('user_id', 'id', false, false, 'CASCADE')
            ->build();
    }
}
<?php

namespace App\DAL\Session\Metadata;

use App\Core\Session\Entities\AdministratorSession;
use App\Core\Session\Entities\UserSession;
use App\Core\Session\Enums\AuthenticableUserType;
use App\DAL\Support\Metadata\AbstractMetadataProvider;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

class SessionMetadata extends AbstractMetadataProvider
{
    /**
     * @param ClassMetadataBuilder $builder
     */
    public function define(ClassMetadataBuilder $builder) : void
    {
        $builder->setTable('sessions')
            ->setSingleTableInheritance()
            ->setDiscriminatorColumn('user_type', 'string', 20)
            ->addDiscriminatorMapClass(AuthenticableUserType::USER, UserSession::class)
            ->addDiscriminatorMapClass(AuthenticableUserType::ADMINISTRATOR, AdministratorSession::class);

        $this->defineId($builder);

        $builder->createField('accessToken', 'string')
            ->length(100)
            ->unique()
            ->build();

        $builder->createField('refreshToken', 'string')
            ->length(100)
            ->unique()
            ->build();

        $builder->createField('expireAt', 'datetime')->build();

        $builder->createField('createdAt', 'datetime')->build();
    }
}
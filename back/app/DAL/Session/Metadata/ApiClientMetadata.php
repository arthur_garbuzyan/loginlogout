<?php

namespace App\DAL\Session\Metadata;

use App\DAL\Session\Types\AuthenticableUserTypeType;
use App\DAL\Support\Metadata\AbstractMetadataProvider;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

class ApiClientMetadata extends AbstractMetadataProvider
{
    /**
     * @param ClassMetadataBuilder $builder
     */
    public function define(ClassMetadataBuilder $builder) : void
    {
        $builder->setTable('api_clients');

        $this->defineId($builder);

        $builder->createField('secret', 'string')
            ->length(100)
            ->build();

        $builder
            ->createField('authenticable', AuthenticableUserTypeType::class)
            ->nullable()
            ->build();
    }
}
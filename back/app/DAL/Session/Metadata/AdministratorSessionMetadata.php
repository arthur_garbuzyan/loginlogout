<?php

namespace App\DAL\Session\Metadata;

use App\Core\Administrator\Entities\Administrator;
use App\DAL\Support\Metadata\AbstractMetadataProvider;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;

class AdministratorSessionMetadata extends AbstractMetadataProvider
{
    /**
     * @param ClassMetadataBuilder $builder
     */
    public function define(ClassMetadataBuilder $builder) : void
    {
        $builder
            ->createManyToOne('user', Administrator::class)
            ->addJoinColumn('user_id', 'id', false, false, 'CASCADE')
            ->build();
    }
}
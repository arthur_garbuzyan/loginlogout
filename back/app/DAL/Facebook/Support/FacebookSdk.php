<?php

namespace App\DAL\Facebook\Support;

use App\Core\Facebook\Interfaces\FacebookSdkInterface;
use App\Core\Facebook\Objects\FacebookUserData;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\GraphNodes\GraphUser;
use RuntimeException;
use Illuminate\Contracts\Config\Repository as Config;

class FacebookSdk implements FacebookSdkInterface
{
    /**
     * @var Facebook
     */
    private $sdk;

    /**
     * @var Config
     */
    private $config;

    /**
     * FacebookSdk constructor.
     * @param Config $config
     * @throws FacebookSDKException
     */
    public function __construct(Config $config)
    {
        $this->config = $config;

        $this->sdk = new Facebook([
            'app_id' => $this->config->get('services.facebook.app_id'),
            'app_secret' => $this->config->get('services.facebook.app_secret')
        ]);
    }

    /**
     * @param string $id
     * @param string $token
     * @return GraphUser
     * @throws FacebookSDKException
     */
    protected function getGraphUser(string $id, string $token): GraphUser
    {
        $response = $this->sdk->get("/{$id}/", $token);
        return $response->getGraphUser();
    }

    /**
     * @param string $id
     * @param string $token
     * @return bool
     */
    public function isAuthorized(string $id, string $token): bool
    {
        try {
            $this->getGraphUser($id, $token);
        } catch (FacebookResponseException $e) {
            return false;
        } catch (FacebookSDKException $e) {
            throw new RuntimeException('Facebook SDK returned an error: ' . $e->getMessage());
        }

        return true;
    }

    /**
     * @param string $id
     * @param string $token
     * @return FacebookUserData|null
     */
    public function getUserData(string $id, string $token): ?FacebookUserData
    {
        try {
            $user = $this->getGraphUser($id, $token);
        } catch (FacebookResponseException $e) {
            return null;
        } catch (FacebookSDKException $e) {
            throw new RuntimeException('Facebook SDK returned an error: ' . $e->getMessage());
        }

        $userData = new FacebookUserData();
        $userData->setId($user->getId());
        $userData->setName($user->getName());

        return $userData;
    }
}
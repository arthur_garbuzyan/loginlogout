<?php

namespace App\Mail\Handlers\User;

use App\Core\User\Notifications\PasswordResetNotification;
use App\Mail\Support\HandlerInterface;
use App\Mail\Support\Message;
use Illuminate\Mail\Mailer;
use Illuminate\Config\Repository as Config;

class PasswordResetHandler implements HandlerInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param Mailer $mailer
     * @param Message $message
     * @param PasswordResetNotification $source
     */
    public function handle(Mailer $mailer, Message $message, $source)
    {
        $noReply = $this->config->get('mail.from.address');
        $signature = $this->config->get('mail.from.name');
        $frontendUrl = $this->config->get('app.frontend.url');
        $user = $source->getPasswordResetToken()->getUser();

        $message->view('emails.user.password-reset', [
            'token' => $source->getPasswordResetToken()->getToken(),
            'url' => $frontendUrl,
            'userName' => $user->getFirstName() .' '. $user->getLastName()
        ]);

        $message->from($noReply, $signature);
        $message->subject('Your Password Reset link');
        $message->to($user->getEmail());
        $mailer->queue($message);
    }
}
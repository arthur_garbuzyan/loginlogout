<?php

namespace App\Mail\Support;

use Illuminate\Mail\Mailer;

interface HandlerInterface
{
    /**
     * @param Mailer $mailer
     * @param Message $message
     * @param object $source
     */
    public function handle(Mailer $mailer, Message $message,  $source);
}
<?php

namespace App\Mail\Support;

use App\Core\Shared\Interfaces\NotifierInterface;
use Illuminate\Container\Container;
use Illuminate\Config\Repository as Config;

class Notifier implements NotifierInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var callable[]
     */
    private $filters = [];

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->config = $container->make('config');
    }

    /**
     * @param object $notification
     */
    public function notify($notification)
    {
        foreach ($this->filters as $filter){
            if ($filter($notification) === null) {
                return ;
            }
        }

        $handlers = $this->container->make('config')->get('alert.handlers.email', []);
        $class = get_class($notification);

        if (!isset($handlers[$class])){
            return ;
        }

        /**
         * @var HandlerInterface $handler
         */
        $handler = $this->container->make($handlers[$class]);
        $handler->handle($this->container->make('mailer'), new Message(), $notification);
    }
}
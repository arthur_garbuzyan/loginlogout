<?php

namespace App\Core\Session\Services;

use App\Core\Administrator\Services\AdministratorService;
use App\Core\Session\Entities\ApiClient;
use App\Core\Session\Enums\AuthenticableUserType;
use App\Core\Session\Objects\ClientCredentials;
use App\Core\Session\Persistables\ApiClientPersistable;
use App\Core\Session\Validation\ClientValidator;
use App\Core\Support\Service\AbstractService;
use App\Core\User\Services\UserService;
use RuntimeException;

class ApiClientService extends AbstractService
{
    /**
     * @param ApiClientPersistable $persistable
     * @return ApiClient
     */
    public function create(ApiClientPersistable $persistable)
    {
        (new ClientValidator())->validate($persistable);

        $client = new ApiClient();

        $client->setSecret($persistable->getSecret());
        $client->setAuthenticable($persistable->getAuthenticable());

        $this->entityManager->persist($client);
        $this->entityManager->flush();

        return $client;
    }

    /**
     * @param ClientCredentials $credentials
     * @return ApiClient
     */
    public function getByCredentials(ClientCredentials $credentials)
    {
        $repository = $this->entityManager->getRepository(ApiClient::class);

        /**
         * @var ApiClient $client
         */
        $client = $repository->findOneBy([
            'id' => $credentials->getId(),
            'secret' => $credentials->getSecret(),
        ]);

        return $client;
    }

    /**
     * @param ClientCredentials $credentials
     * @return bool
     */
    public function existsByCredentials(ClientCredentials $credentials)
    {
        return (bool) $this->getByCredentials($credentials);
    }

    /**
     * @param ApiClient $client
     * @return AbstractAuthenticableUserService
     */
    public function getUserService(ApiClient $client): AbstractAuthenticableUserService
    {
        if ($client->getAuthenticable() === null) {
            throw new RuntimeException('Unknown user type for the given API client');
        }

        if ($client->getAuthenticable()->is(AuthenticableUserType::ADMINISTRATOR)) {
            return $this->container->get(AdministratorService::class);
        } elseif ($client->getAuthenticable()->is(AuthenticableUserType::USER)) {
            return $this->container->get(UserService::class);
        } else {
            throw new RuntimeException('Unknown user type for the given API client');
        }
    }
}
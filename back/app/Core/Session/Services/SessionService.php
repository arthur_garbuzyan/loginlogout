<?php

namespace App\Core\Session\Services;

use App\Core\CheckInAndOut\Persistables\CheckInAndOutPersistable;
use App\Core\CheckInAndOut\Services\CheckInAndOutService;
use App\Core\Session\Entities\Session;
use App\Core\Session\Interfaces\AuthenticableUserInterface;
use App\Core\Session\Interfaces\SessionPreferenceInterface;
use App\Core\Session\Interfaces\TokenGeneratorInterface;
use App\Core\Session\Objects\ClientCredentials;
use App\Core\Session\Support\SessionFactory;
use App\Core\Session\Validation\ClientCredentialsValidator;
use App\Core\Session\Validation\CredentialsValidator;
use App\Core\Support\Service\AbstractService;
use App\Core\Session\Objects\Credentials;
use App\Core\User\Entities\User;
use InscopeRest\Validation\PresentableException;
use DateTime;

class SessionService extends AbstractService
{
    /**
     * @var SessionPreferenceInterface
     */
    private $preference;

    /**
     * @var TokenGeneratorInterface
     */
    private $generator;

    /**
     * @param SessionPreferenceInterface $preference
     * @param TokenGeneratorInterface $generator
     */
    public function initialize(SessionPreferenceInterface $preference, TokenGeneratorInterface $generator)
    {
        $this->preference = $preference;
        $this->generator = $generator;
    }

    /**
     * @param Credentials $credentials
     * @param ClientCredentials $clientCredentials
     * @return Session
     */
    public function create(Credentials $credentials, ClientCredentials $clientCredentials)
    {
        /**
         * @var ApiClientService $clientService
         */
        $clientService = $this->container->get(ApiClientService::class);

        (new ClientCredentialsValidator($clientService))->validate($clientCredentials);

        $client = $clientService->getByCredentials($clientCredentials);

        $userService = $clientService->getUserService($client);

        (new CredentialsValidator($userService))->validate($credentials);

        $session = $this->createInMemoryWithUser($userService->getAuthorized($credentials));
        /**
         * @var User $user
         */
        $user = $this->entityManager->find(User::class, $session->getUser()->getId());
        /**
         * @var CheckInAndOutService $checkInAndOut
         */
        $checkInAndOut = $this->container->get(CheckInAndOutService::class);
        /**
         * @var CheckInAndOutPersistable $checkInAndOutPersist
         */
        $checkInAndOutPersist = new CheckInAndOutPersistable();




        //todo
//        $checkInAndOut->create($user->getId());

        $user->setLoggedIn();
        $this->entityManager->persist($session);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $session;
    }

    /**
     * @param AuthenticableUserInterface $user
     * @return Session
     */
    public function createInMemoryWithUser(AuthenticableUserInterface $user = null)
    {
        $session = SessionFactory::createByUser($user);

        $session->setCreatedAt(new DateTime());
        $session->setExpireAt(new DateTime('+' . $this->preference->getLifetime().' minutes'));

        $session->setAccessToken($this->generator->generate());
        $session->setRefreshToken($this->generator->generate());

        return $session;
    }

    /**
     * @param int $id
     * @return Session
     * @throws \Doctrine\ORM\ORMException
     */
    public function refresh(int $id): Session
    {
        /**
         * @var Session $session
         */
        $session = $this->entityManager->getReference(Session::class, $id);

        if ($session === null) {
            throw new PresentableException('The session with the "' . $id . '" ID cannot be refreshed since it has not been found.');
        }

        $session->setAccessToken($this->generator->generate());
        $session->setRefreshToken($this->generator->generate());
        $newDate = $session->getExpireAt()->modify('+' . $this->preference->getLifetime() . ' minutes');
        $session->setExpireAt($newDate);

        $this->entityManager->persist($session);
        $this->entityManager->flush();

        return $session;
    }

    /**
     * @param string $token
     * @return Session|null
     */
    public function getByToken(string $token = null): ?Session
    {
        $repository = $this->entityManager->getRepository(Session::class);
        return $repository->findOneBy(['accessToken' => $token]);
    }

    /**
     * @param int $id
     * @param string|null $token
     * @return bool
     */
    public function existsWithRefreshToken(int $id, string $token = null): bool
    {
        if (!$token) {
            return false;
        }

        $repository = $this->entityManager->getRepository(Session::class);
        return $repository->findOneBy([
            'id' => $id,
            'refreshToken' => $token
        ]) !== null;
    }

    /**
     * @param int $id
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(int $id): void
    {
        /**
         * @var Session $session
         */
        $session = $this->entityManager->getReference(Session::class, $id);
        $user = $this->entityManager->find(User::class, $session->getUser()->getId());
        $user->setLoggedOut();
        $this->entityManager->remove($session);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
<?php

namespace App\Core\Session\Services;

use App\Core\Session\Interfaces\AuthenticableUserInterface;
use App\Core\Session\Objects\Credentials;
use App\Core\Support\Service\AbstractService;

abstract class AbstractAuthenticableUserService extends AbstractService
{
    /**
     * @param Credentials $credentials
     * @return null|AuthenticableUserInterface
     */
    abstract public function getAuthorized(Credentials $credentials): ?AuthenticableUserInterface;

    /**
     * @param Credentials $credentials
     * @return bool
     */
    public function canAuthorize(Credentials $credentials)
    {
        return (bool) $this->getAuthorized($credentials);
    }
}
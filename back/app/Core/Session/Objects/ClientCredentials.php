<?php

namespace App\Core\Session\Objects;

use App\Core\Shared\Properties\IdPropertyTrait;

class ClientCredentials
{
    use IdPropertyTrait;

    /**
     * @var string
     */
    private $secret;
    public function setSecret($secret) { $this->secret = $secret; }
    public function getSecret() { return $this->secret; }
}
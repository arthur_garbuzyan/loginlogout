<?php

namespace App\Core\Session\Enums;

use InscopeRest\Enum\Enum;

class ClientType extends Enum
{
    const ADMINISTRATOR = 'administrator';
    const USER = 'user';
}
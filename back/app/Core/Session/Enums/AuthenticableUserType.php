<?php

namespace App\Core\Session\Enums;

use InscopeRest\Enum\Enum;

class AuthenticableUserType extends Enum
{
    const ADMINISTRATOR = 'administrator';
    const USER = 'user';
}
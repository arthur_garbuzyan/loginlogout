<?php

namespace App\Core\Session\Enums;

use InscopeRest\Enum\Enum;

class SocialNetworkProvider extends Enum
{
    const FACEBOOK = 'facebook';
}
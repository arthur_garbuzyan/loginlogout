<?php

namespace App\Core\Session\Support;

use App\Core\Administrator\Entities\Administrator;
use App\Core\Session\Interfaces\AuthenticableUserInterface;
use App\Core\User\Entities\User;
use App\Core\Session\Entities\AdministratorSession;
use App\Core\Session\Entities\UserSession;
use App\Core\Session\Entities\Session;
use RuntimeException;

class SessionFactory
{
    /**
     * @param AuthenticableUserInterface $user
     * @return Session
     */
    public static function createByUser(AuthenticableUserInterface $user)
    {
        if ($user instanceof Administrator) {
            $session = new AdministratorSession;
        } elseif ($user instanceof User) {
            $session = new UserSession;
        } else {
            throw new RuntimeException('Unknown user type.');
        }

        $session->setUser($user);

        return $session;
    }
}
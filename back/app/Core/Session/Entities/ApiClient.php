<?php

namespace App\Core\Session\Entities;

use App\Core\Session\Enums\AuthenticableUserType;
use App\Core\Shared\Properties\IdPropertyTrait;

class ApiClient
{
    use IdPropertyTrait;

    /**
     * @var string
     */
    private $secret;
    public function setSecret(string $secret): void { $this->secret = $secret; }
    public function getSecret(): ?string { return $this->secret; }

    /**
     * @var AuthenticableUserType
     */
    private $authenticable;
    public function setAuthenticable(AuthenticableUserType $authenticable): void { $this->authenticable = $authenticable; }
    public function getAuthenticable(): ?AuthenticableUserType { return $this->authenticable; }
}
<?php

namespace App\Core\Session\Entities;

use App\Core\Session\Interfaces\AuthenticableUserInterface;
use App\Core\Shared\Properties\CreatedAtPropertyTrait;
use App\Core\Shared\Properties\IdPropertyTrait;
use DateTime;

abstract class Session
{
    use IdPropertyTrait;
    use CreatedAtPropertyTrait;

    /**
     * @var AuthenticableUserInterface
     */
    protected $user;
    public function setUser(AuthenticableUserInterface $user): void { $this->user = $user; }
    public function getUser(): ?AuthenticableUserInterface { return $this->user; }

    /**
     * @var string
     */
    protected $accessToken;
    public function setAccessToken($token) { $this->accessToken = $token; }
    public function getAccessToken() { return $this->accessToken; }

    /**
     * @var string
     */
    protected $refreshToken;
    public function setRefreshToken($token) { $this->refreshToken = $token; }
    public function getRefreshToken() { return $this->refreshToken; }

    /**
     * @var DateTime
     */
    protected $expireAt;
    public function setExpireAt(DateTime $expireAt) { $this->expireAt = clone $expireAt; }
    public function getExpireAt() { return clone $this->expireAt; }
}
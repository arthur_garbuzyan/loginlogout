<?php

namespace App\Core\Session\Validation;

use App\Core\Session\Services\ApiClientService;
use App\Core\Session\Validation\Rules\ClientExists;
use InscopeRest\Validation\AbstractThrowableValidator;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\Rules\Blank;
use InscopeRest\Validation\Rules\Obligate;

class ClientCredentialsValidator extends AbstractThrowableValidator
{
    /**
     * @var ApiClientService
     */
    private $clientService;

    /**
     *
     * @param ApiClientService $clientService
     */
    public function __construct(ApiClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     *
     * @param Binder $binder
     */
    protected function define(Binder $binder) : void
    {
        $binder->bind('id', function (Property $property) {

            $property->addRule(new Obligate())
                ->addRule(new Blank());
        });

        $binder->bind('secret', function (Property $property) {

            $property->addRule(new Obligate())
                ->addRule(new Blank());
        });

        $binder->bind('credentials', ['id', 'secret'], function (Property $property) {
            $property->addRule(new ClientExists($this->clientService));
        });
    }
}
<?php

namespace App\Core\Session\Validation\Rules;

use App\Core\Session\Objects\ClientCredentials;
use App\Core\Session\Services\ApiClientService;
use InscopeRest\Validation\Error;
use InscopeRest\Validation\Rules\AbstractRule;
use InscopeRest\Validation\Value;

class ClientExists extends AbstractRule
{
    /**
     *
     * @var ApiClientService
     */
    private $clientService;

    /**
     *
     * @param ApiClientService $clientService
     */
    public function __construct(ApiClientService $clientService)
    {
        $this->clientService = $clientService;

        $this->setIdentifier('client');
        $this->setMessage('Unknown client');
    }

    /**
     *
     * @param Value $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        list ($id, $secret) = $value->extract();

        $credentials = new ClientCredentials();
        $credentials->setId($id);
        $credentials->setSecret($secret);

        if (! $this->clientService->existsByCredentials($credentials)) {
            return $this->getError();
        }

        return null;
    }
}
<?php

namespace App\Core\Session\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Rules\AbstractRule;
use InscopeRest\Validation\Rules\NullProcessableRuleInterface;
use InscopeRest\Validation\Value;

class PasswordConfirmation extends AbstractRule implements NullProcessableRuleInterface
{
    public function __construct()
    {
        $this->setIdentifier('confirm');
        $this->setMessage('The passwords do not match.');
    }

    /**
     * @param mixed|Value $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        list($password, $confirmation) = $value->extract();

        if($password !== $confirmation) {
            return $this->getError();
        }

        return null;
    }
}
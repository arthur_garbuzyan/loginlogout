<?php

namespace App\Core\Session\Validation\Rules;

use App\Core\Session\Objects\Credentials;
use App\Core\Session\Services\AbstractAuthenticableUserService;
use InscopeRest\Validation\Error;
use InscopeRest\Validation\Rules\AbstractRule;
use InscopeRest\Validation\Value;

class Access extends AbstractRule
{
    /**
     * @var AbstractAuthenticableUserService $userService
     */
    private $userService;

    /**
     * @param AbstractAuthenticableUserService $userService
     */
    public function __construct(AbstractAuthenticableUserService $userService)
    {
        $this->userService = $userService;

        $this->setIdentifier('access');
        $this->setMessage('The user with the provided credentials cannot be found.');
    }

    /**
     * @param Value $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        list ($username, $password) = $value->extract();

        $credentials = new Credentials();
        $credentials->setUsername($username);
        $credentials->setPassword($password);

        if (! $this->userService->canAuthorize($credentials)) {
            return $this->getError();
        }

        return null;
    }
}
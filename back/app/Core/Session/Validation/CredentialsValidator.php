<?php

namespace App\Core\Session\Validation;

use App\Core\Session\Services\AbstractAuthenticableUserService;
use App\Core\Session\Validation\Rules\Access;
use InscopeRest\Validation\AbstractThrowableValidator;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\Rules\Blank;
use InscopeRest\Validation\Rules\Obligate;

class CredentialsValidator extends AbstractThrowableValidator
{
    /**
     * @var AbstractAuthenticableUserService
     */
    private $userService;

    /**
     * @param AbstractAuthenticableUserService $userService
     */
    public function __construct(AbstractAuthenticableUserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Binder $binder
     */
    protected function define(Binder $binder) : void
    {
        $binder->bind('username', function (Property $property) {
            $property->addRule(new Obligate())
                ->addRule(new Blank());
        });

        $binder->bind('password', function (Property $property) {

            $property->addRule(new Obligate())
                ->addRule(new Blank());
        });

        $binder->bind('credentials', ['username', 'password'], function (Property $property) {
            $property->addRule(new Access($this->userService));
        });
    }
}
<?php

namespace App\Core\Session\Validation;

use InscopeRest\Validation\AbstractThrowableValidator;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\Rules\Blank;
use InscopeRest\Validation\Rules\Length;
use InscopeRest\Validation\Rules\Obligate;

class ClientValidator extends AbstractThrowableValidator
{
    /**
     * @param Binder $binder
     * @return void
     */
    protected function define(Binder $binder): void
    {
        $binder->bind('secret', function (Property $property) {
            $property->addRule(new Obligate())
                ->addRule(new Blank())
                ->addRule(new Length(5, 15));
        });
    }
}
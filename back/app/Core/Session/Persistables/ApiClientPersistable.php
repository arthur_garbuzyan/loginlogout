<?php

namespace App\Core\Session\Persistables;

use App\Core\Session\Enums\AuthenticableUserType;

class ApiClientPersistable
{
    /**
     * @var string
     */
    private $secret;
    public function setSecret($secret): void { $this->secret = $secret; }
    public function getSecret(): ?string { return $this->secret; }

    /**
     * @var AuthenticableUserType
     */
    private $authenticable;
    public function setAuthenticable(AuthenticableUserType $authenticable): void { $this->authenticable = $authenticable; }
    public function getAuthenticable(): ?AuthenticableUserType { return $this->authenticable; }
}
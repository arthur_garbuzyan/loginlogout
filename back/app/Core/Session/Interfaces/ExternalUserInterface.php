<?php

namespace App\Core\Session\Interfaces;

use App\Core\Session\Enums\SocialNetworkProvider;

interface ExternalUserInterface
{
    /**
     * @return SocialNetworkProvider
     */
    public function getProvider(): ?SocialNetworkProvider;

    /**
     * @return string
     */
    public function getProviderId(): ?string;
}
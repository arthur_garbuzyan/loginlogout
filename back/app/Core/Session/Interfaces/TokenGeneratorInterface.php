<?php

namespace App\Core\Session\Interfaces;

interface TokenGeneratorInterface
{
    /**
     * @return string
     */
    public function generate();
}
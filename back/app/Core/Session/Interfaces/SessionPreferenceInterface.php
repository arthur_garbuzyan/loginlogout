<?php

namespace App\Core\Session\Interfaces;

interface SessionPreferenceInterface
{
    /**
     * @return int
     */
    public function getLifetime();
}

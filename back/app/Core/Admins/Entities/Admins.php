<?php

namespace App\Core\Admins\Entities;

use App\Core\User\Entities\User;
use App\Core\User\Enums\UserType;

class Admins extends User
{

    /**
     * @return UserType
     */
    public function getType(): UserType
    {
        return new UserType(UserType::ADMIN);
    }
}
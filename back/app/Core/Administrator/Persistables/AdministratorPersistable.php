<?php

namespace App\Core\Administrator\Persistables;

class AdministratorPersistable
{
    /**
     * @var string
     */
    protected $username;
    public function setUsername($username) { $this->username = $username; }
    public function getUsername() { return $this->username; }

    /**
     * @var string
     */
    protected $email;
    public function setEmail($email) { $this->email = $email; }
    public function getEmail() { return $this->email; }

    /**
     * @var string
     */
    protected $password;
    public function setPassword($password) { $this->password = $password; }
    public function getPassword() { return $this->password; }

    /**
     * @var string
     */
    protected $confirmPassword;
    public function setConfirmPassword($confirmPassword) { $this->confirmPassword = $confirmPassword; }
    public function getConfirmPassword() { return $this->confirmPassword; }
}
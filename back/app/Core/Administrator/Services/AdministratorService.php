<?php

namespace App\Core\Administrator\Services;

use App\Core\Administrator\Entities\Administrator;
use App\Core\Administrator\Enums\Role;
use App\Core\Administrator\Persistables\AdministratorPersistable;
use App\Core\Administrator\Validation\AdministratorValidator;
use App\Core\Session\Interfaces\AuthenticableUserInterface;
use App\Core\Session\Interfaces\PasswordEncryptorInterface;
use App\Core\Session\Objects\Credentials;
use App\Core\Session\Services\AbstractAuthenticableUserService;
use Doctrine\ORM\UnexpectedResultException;

class AdministratorService extends AbstractAuthenticableUserService
{
    /**
     * @param AdministratorPersistable $persistable
     * @return Administrator
     */
    public function create(AdministratorPersistable $persistable)
    {
        (new AdministratorValidator($this->container))->validate($persistable);

        $administrator = new Administrator();

        $this->save($persistable, $administrator);

        return $administrator;
    }

    /**
     * @param int $id
     * @param AdministratorPersistable $persistable
     */
    public function update($id, AdministratorPersistable $persistable)
    {
        /**
         * @var Administrator $administrator
         */
        $administrator = $this->entityManager->find(Administrator::class, $id);

        (new AdministratorValidator($this->container))
            ->setId($id)
            ->validate($persistable);

        $this->save($persistable, $administrator);
    }

    /**
     * @param AdministratorPersistable $persistable
     * @param Administrator $entity
     */
    protected function save(AdministratorPersistable $persistable, Administrator $entity) : void
    {
        $this->transfer($persistable, $entity, [
            'ignore' => [
                'password',
                'confirmPassword'
            ]
        ]);

        if ($password = $persistable->getPassword()) {
            /**
             * @var PasswordEncryptorInterface $encryptor
             */
            $encryptor = $this->container->get(PasswordEncryptorInterface::class);
            $password = $encryptor->encrypt($password);
            $entity->setPassword($password);
        }

        if ($entity->getId() === null) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }

    /**
     *
     * @param Credentials $credentials
     * @return null|AuthenticableUserInterface
     */
    public function getAuthorized(Credentials $credentials): ?AuthenticableUserInterface
    {
        $builder = $this->entityManager->createQueryBuilder();

        $builder->select('a')
            ->from(Administrator::class, 'a')
            ->where($builder->expr()->orX(
                $builder->expr()->eq('a.username', ':username')
            ))->setParameter('username', $credentials->getUsername());

        /**
         * @var Administrator $user
         */
        try {
            $user = $builder->getQuery()->getSingleResult();
        } catch (UnexpectedResultException $e) {
            return null;
        }

        /**
         * @var PasswordEncryptorInterface $encryptor
         */
        $encryptor = $this->container->get(PasswordEncryptorInterface::class);

        if (!$encryptor->verify($credentials->getPassword(), $user->getPassword())) {
            return null;
        }

        return $user;
    }

    /**
     * @param int $id
     */
    public function delete($id)
    {
        /**
         * @var Administrator $administrator
         */
        $administrator = $this->entityManager->find(Administrator::class, $id);

        $this->entityManager->remove($administrator);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function exists($id): bool
    {
        $builder = $this->entityManager->createQueryBuilder();

        $builder->select($builder->expr()->count('a'))
            ->from(Administrator::class, 'a')
            ->where($builder->expr()->neq('a.role', ':exRole'))
            ->setParameter('exRole', Role::SUPERUSER)
            ->andWhere($builder->expr()->eq('a.id', ':id'))
            ->setParameter('id', $id);

        return $builder->getQuery()->getSingleScalarResult() > 0;
    }
}
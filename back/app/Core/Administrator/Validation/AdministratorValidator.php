<?php

namespace App\Core\Administrator\Validation;

use App\Core\Administrator\Entities\Administrator;
use App\Core\Administrator\Validation\Rules\Username;
use App\Core\Session\Validation\Rules\Password;
use App\Core\Session\Validation\Rules\PasswordConfirmation;
use App\Core\Shared\Validation\Rules\UniqueProperty;
use App\Core\Support\Service\ContainerInterface;
use InscopeRest\Validation\AbstractThrowableValidator;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\Rules\Blank;
use InscopeRest\Validation\Rules\Email;
use InscopeRest\Validation\Rules\Obligate;
use Doctrine\ORM\EntityManagerInterface;

class AdministratorValidator extends AbstractThrowableValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var int
     */
    private $id;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->entityManager = $container->get(EntityManagerInterface::class);
    }

    /**
     * @param int $id
     * @return self
     */
    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param Binder $binder
     * @return void
     */
    protected function define(Binder $binder): void
    {
        $binder->bind('email', function (Property $property) {
            $unique = new UniqueProperty($this->entityManager, Administrator::class, 'email');

            if ($this->id) {
                $unique->ignore($this->id);
            }

            $property->addRule(new Blank())
                ->addRule(new Email())
                ->addRule($unique);
        });

        $binder->bind('username', function (Property $property) {
            $unique = new UniqueProperty($this->entityManager, Administrator::class, 'username');

            if ($this->id) {
                $unique->ignore($this->id);
            }

            $property->addRule(new Blank())
                ->addRule(new Username())
                ->addRule($unique);
        });

        if ($this->id === null) {
            $binder->bind('password', function (Property $property) {
                $property->addRule(new Obligate());
            });
            $binder->bind('password', ['password', 'confirmPassword'], function (Property $property) {
                $property->addRule(new PasswordConfirmation());
            });
            $binder->bind('email', function (Property $property) {
                $property->addRule(new Obligate());
            });
            $binder->bind('username', function (Property $property) {
                $property->addRule(new Obligate());
            });
        }

        $binder->bind('password', function (Property $property) {
            $property->addRule(new Password())
                ->addRule(new Blank());
        });
    }
}
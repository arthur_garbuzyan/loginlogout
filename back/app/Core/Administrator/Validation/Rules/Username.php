<?php

namespace App\Core\Administrator\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Rules\AbstractRule;
use InscopeRest\Validation\Rules\Length;
use InscopeRest\Validation\Rules\Regex;

class Username extends AbstractRule
{
    const ALLOWED_CHARACTERS = '[a-zA-Z0-9\._@\-]+';

    public function __construct()
    {
        $this->setIdentifier('format');
        $this->setMessage('The username can contain only letters, digits, "@", ".", "-" or "_" '
            . 'must be at least 5 and at most 50 characters long.');
    }

    /**
     * @param string $value
     * @return Error|null
     */
    public function check($value): ?Error
    {
        $error = (new Regex('/^'.static::ALLOWED_CHARACTERS.'$/'))->check($value);

        if ($error) {
            return $this->getError();
        }

        $error = (new Length(5, 255))->check($value);

        if ($error) {
            return $this->getError();
        }

        return null;
    }
}
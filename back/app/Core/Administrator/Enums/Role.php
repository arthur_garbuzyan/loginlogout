<?php

namespace App\Core\Administrator\Enums;

use InscopeRest\Enum\Enum;

class Role extends Enum
{
    const SUPERUSER = 'superuser';
}
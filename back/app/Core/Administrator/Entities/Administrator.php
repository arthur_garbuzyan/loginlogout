<?php

namespace App\Core\Administrator\Entities;

use App\Core\Administrator\Enums\Role;
use App\Core\Session\Interfaces\AuthenticableUserInterface;
use App\Core\Shared\Properties\IdPropertyTrait;

class Administrator implements AuthenticableUserInterface
{
    use IdPropertyTrait;

    /**
     * @var string
     */
    protected $username;
    public function setUsername(string $username): void { $this->username = $username; }
    public function getUsername(): string { return $this->username; }

    /**
     * @var string
     */
    protected $password;
    public function setPassword(string $password): void { $this->password = $password; }
    public function getPassword(): string { return $this->password; }

    /**
     * @var Role
     */
    private $role;
    public function setRole(Role $role) { $this->role = $role; }
    public function getRole() { return $this->role; }
}
<?php

namespace App\Core\Facebook\Interfaces;

use App\Core\Facebook\Objects\FacebookUserData;

interface FacebookSdkInterface
{
    /**
     * @param string $id
     * @param string $token
     * @return bool
     */
    public function isAuthorized(string $id, string $token): bool;

    /**
     * @param string $id
     * @param string $token
     * @return FacebookUserData|null
     */
    public function getUserData(string $id, string $token): ?FacebookUserData;
}
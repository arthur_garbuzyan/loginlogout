<?php

namespace App\Core\Facebook\Interfaces;

use App\Core\Facebook\Objects\FacebookUserData;
use App\Core\Session\Interfaces\AuthenticableUserInterface;

interface FacebookProviderServiceInterface
{
    /**
     * @param string $id
     * @return AuthenticableUserInterface|null
     */
    public function getByFacebookId(string $id): ?AuthenticableUserInterface;

    /**
     * @param FacebookUserData $data
     * @return AuthenticableUserInterface
     */
    public function createWithFacebookData(FacebookUserData $data): AuthenticableUserInterface;
}
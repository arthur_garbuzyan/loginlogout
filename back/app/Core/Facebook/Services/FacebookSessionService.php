<?php

namespace App\Core\Facebook\Services;

use App\Core\Facebook\Interfaces\FacebookProviderServiceInterface;
use App\Core\Facebook\Interfaces\FacebookSdkInterface;
use App\Core\Facebook\Objects\FacebookCredentials;
use App\Core\Facebook\Validation\FacebookCredentialsValidator;
use App\Core\Session\Entities\Session;
use App\Core\Session\Objects\ClientCredentials;
use App\Core\Session\Services\ApiClientService;
use App\Core\Session\Services\SessionService;
use App\Core\Session\Validation\ClientCredentialsValidator;
use App\Core\Support\Service\AbstractService;
use InscopeRest\Validation\PresentableException;

class FacebookSessionService extends AbstractService
{
    /**
     * @param FacebookCredentials $credentials
     * @param ClientCredentials $clientCredentials
     * @return Session
     */
    public function create(FacebookCredentials $credentials, ClientCredentials $clientCredentials): Session
    {
        /**
         * @var ApiClientService $clientService
         */
        $clientService = $this->container->get(ApiClientService::class);

        (new ClientCredentialsValidator($clientService))->validate($clientCredentials);

        $apiClient = $clientService->getByCredentials($clientCredentials);
        $userService = $clientService->getUserService($apiClient);

        if (!($userService instanceof FacebookProviderServiceInterface)) {
            throw new PresentableException('API Client user type does not support login via Facebook.');
        }


        /**
         * @var FacebookSdkInterface $facebookApi
         */
        $facebookApi = $this->container->get(FacebookSdkInterface::class);

        //validate facebook credentials using Facebook Graph API.
        (new FacebookCredentialsValidator($facebookApi))->validate($credentials);

        //check if this facebook user already is in the database
        $user = $userService->getByFacebookId($credentials->getId());

        if ($user === null) {
            //if not, request more data from Facebook and create a new user.
            $data = $facebookApi->getUserData($credentials->getId(), $credentials->getToken());
            $user = $userService->createWithFacebookData($data);
        }

        /**
         * @var SessionService $sessionService
         */
        $sessionService = $this->container->get(SessionService::class);

        //create a session for the new user
        $session = $sessionService->createInMemoryWithUser($user);

        $this->entityManager->persist($session);
        $this->entityManager->flush();

        return $session;
    }
}
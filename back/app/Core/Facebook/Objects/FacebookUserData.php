<?php

namespace App\Core\Facebook\Objects;

class FacebookUserData
{
    /**
     * @var string
     */
    private $id;
    public function setId(string $id): void { $this->id = $id; }
    public function getId(): ?string { return $this->id; }

    /**
     * @var string
     */
    private $name;
    public function setName(string $name): void { $this->name = $name; }
    public function getName(): ?string { return $this->name; }
}
<?php

namespace App\Core\Facebook\Objects;

class FacebookCredentials
{
    /**
     * @var string
     */
    private $id;
    public function setId(string $id): void { $this->id = $id; }
    public function getId(): ?string { return $this->id; }

    /**
     * @var string
     */
    private $token;
    public function setToken(string $token): void { $this->token = $token; }
    public function getToken(): ?string { return $this->token; }
}
<?php

namespace App\Core\Facebook\Validation;

use App\Core\Facebook\Interfaces\FacebookSdkInterface;
use App\Core\Facebook\Validation\Rules\FacebookAccess;
use InscopeRest\Validation\AbstractThrowableValidator;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\Rules\Obligate;

class FacebookCredentialsValidator extends AbstractThrowableValidator
{
    /**
     * @var FacebookSdkInterface
     */
    private $facebookApi;

    /**
     * @param FacebookSdkInterface $facebookApi
     */
    public function __construct(FacebookSdkInterface $facebookApi)
    {
        $this->facebookApi = $facebookApi;
    }

    /**
     * @param Binder $binder
     * @return void
     */
    protected function define(Binder $binder): void
    {
        $binder->bind('id', function (Property $property) {
            $property->addRule(new Obligate());
        });

        $binder->bind('token', function (Property $property) {
            $property->addRule(new Obligate());
        });

        $binder->bind('credentials', ['id', 'token'], function (Property $property) {
            $property->addRule(new FacebookAccess($this->facebookApi));
        });
    }
}
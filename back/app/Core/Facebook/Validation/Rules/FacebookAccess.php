<?php

namespace App\Core\Facebook\Validation\Rules;

use App\Core\Facebook\Interfaces\FacebookSdkInterface;
use InscopeRest\Validation\Error;
use InscopeRest\Validation\Rules\AbstractRule;
use InscopeRest\Validation\Value;

class FacebookAccess extends AbstractRule
{
    /**
     * @var FacebookSdkInterface
     */
    private $facebookApi;

    /**
     * @param FacebookSdkInterface $facebookApi
     */
    public function __construct(FacebookSdkInterface $facebookApi)
    {
        $this->facebookApi = $facebookApi;

        $this->setIdentifier('access');
        $this->setMessage('Could not verify user\'s authorization on Facebook.');
    }

    /**
     * @param Value $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        list ($id, $token) = $value->extract();

        if (!$this->facebookApi->isAuthorized($id, $token)) {
            return $this->getError();
        }

        return null;
    }
}
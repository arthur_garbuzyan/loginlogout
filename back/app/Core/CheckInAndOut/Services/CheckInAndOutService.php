<?php


namespace App\Core\CheckInAndOut\Services;

use App\Core\CheckInAndOut\Entities\CheckInAndOut;
use App\Core\CheckInAndOut\Persistables\CheckInAndOutPersistable;
use App\Core\Support\Service\AbstractService;
use App\Core\User\Entities\User;
use App\Core\User\Persistables\UserPersistable;

class CheckInAndOutService extends AbstractService
{
    /**
     * @param int $userId
     * @param CheckInAndOutPersistable $persistable
     * @return checkInAndOut
     */
    public function create(int $userId, UserPersistable $persistable): UserPersistable
    {

        /**
         * @var User $user0+-
         */
        $user = $this->entityManager->find(User::class, $userId);


        $checkInAndOut = new CheckInAndOut();
        $checkInAndOut->setUser($user);

        $this->save($persistable, $checkInAndOut);

        return $checkInAndOut;
    }

    protected function save(UserPersistable $persistable, CheckInAndOut $entity): void
    {
        $this->transfer($persistable, $entity);

        if ($entity->getId() === null) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }
}
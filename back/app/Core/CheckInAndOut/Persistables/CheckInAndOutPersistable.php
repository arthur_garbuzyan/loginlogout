<?php

namespace App\Core\CheckInAndOut\Persistables;

use DateTime;

class CheckInAndOutPersistable
{
    /**
     * @var DateTime
     */
    protected $loggedIn;
    public function setLoggedIn(){$this->loggedIn = new DateTime();}
    public function getLoggedIn(){return $this->loggedIn;}

    /**
     * @var
     */
    protected $loggedOut;
    public function getLoggedOut(){return $this->loggedOut;}
    public function setLoggedOut(): void{$this->loggedOut = new DateTime();}

    protected $reason;
    public function getReason(){return $this->reason;}
    public function setReason($reason): void{$this->reason = $reason;}

    protected $subject;
    public function getSubject(){return $this->subject;}
    public function setSubject($subject): void{$this->subject = $subject;}
}
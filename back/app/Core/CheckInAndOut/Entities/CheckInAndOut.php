<?php

namespace App\Core\CheckInAndOut\Entities;

use App\Core\Shared\Properties\CreatedAtPropertyTrait;
use App\Core\Shared\Properties\IdPropertyTrait;
use App\Core\Shared\Properties\UpdatedAtPropertyTrait;
use App\Core\User\Entities\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;

class CheckInAndOut
{
    use IdPropertyTrait;
    use CreatedAtPropertyTrait;
    use UpdatedAtPropertyTrait;

    protected $reason;
    public function getReason(){return $this->reason;}
    public function setReason($reason): void{$this->reason = $reason;}

    protected $subject;
    public function getSubject(){return $this->subject;}
    public function setSubject($subject): void{$this->subject = $subject;}

    /**
     * @var User
     */
    private $user;
    public function setUser(User $user): void { $this->user = $user; }
    public function getUser(): User { return $this->user; }

    /**
     * @var DateTime
     */
    protected $loggedIn;
    public function setLoggedIn(){$this->loggedIn = new DateTime();}
    public function getLoggedIn(){return $this->loggedIn;}

    /**
     * @var
     */
    protected $loggedOut;
    public function getLoggedOut(){return $this->loggedOut;}
    public function setLoggedOut(): void{$this->loggedOut = new DateTime();}

    public function __construct()
    {
        $this->loggedIn = new DateTime();
        $this->checkInAndOut = new ArrayCollection();
    }
}
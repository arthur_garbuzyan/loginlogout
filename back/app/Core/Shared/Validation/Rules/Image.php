<?php

namespace App\Core\Shared\Validation\Rules;

use App\Core\Shared\Interfaces\ImageManagerInterface;
use App\Core\Shared\Persistables\FilePersistable;
use InscopeRest\Validation\Error;
use InscopeRest\Validation\Rules\AbstractRule;

class Image extends AbstractRule
{
    /**
     * @var ImageManagerInterface
     */
    private $imageManager;

    public function __construct(ImageManagerInterface $imageManager)
    {
        $this->imageManager = $imageManager;

        $this->setIdentifier('image');
        $this->setMessage('File is not an image.');
    }

    /**
     *
     * @param mixed $value
     * @return Error|null
     */
    public function check($value): ?Error
    {
        if (! ($value instanceof FilePersistable)) {
            return $this->getError();
        }

        if (! $this->imageManager->validate($value)) {
            return $this->getError();
        }

        return null;
    }
}
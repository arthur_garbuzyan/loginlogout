<?php

namespace App\Core\Shared\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Rules\AbstractRule;
use InscopeRest\Validation\Value;
use Doctrine\ORM\EntityManagerInterface;

class UniqueProperty extends AbstractRule
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     *
     * @var string
     */
    private $entityClass;

    /**
     *
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $idColumn = 'id';

    /**
     * @var int
     */
    private $ignore;

    /**
     *
     * @param EntityManagerInterface $entityManager
     * @param string $entityClass
     * @param string $property
     */
    public function __construct(EntityManagerInterface $entityManager, string $entityClass, string $property)
    {
        $this->entityManager = $entityManager;
        $this->entityClass = $entityClass;
        $this->property = $property;

        $this->setIdentifier('unique');
        $this->setMessage('Value must be unique');
    }

    /**
     * @param int $id
     * @param string $idColumn
     * @return self
     */
    public function ignore(int $id, string $idColumn = 'id') : self
    {
        $this->ignore = $id;
        $this->idColumn = $idColumn;
        return $this;
    }

    /**
     * @param mixed|Value $value
     * @return Error|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function check($value) : ?Error
    {
        $builder = $this->entityManager->createQueryBuilder();
        $builder->select($builder->expr()->countDistinct('e'))
            ->from($this->entityClass, 'e')
            ->where($builder->expr()->eq('e.'.$this->property, ':value'))
            ->setParameter('value', $value);

        if ($this->ignore) {
            $builder->andWhere($builder->expr()->neq('e.'.$this->idColumn, ':id'))
                ->setParameter('id', $this->ignore);
        }

        $exists = $builder->getQuery()->getSingleScalarResult() > 0;

        if ($exists) {
            return $this->getError();
        }

        return null;
    }
}
<?php

namespace App\Core\Shared\Options;

class DefaultFetchOptions
{
    use CriteriaAwareTrait;
    use PaginationAwareTrait;
    use SortablesAwareTrait;
}
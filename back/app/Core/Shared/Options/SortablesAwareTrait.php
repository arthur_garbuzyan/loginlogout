<?php

namespace App\Core\Shared\Options;

use App\Core\Support\Sorting\Sortable;

trait SortablesAwareTrait
{
    /**
     * @var Sortable[]
     */
    private $sortables = [];

    /**
     * @param Sortable[] $sortables
     */
    public function setSortables(array $sortables)
    {
        $this->sortables = $sortables;
    }

    /**
     * @return Sortable[]
     */
    public function getSortables()
    {
        return $this->sortables;
    }
}
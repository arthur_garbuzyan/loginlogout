<?php

namespace App\Core\Shared\Interfaces;

use App\Core\Shared\Objects\Dimensions;

interface ImagePreferencesInterface
{
    /**
     * @param string $key
     * @return Dimensions
     */
    public function getDimensions(string $key): Dimensions;
}
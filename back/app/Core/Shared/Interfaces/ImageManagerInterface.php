<?php

namespace App\Core\Shared\Interfaces;

use App\Core\Shared\Persistables\FilePersistable;

interface ImageManagerInterface
{
    /**
     * @param FilePersistable $file
     * @return bool
     */
    public function validate(FilePersistable $file): bool;
}
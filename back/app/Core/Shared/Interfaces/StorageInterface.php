<?php

namespace App\Core\Shared\Interfaces;

interface StorageInterface
{
    /**
     * @param string|resource $location
     * @param string $dest
     */
    public function putFileIntoRemoteStorage($location, string $dest): void;

    /**
     * @param array $locations
     */
    public function removeFilesFromRemoteStorage(array $locations): void;

    /**
     * @param string|resource $location
     * @return bool
     */
    public function isFileReadable($location): bool;

    /**
     * @param string|resource $location
     * @return string
     */
    public function getFilePublicUri($location): string;

    /**
     * @param string|resource $location
     * @return string
     */
    public function getFileName($location): string;

    /**
     * @param string|resource $location
     * @return string
     */
    public function getFileFormat($location): string;
}
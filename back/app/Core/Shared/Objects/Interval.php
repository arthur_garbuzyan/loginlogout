<?php

namespace App\Core\Shared\Objects;

class Interval
{
    /**
     * @var float
     */
    private $min;
    public function setMin(float $min): float { $this->min = $min; }
    public function getMin(): ?float { return $this->min; }

    /**
     * @var float
     */
    private $max;
    public function setMax(float $max): void { $this->max = $max; }
    public function getMax(): ?float { return $this->max; }
}
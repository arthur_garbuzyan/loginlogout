<?php

namespace App\Core\Shared\Objects;

use DateTime;

class DateInterval
{
    /**
     * @var DateTime
     */
    private $from;
    public function setFrom(DateTime $from): void { $this->from = $from; }
    public function getFrom(): ?DateTime { return $this->from; }

    /**
     * @var DateTime
     */
    private $to;
    public function setTo(DateTime $to): void { $this->to = $to; }
    public function getTo(): ?DateTime { return $this->to; }
}
<?php

namespace App\Core\Shared\Objects;

class Dimensions
{
    /**
     * Dimensions constructor.
     * @param int|null $width
     * @param int|null $height
     */
    public function __construct(int $width = null, int $height = null)
    {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @var int
     */
    private $width;
    public function setWidth(int $width): void { $this->width = $width; }
    public function getWidth(): ?int { return $this->width; }

    /**
     * @var int
     */
    private $height;
    public function setHeight(int $height): void { $this->height = $height; }
    public function getHeight(): ?int { return $this->height; }
}
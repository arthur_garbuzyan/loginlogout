<?php

namespace App\Core\Shared\Objects;

class File
{
    /**
     * @var string
     */
    private $location;
    public function getLocation(): string { return $this->location; }

    /**
     * File constructor.
     * @param string $location
     */
    public function __construct(string $location)
    {
        $this->location = $location;
    }
}
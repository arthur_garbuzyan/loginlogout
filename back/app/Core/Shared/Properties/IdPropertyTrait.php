<?php

namespace App\Core\Shared\Properties;

trait IdPropertyTrait
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
<?php

namespace App\Core\Shared\Properties;

use DateTime;

trait UpdatedAtPropertyTrait
{
    /**
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @param DateTime $datetime
     */
    public function setUpdatedAt(DateTime $datetime)
    {
        $this->updatedAt = $datetime;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
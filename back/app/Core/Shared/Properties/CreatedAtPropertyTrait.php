<?php

namespace App\Core\Shared\Properties;

use DateTime;

trait CreatedAtPropertyTrait
{
    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @param DateTime $datetime
     */
    public function setCreatedAt(DateTime $datetime)
    {
        $this->createdAt = $datetime;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
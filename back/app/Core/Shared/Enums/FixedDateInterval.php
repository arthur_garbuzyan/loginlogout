<?php

namespace App\Core\Shared\Enums;

use InscopeRest\Enum\Enum;

class FixedDateInterval extends Enum
{
    const TODAY = 'today';
    const YESTERDAY = 'yesterday';
    const THIS_WEEK = 'week';
    const THIS_MONTH = 'month';
}
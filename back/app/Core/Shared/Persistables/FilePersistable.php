<?php

namespace App\Core\Shared\Persistables;

class FilePersistable
{
    /**
     * @var string
     */
    private $location;
    public function setLocation(string $location): void { $this->location = $location; }
    public function getLocation(): ?string { return $this->location; }

    /**
     * @var string
     */
    private $suggestedName;
    public function setSuggestedName(string $suggestedName): void { $this->suggestedName = $suggestedName; }
    public function getSuggestedName(): ?string { return $this->suggestedName; }

    /**
     * @var string
     */
    private $mimeType;
    public function setMimeType(string $mimeType): void { $this->mimeType = $mimeType; }
    public function getMimeType(): ?string { return $this->mimeType; }
}
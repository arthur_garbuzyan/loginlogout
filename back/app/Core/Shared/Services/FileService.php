<?php

namespace App\Core\Shared\Services;

use App\Core\Shared\Interfaces\StorageInterface;
use App\Core\Shared\Objects\File;
use App\Core\Shared\Persistables\FilePersistable;
use App\Core\Support\Service\AbstractService;

class FileService extends AbstractService
{
    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * @param StorageInterface $storage
     */
    public function initialize(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param FilePersistable $persistable
     * @param string $directory
     * @return File
     */
    public function store(FilePersistable $persistable, string $directory = '/tmp'): File
    {
        $remotePath = $directory . DIRECTORY_SEPARATOR . $persistable->getSuggestedName();
        $this->storage->putFileIntoRemoteStorage($persistable->getLocation(), $remotePath);

        return new File($remotePath);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: grigo
 * Date: 1/5/2020
 * Time: 8:14 PM
 */

namespace App\Core\Teachers\Entities;


use App\Core\User\Entities\User;
use App\Core\User\Enums\UserType;

class Teachers extends User
{

    /**
     * @return UserType
     */
    public function getType(): UserType
    {
        return new UserType(UserType::TEACHER);
    }
}
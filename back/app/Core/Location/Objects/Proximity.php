<?php

namespace App\Core\Location\Objects;

class Proximity
{
    /**
     * @var float
     */
    private $radius = 0;
    public function setRadius(float $radius): void { $this->radius = $radius; }
    public function getRadius(): float { return $this->radius; }

    /**
     * @var Coordinates
     */
    private $coordinates;
    public function setCoordinates(Coordinates $coordinates): void { $this->coordinates = $coordinates; }
    public function getCoordinates(): ?Coordinates { return $this->coordinates; }
}
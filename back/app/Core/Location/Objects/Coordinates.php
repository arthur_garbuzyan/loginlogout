<?php

namespace App\Core\Location\Objects;

class Coordinates
{
    /**
     * @var float
     */
    private $longitude;
    public function setLongitude(float $longitude): void { $this->longitude = $longitude; }
    public function getLongitude(): ?float { return $this->longitude; }

    /**
     * @var float
     */
    private $latitude;
    public function setLatitude(float $latitude): void { $this->latitude = $latitude; }
    public function getLatitude(): ?float { return $this->latitude; }
}
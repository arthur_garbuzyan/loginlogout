<?php

namespace App\Core\Location\Properties;

trait LocationPropertiesTrait
{
    /**
     * @var string
     */
    private $address;
    public function setAddress(string $address): void { $this->address = $address; }
    public function getAddress(): ?string { return $this->address; }

    /**
     * @var string
     */
    private $country;
    public function setCountry(string $country): void { $this->country = $country; }
    public function getCountry(): ?string { return $this->country; }

    /**
     * @var string
     */
    private $city;
    public function setCity(string $city): void { $this->city = $city; }
    public function getCity(): ?string { return $this->city; }

    /**
     * @var string
     */
    private $state;
    public function setState(string $state): void { $this->state = $state; }
    public function getState(): ?string { return $this->state; }

    /**
     * @var string
     */
    private $postcode;
    public function setPostcode(string $postcode): void { $this->postcode = $postcode; }
    public function getPostcode(): ?string { return $this->postcode; }

    /**
     * @var float
     */
    private $longitude;
    public function setLongitude(float $longitude): void { $this->longitude = $longitude; }
    public function getLongitude(): ?float { return $this->longitude; }

    /**
     * @var float
     */
    private $latitude;
    public function setLatitude(float $latitude): void { $this->latitude = $latitude; }
    public function getLatitude(): ?float { return $this->latitude; }
}
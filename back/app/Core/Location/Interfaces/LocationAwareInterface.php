<?php

namespace App\Core\Location\Interfaces;

interface LocationAwareInterface
{
    /**
     * @param string $address
     */
    public function setAddress(string $address): void;

    /**
     * @return null|string
     */
    public function getAddress(): ?string;

    /**
     * @param string $country
     */
    public function setCountry(string $country): void;

    /**
     * @return null|string
     */
    public function getCountry(): ?string;

    /**
     * @param string $city
     */
    public function setCity(string $city): void;

    /**
     * @return null|string
     */
    public function getCity(): ?string;

    /**
     * @param string $state
     */
    public function setState(string $state): void;

    /**
     * @return null|string
     */
    public function getState(): ?string;

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude): void;

    /**
     * @return float|null
     */
    public function getLongitude(): ?float;

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude): void;

    /**
     * @return float|null
     */
    public function getLatitude(): ?float;
}
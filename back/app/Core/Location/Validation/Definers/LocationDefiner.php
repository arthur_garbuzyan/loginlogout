<?php

namespace App\Core\Location\Validation\Definers;

use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\Rules\Blank;
use InscopeRest\Validation\Rules\FloatCast;
use InscopeRest\Validation\Rules\Obligate;

class LocationDefiner
{
    /**
     * @var bool
     */
    private $obligate = true;

    /**
     * @param Binder $binder
     */
    public function define(Binder $binder) : void
    {
        $binder->bind('address', function (Property $property) {
            if ($this->obligate) {
                $property->addRule(new Obligate());
            }
            $property->addRule(new Blank());
        });

        $binder->bind('city', function (Property $property) {
            if ($this->obligate){
                $property->addRule(new Obligate());
            }
            $property->addRule(new Blank());
        });

        $binder->bind('country', function (Property $property) {
            if ($this->obligate){
                $property->addRule(new Obligate());
            }
            $property->addRule(new Blank());
        });

        $binder->bind('state', function (Property $property) {
            if ($this->obligate){
                $property->addRule(new Obligate());
            }
            $property->addRule(new Blank());
        });

        $binder->bind('longitude', function (Property $property) {
            if ($this->obligate){
                $property->addRule(new Obligate());
            }
            $property->addRule(new FloatCast());
        });

        $binder->bind('latitude', function (Property $property) {
            if ($this->obligate){
                $property->addRule(new Obligate());
            }
            $property->addRule(new FloatCast());
        });
    }

    /**
     * @param bool $flag
     * @return $this
     */
    public function setObligate(bool $flag): self
    {
        $this->obligate = $flag;
        return $this;
    }
}
<?php

namespace App\Core\Support\Service;

use App\Core\Shared\Interfaces\NotifierInterface;
use InscopeRest\Converter\Transferer\Transferer;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->entityManager = $container->get(EntityManagerInterface::class);

        if (method_exists($this, 'initialize')) {
            $this->container->invoke([$this, 'initialize']);
        }
    }

    /**
     *
     * @param object $src
     * @param object $dest
     * @param array $config
     * @return object
     */
    protected function transfer($src, $dest, array $config = [])
    {
        $transferer = new Transferer($config);
        return $transferer->transfer($src, $dest);
    }

    /**
     * @param object $notification
     */
    protected function notify($notification) : void
    {
        /**
         * @var NotifierInterface $notifier
         */
        $notifier = $this->container->get(NotifierInterface::class);

        $notifier->notify($notification);
    }
}
<?php

namespace App\Core\Support\Criteria;

class Criteria
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var Constraint
     */
    private $constraint;

    /**
     * @param string $property
     * @param Constraint $constraint
     * @param mixed $value
     */
    public function __construct(string $property, Constraint $constraint, $value)
    {
        $this->property = $property;
        $this->constraint = $constraint;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getProperty() : string
    {
        return $this->property;
    }

    /**
     * @return Constraint
     */
    public function getConstraint() : Constraint
    {
        return $this->constraint;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
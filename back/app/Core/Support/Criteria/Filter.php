<?php

namespace App\Core\Support\Criteria;

use App\Core\Support\Criteria\Sorting\Sorter;
use Doctrine\ORM\QueryBuilder;
use RuntimeException;
use App\Core\Support\Criteria\Sorting\ResolverInterface as SortingResolverInterface;

class Filter extends Mutator
{
    /**
     * @param QueryBuilder $builder
     * @param Criteria[] $criteria
     * @param ResolverInterface $resolver
     * @return self
     */
    public function apply(QueryBuilder $builder, array $criteria, ResolverInterface $resolver) : self
    {
        foreach ($criteria as $single) {
            $this->resolve($builder, $single, $resolver);
        }

        $this->applyJoins($builder);

        return $this;
    }

    /**
     * @param QueryBuilder $builder
     * @param Criteria $criteria
     * @param ResolverInterface $resolver
     */
    private function resolve(QueryBuilder $builder, Criteria $criteria, ResolverInterface $resolver) : void
    {
        if ($resolver->canResolve($criteria)) {
            $result = $resolver->resolve($builder, $criteria);

            $this->processResult($result);

            return;
        }

        throw new RuntimeException(
            'Unable to resolve the criteria with the "'.$criteria->getConstraint()
            .'" constraint and the "'.$criteria->getProperty().'" property.'
        );
    }

    /**
     * @param QueryBuilder $builder
     * @param array $sortables
     * @param SortingResolverInterface $resolver
     */
    public function withSorter(QueryBuilder $builder, array $sortables, SortingResolverInterface $resolver) : void
    {
        (new Sorter($this->context))->apply($builder, $sortables, $resolver);
    }
}
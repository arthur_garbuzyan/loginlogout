<?php

namespace App\Core\Support\Criteria;

class Context
{
    /**
     * @var Join[]
     */
    private $joins = [];

    /**
     * @param Join $join
     */
    public function addJoin(Join $join) : void
    {
        $this->joins[(string) $join] = ['reference' => $join, 'used' => false];
    }

    /**
     * @param Join $join
     */
    public function useJoin(Join $join) : void
    {
        $this->joins[(string) $join]['used'] = true;
    }

    /**
     * @param Join $join
     * @return bool
     */
    public function hasJoin(Join $join) : bool
    {
        return isset($this->joins[(string) $join]);
    }

    /**
     * @return Join[]
     */
    public function getUnusedJoins() : array
    {
        return array_map(function($data){
            return $data['reference'];
        }, array_filter($this->joins, function($data){
            return $data['used'] == false;
        }));
    }
}
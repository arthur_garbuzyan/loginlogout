<?php

namespace App\Core\Support\Criteria\Sorting;

use App\Core\Support\Criteria\Join;
use Doctrine\ORM\QueryBuilder;

interface ResolverInterface
{
    /**
     * @param Sortable $sortable
     * @return bool
     */
    public function canResolve(Sortable $sortable) : bool;

    /**
     * @param QueryBuilder $builder
     * @param Sortable $sortable
     * @return Join|Join[]|null
     */
    public function resolve(QueryBuilder $builder, Sortable $sortable);
}
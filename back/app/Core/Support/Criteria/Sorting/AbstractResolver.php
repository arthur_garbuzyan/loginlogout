<?php

namespace App\Core\Support\Criteria\Sorting;

use App\Core\Support\Criteria\Join;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractResolver implements ResolverInterface
{
    /**
     * @param Sortable $sortable
     * @return bool
     */
    public function canResolve(Sortable $sortable) : bool
    {
        return method_exists($this, $this->getMethod($sortable->getProperty()));
    }

    /**
     * @param string $property
     * @return string
     */
    private function getMethod(string $property) : string
    {
        return 'by' . str_replace('.', '', $property);
    }


    /**
     * @param QueryBuilder $builder
     * @param Sortable $sortable
     * @return Join|Join[]|null
     */
    public function resolve(QueryBuilder $builder, Sortable $sortable)
    {
        $method = $this->getMethod($sortable->getProperty());

        return call_user_func([
            $this,
            $method
        ], $builder, (string) $sortable->getDirection());
    }
}
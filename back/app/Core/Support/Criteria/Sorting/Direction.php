<?php

namespace App\Core\Support\Criteria\Sorting;

use InscopeRest\Enum\Enum;

class Direction extends Enum
{
    const ASC = 'asc';
    const DESC = 'desc';
}
<?php

namespace App\Core\Support\Criteria;

class Join
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $alias;

    /**
     * @param string $property
     * @param string $alias
     */
    public function __construct($property, $alias)
    {
        $this->property = $property;
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getAlias() : string
    {
        return $this->alias;
    }

    /**
     * @return string
     */
    public function getProperty() : string
    {
        return $this->property;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->property . ' ' . $this->alias;
    }
}
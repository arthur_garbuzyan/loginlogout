<?php

namespace App\Core\Support\Criteria;

use InscopeRest\Enum\Enum;

class Constraint extends Enum
{
    const EQUAL = 'equal';
    const SIMILAR = 'similar';
    const GREATER_OR_EQUAL = 'greater-or-equal';
    const LESS_OR_EQUAL = 'less-or-equal';
    const IN = 'in';
    const CONTAIN = 'contain';
    const BETWEEN = 'between';

    private $isNot = false;

    /**
     * @param mixed $value
     * @param bool $isNot
     */
    public function __construct($value, $isNot = false)
    {
        parent::__construct($value);
        $this->isNot = $isNot;
    }

    /**
     * @param bool $flag
     */
    public function setNot(bool $flag) : void
    {
        $this->isNot = $flag;
    }

    /**
     * @return bool
     */
    public function isNot() : bool
    {
        return $this->isNot;
    }
}
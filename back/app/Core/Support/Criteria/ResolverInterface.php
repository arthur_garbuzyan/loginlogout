<?php

namespace App\Core\Support\Criteria;

use Doctrine\ORM\QueryBuilder;

interface ResolverInterface
{
    /**
     * @param Criteria $criteria
     * @return bool
     */
    public function canResolve(Criteria $criteria) : bool;

    /**
     * @param QueryBuilder $builder
     * @param Criteria $criteria
     * @return Join|Join[]|null
     */
    public function resolve(QueryBuilder $builder, Criteria $criteria);
}
<?php

namespace App\Core\Support\Criteria;

use InscopeRest\Koala\Pagination\PaginationOptions;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;

class Paginator
{
    /**
     * @param QueryBuilder $builder
     * @param PaginationOptions $pagination
     * @return DoctrinePaginator
     */
    public function apply(QueryBuilder $builder, PaginationOptions $pagination) : DoctrinePaginator
    {
        $builder
            ->setMaxResults($pagination->getPerPage())
            ->setFirstResult($pagination->getOffset());

        return new DoctrinePaginator($builder);
    }
}
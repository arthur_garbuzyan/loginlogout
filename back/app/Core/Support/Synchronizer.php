<?php

namespace App\Core\Support;

class Synchronizer
{
    /**
     * @var callable
     */
    private $identify1;

    /**
     * @var callable
     */
    private $identify2;

    /**
     * @var callable
     */
    private $onRemove;

    /**
     * @var callable
     */
    private $onUpdate;

    /**
     * @var callable
     */
    private $onCreate;

    /**
     * @param callable $callback
     * @return self
     */
    public function identify1(callable $callback) : self
    {
        $this->identify1 = $callback;
        return $this;
    }

    /**
     * @param callable $callback
     * @return self
     */
    public function identify2(callable $callback) : self
    {
        $this->identify2 = $callback;
        return $this;
    }

    /**
     * @param callable $callback
     * @return self
     */
    public function onRemove(callable $callback) : self
    {
        $this->onRemove = $callback;
        return $this;
    }

    /**
     * @param callable $callback
     * @return self
     */
    public function onUpdate(callable  $callback) : self
    {
        $this->onUpdate = $callback;
        return $this;
    }

    /**
     * @param callable $callback
     * @return self
     */
    public function onCreate(callable  $callback) : self
    {
        $this->onCreate = $callback;
        return $this;
    }

    /**
     * @param object[] $source1
     * @param object[] $source2
     * @return object[]
     */
    public function synchronize(array $source1, array $source2) : array
    {
        $result = [];
        $source1 = array_define_keys($source1, $this->identify1);
        $source2NullKeys = array_define_null_keys($source2, $this->identify2);
        $source2 = array_define_keys($source2, $this->identify2);

        foreach ($source1 as $identity => $object) {
            if (!isset($source2[$identity]) && is_callable($this->onRemove)) {
                call_user_func($this->onRemove, $object);
            } else {
                if (is_callable($this->onUpdate)) {
                    call_user_func($this->onUpdate, $object, $source2[$identity]);
                }
                $result[] = $object;
            }
        }

        if(is_callable($this->onCreate)) {
            foreach ($source2 as $identity => $object) {
                if (!isset($source1[$identity])) {
                    $result[] = call_user_func($this->onCreate, $object);
                }
            }

            foreach ($source2NullKeys as $object) {
                $result[] = call_user_func($this->onCreate, $object);
            }
        }

        return $result;
    }
}
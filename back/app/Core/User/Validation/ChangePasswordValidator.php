<?php

namespace App\Core\User\Validation;

use App\Core\Session\Validation\Rules\Password;
use App\Core\Session\Validation\Rules\PasswordConfirmation;
use InscopeRest\Validation\AbstractThrowableValidator;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\Rules\Blank;
use InscopeRest\Validation\Rules\Obligate;

class ChangePasswordValidator extends AbstractThrowableValidator
{
    /**
     * @param Binder $binder
     * @return void
     */
    protected function define(Binder $binder): void
    {
        $binder->bind('password', function (Property $property) {
            $property->addRule(new Obligate())
                ->addRule(new Blank())
                ->addRule(new Password());
        });

        $binder->bind('password', ['password', 'confirmPassword'], function (Property $property) {
            $property->addRule(new PasswordConfirmation());
        });
    }
}
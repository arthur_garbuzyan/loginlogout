<?php

namespace App\Core\User\Validation;

use App\Core\Session\Validation\Rules\Password;
use App\Core\Session\Validation\Rules\PasswordConfirmation;
use App\Core\Shared\Interfaces\ImageManagerInterface;
use App\Core\Shared\Validation\Rules\Image;
use App\Core\Shared\Validation\Rules\UniqueProperty;
use App\Core\Support\Service\ContainerInterface;
use App\Core\User\Entities\User;
use Doctrine\ORM\EntityManagerInterface;
use InscopeRest\Validation\AbstractThrowableValidator;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\Property;
use InscopeRest\Validation\Rules\Alpha;
use InscopeRest\Validation\Rules\Blank;
use InscopeRest\Validation\Rules\Email;
use InscopeRest\Validation\Rules\Obligate;
use InscopeRest\Validation\SourceHandlerInterface;

class UserValidator extends AbstractThrowableValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ImageManagerInterface
     */
    private $imageManager;

    /**
     * @var int
     */
    private $id;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->entityManager = $container->get(EntityManagerInterface::class);
        $this->imageManager = $container->get(ImageManagerInterface::class);
    }

    /**
     * @param int $id
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param Binder $binder
     * @return void
     */
    protected function define(Binder $binder): void
    {
        $binder->bind('email', function(Property $property) {
            $unique = new UniqueProperty($this->entityManager, User::class, 'email');

            if ($this->isUpdating()) {
                $unique->ignore($this->id);
            } else {
                $property->addRule(new Obligate());
            }

            $property
                ->addRule(new Blank())
                ->addRule(new Email())
                ->addRule($unique);
        });

        $binder->bind('password', function(Property $property) {
            if (!$this->isUpdating()) {
                $property->addRule(new Obligate());
            }

            $property
                ->addRule(new Blank())
                ->addRule(new Password());
        });

        $binder->bind('confirmPassword', function(Property $property) {
            $property->addRule(new Obligate());
        })->when(function(SourceHandlerInterface $source) {
            return $source->hasProperty('password');
        });

        $binder->bind('password', ['password', 'confirmPassword'], function(Property $property) {
            $property->addRule(new PasswordConfirmation());
        })->when(function(SourceHandlerInterface $source) {
            return $source->hasProperty('password');
        });

        $binder->bind('firstName', function(Property $property) {
            if (!$this->isUpdating()) {
                $property->addRule(new Obligate());
            }

            $property->addRule(new Blank())
                ->addRule(new Alpha(true));
        });

        $binder->bind('lastName', function(Property $property) {
            $property->addRule(new Blank())
                ->addRule(new Alpha(true));
        });

        $binder->bind('avatar', function(Property $property) {
            $property->addRule(new Image($this->imageManager));
        });
    }

    /**
     * @return bool
     */
    protected function isUpdating(): bool
    {
        return $this->id !== null;
    }
}
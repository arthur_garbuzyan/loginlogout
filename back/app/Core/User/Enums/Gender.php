<?php

namespace App\Core\User\Enums;

use InscopeRest\Enum\Enum;

class Gender extends Enum
{
    const MALE = 'male';
    const FEMALE = 'female';
}
<?php

namespace App\Core\User\Enums;

use InscopeRest\Enum\Enum;

class UserType extends Enum
{
    const ADMIN = 'admin';
    const TEACHER = 'teacher';
}
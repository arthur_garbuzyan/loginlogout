<?php

namespace App\Core\User\Entities;

use App\Core\Session\Enums\SocialNetworkProvider;
use App\Core\Session\Interfaces\AuthenticableUserInterface;
use App\Core\Session\Interfaces\ExternalUserInterface;
use App\Core\Shared\Objects\File;
use App\Core\Shared\Properties\CreatedAtPropertyTrait;
use App\Core\Shared\Properties\IdPropertyTrait;
use App\Core\Shared\Properties\UpdatedAtPropertyTrait;
use App\Core\User\Enums\Gender;
use App\Core\User\Enums\UserType;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

abstract class User implements AuthenticableUserInterface, ExternalUserInterface
{
    use IdPropertyTrait;
    use CreatedAtPropertyTrait;
    use UpdatedAtPropertyTrait;

    /**
     * @return UserType
     */
    public abstract function getType(): UserType;

    /**
     * @var string
     */
    protected $email;
    public function setEmail(string $email): void { $this->email = $email; }
    public function getEmail(): ?string { return $this->email; }

    /**
     * @var string
     */
    protected $password;
    public function setPassword(string $password): void { $this->password = $password; }
    public function getPassword(): ?string { return $this->password; }

    /**
     * @var string
     */
    protected $firstName;
    public function setFirstName(string $firstName): void { $this->firstName = $firstName; }
    public function getFirstName(): ?string { return $this->firstName; }

    /**
     * @var string
     */
    protected $lastName;
    public function setLastName(string $lastName): void { $this->lastName = $lastName; }
    public function getLastName(): ?string { return $this->lastName; }

    /**
     * @var Gender
     */
    protected $gender;
    public function setGender(Gender $gender): void { $this->gender = $gender; }
    public function getGender(): ?Gender { return $this->gender; }

    /**
     * @var File
     */
    protected $avatar;
    public function setAvatar(File $file = null): void { $this->avatar = $file; }
    public function getAvatar(): ?File { return $this->avatar; }

    /**
     * @var bool
     */
    protected $isActive = true;
    public function setActive(bool $flag): void { $this->isActive = $flag; }
    public function isActive(): bool { return $this->isActive; }

    /**
     * @var bool
     */
    protected $hasAgreedToTerms = false;
    public function setAgreedToTerms(bool $flag): void { $this->hasAgreedToTerms = $flag; }
    public function hasAgreedToTerms(): bool { return $this->hasAgreedToTerms; }

    /**
     * @var SocialNetworkProvider
     */
    protected $provider;
    public function setProvider(SocialNetworkProvider $provider): void { $this->provider = $provider; }
    public function getProvider(): ?SocialNetworkProvider { return $this->provider; }

    /**
     * @var string
     */
    protected $providerId;
    public function setProviderId(string $providerId): void { $this->providerId = $providerId; }
    public function getProviderId(): ?string { return $this->providerId; }

    protected $checkInAndOut;
    public function getCheckInAndOut() {
        $criteria = Criteria::create()->orderBy(['created_at' => Criteria::ASC]);
        return $this->checkInAndOut->matching($criteria);
    }
    /**
     * @var DateTime
     */
    protected $loggedIn;
    public function setLoggedIn(){$this->loggedIn = new DateTime(); $this->loggedOut = null;}
    public function getLoggedIn(){return $this->loggedIn;}

    /**
     * @var
     */
    protected $loggedOut;
    public function getLoggedOut(){return $this->loggedOut;}
    public function setLoggedOut(): void{$this->loggedOut = new DateTime();}

    protected $reason;
    public function getReason(){return $this->reason;}
    public function setReason($reason): void{$this->reason = $reason;}

    protected $subject;
    public function getSubject(){return $this->subject;}
    public function setSubject($subject): void{$this->subject = $subject;}

    public function __construct()
    {
        $this->loggedIn = new DateTime();
        $this->checkInAndOut = new ArrayCollection();
    }
}
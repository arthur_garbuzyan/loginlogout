<?php

namespace App\Core\User\Entities;

use App\Core\Shared\Properties\IdPropertyTrait;
use DateTime;

class PasswordResetToken
{
    use IdPropertyTrait;

    /**
     * @var User
     */
    private $user;
    public function setUser(User $user) { $this->user = $user; }
    public function getUser() { return $this->user; }

    /**
     * @var string
     */
    private $token;
    public function setToken($token) { $this->token = $token; }
    public function getToken() { return $this->token; }

    /**
     * @var DateTime
     */
    private $expiresAt;
    public function setExpiresAt(DateTime $expiresAt) { $this->expiresAt = $expiresAt; }
    public function getExpiresAt() { return $this->expiresAt; }

    public function __construct()
    {
        $this->expiresAt = new DateTime('+ 24 hours');
    }
}
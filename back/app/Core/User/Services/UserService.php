<?php

namespace App\Core\User\Services;

use App\Core\CheckInAndOut\Services\CheckInAndOutService;
use App\Core\Facebook\Interfaces\FacebookProviderServiceInterface;
use App\Core\Facebook\Objects\FacebookUserData;
use App\Core\Session\Enums\SocialNetworkProvider;
use App\Core\Session\Interfaces\AuthenticableUserInterface;
use App\Core\Session\Interfaces\PasswordEncryptorInterface;
use App\Core\Session\Services\AbstractAuthenticableUserService;
use App\Core\Shared\Interfaces\StorageInterface;
use App\Core\Shared\Options\DefaultFetchOptions;
use App\Core\Shared\Services\FileService;
use App\Core\Support\Criteria\Filter;
use App\Core\Support\Criteria\Paginator;
use App\Core\Support\Criteria\Sorting\Sortable;
use App\Core\Teachers\Entities\Teachers;
use App\Core\User\Criteria\UserFilterResolver;
use App\Core\User\Criteria\UserSorterResolver;
use App\Core\User\Notifications\SignUpNotification;
use App\Core\User\Persistables\UserPersistable;
use App\Core\Session\Objects\Credentials;
use App\Core\User\Entities\User;
use App\Core\User\Persistables\UserPersistableWithProvider;
use App\Core\User\Validation\UserValidator;
use DateTime;

class UserService extends AbstractAuthenticableUserService implements FacebookProviderServiceInterface
{
    /**
     * @param DefaultFetchOptions $options
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAll(DefaultFetchOptions $options = null)
    {
        if ($options === null) {
            $options = new DefaultFetchOptions();
        }

        $builder = $this->entityManager->createQueryBuilder();

        $builder->select('u')
            ->from(Teachers::class, 'u')
            ->where($builder->expr()->eq('u.isActive', ':isActive'))
            ->setParameter('isActive', true);

        (new Filter())->apply($builder, $options->getCriteria(), new UserFilterResolver())
            ->withSorter($builder, $options->getSortables(), new UserSorterResolver());

        return (new Paginator())->apply($builder, $options->getPagination());
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function get(int $id): ?User
    {
        return $this->entityManager->find(User::class, $id);
    }

    /**
     * @param array $criteria
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTotal(array $criteria = []): int
    {
        $builder = $this->entityManager->createQueryBuilder();

        $builder
            ->select($builder->expr()->count('u'))
            ->from(User::class, 'u')
            ->where($builder->expr()->eq('u.isActive', ':isActive'))
            ->setParameter('isActive', true);

        (new Filter())->apply($builder, $criteria, new UserFilterResolver());

        return (int)$builder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param UserPersistable $persistable
     * @param bool $signUp
     * @return User
     */
    public function create(UserPersistable $persistable, $signUp = false): User
    {
        (new UserValidator($this->container))->validate($persistable);

        $user = new Teachers;

        $this->save($persistable, $user);

        if ($signUp) {
            $this->notify(new SignUpNotification($user));
        }

        return $user;
    }

    /**
     * @param int $id
     * @param UserPersistable $persistable
     */
    public function update(int $id, UserPersistable $persistable): void
    {
        /**
         * @var User $user
         */
        $user = $this->entityManager->find(User::class, $id);

        (new UserValidator($this->container))
            ->setId($user->getId())
            ->validate($persistable);

        /**
         * @var CheckInAndOutService $checkInAndOut
         */
        $checkInAndOut = $this->container->get(CheckInAndOutService::class);
//        $checkInAndOut->create($user->getId(), $persistable);
        $this->save($persistable, $user);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        /**
         * @var User $user
         */
        $user = $this->entityManager->find(User::class, $id);

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        if ($avatar = $user->getAvatar()) {
            /**
             * @var StorageInterface $storage
             */
            $storage = $this->container->get(StorageInterface::class);
            $storage->removeFilesFromRemoteStorage([$avatar->getLocation()]);
        }
    }

    /**
     * @param Credentials $credentials
     * @return null|User
     */
    public function getAuthorized(Credentials $credentials): ?AuthenticableUserInterface
    {
        $repository = $this->entityManager->getRepository(User::class);

        /**
         * @var User $user
         */
        $user = $repository->findOneBy([
            'email' => $credentials->getUsername(),
        ]);

        if (!$user || !$user->isActive()) {
            return null;
        }

        /**
         * @var PasswordEncryptorInterface $encryptor
         */
        $encryptor = $this->container->get(PasswordEncryptorInterface::class);

        if (!$encryptor->verify($credentials->getPassword(), $user->getPassword())) {
            return null;
        }

        return $user;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function exists(int $id): bool
    {
        return $this->entityManager
            ->getRepository(User::class)
            ->exists(['id' => $id]);
    }

    /**
     * @param Sortable $sortable
     * @return bool
     */
    public function canResolveSortable(Sortable $sortable): bool
    {
        return (new UserSorterResolver())->canResolve($sortable);
    }

    /**
     * @param UserPersistable $persistable
     * @param User $entity
     */
    protected function save(UserPersistable $persistable, User $entity): void
    {
        $this->transfer($persistable, $entity, [
            'ignore' => ['password', 'confirmPassword', 'avatar']
        ]);

        if ($password = $persistable->getPassword()) {
            /**
             * @var PasswordEncryptorInterface $encryptor
             */
            $encryptor = $this->container->get(PasswordEncryptorInterface::class);
            $password = $encryptor->encrypt($password);
            $entity->setPassword($password);
        }

        if ($avatar = $persistable->getAvatar()) {
            if ($oldAvatar = $entity->getAvatar()) {
                /**
                 * @var StorageInterface $storage
                 */
                $storage = $this->container->get(StorageInterface::class);
                $storage->removeFilesFromRemoteStorage([$oldAvatar->getLocation()]);
            }

            /**
             * @var FileService $fileService
             */
            $fileService = $this->container->get(FileService::class);
            $avatar = $fileService->store($avatar);
            $entity->setAvatar($avatar);
        }

        $entity->setUpdatedAt(new DateTime());

        if ($entity->getId() === null) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    /**
     * @param string $id
     * @return User|null
     */
    public function getByFacebookId(string $id): ?AuthenticableUserInterface
    {
        return $this->entityManager
            ->getRepository(User::class)
            ->findOneBy([
                'provider' => new SocialNetworkProvider(SocialNetworkProvider::FACEBOOK),
                'providerId' => $id
            ]);
    }

    /**
     * @param FacebookUserData $data
     * @return User
     */
    public function createWithFacebookData(FacebookUserData $data): AuthenticableUserInterface
    {
        $userPersistable = new UserPersistableWithProvider();

        $userPersistable->setEmail($data->getId() . '@fb.com');
        $userPersistable->setPassword('fb' . $data->getId());
        $userPersistable->setConfirmPassword('fb' . $data->getId());

        @list($firstName, $lastName) = explode(' ', $data->getName());

        $userPersistable->setFirstName($firstName);
        $userPersistable->setLastName($lastName);

        $userPersistable->setProvider(new SocialNetworkProvider(SocialNetworkProvider::FACEBOOK));
        $userPersistable->setProviderId($data->getId());

        return $this->create($userPersistable);
    }
}
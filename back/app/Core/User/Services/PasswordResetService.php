<?php

namespace App\Core\User\Services;

use App\Core\Session\Interfaces\PasswordEncryptorInterface;
use App\Core\User\Entities\User;
use App\Core\Session\Interfaces\TokenGeneratorInterface;
use App\Core\Support\Service\AbstractService;
use App\Core\User\Entities\PasswordResetToken;
use App\Core\User\Exceptions\InvalidTokenException;
use App\Core\User\Exceptions\UserNotFoundException;
use App\Core\User\Notifications\PasswordResetNotification;
use App\Core\User\Objects\PasswordsPair;
use App\Core\User\Validation\ChangePasswordValidator;
use DateTime;

class PasswordResetService extends AbstractService
{
    /**
     * @param string $email
     */
    public function sendNotification($email)
    {
        /**
         * @var User $user
         */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['email' => $email]);

        if ($user === null || !$user->isActive()) {
            throw new UserNotFoundException;
        }

        $passwordResetToken = new PasswordResetToken();

        /**
         * @var TokenGeneratorInterface $tokenGenerator
         */
        $tokenGenerator = $this->container->get(TokenGeneratorInterface::class);

        $token = $tokenGenerator->generate();

        $passwordResetToken->setToken($token);
        $passwordResetToken->setUser($user);

        $this->entityManager->persist($passwordResetToken);
        $this->entityManager->flush();

        $this->notify(new PasswordResetNotification($passwordResetToken));
    }

    /**
     * @param string $token
     * @param PasswordsPair $passwordsPair
     */
    public function changePassword($token, PasswordsPair $passwordsPair)
    {
        /**
         * @var PasswordResetToken $passwordResetToken
         */
        $passwordResetToken = $this->entityManager->getRepository(PasswordResetToken::class)
            ->retrieve([
                'token'     => $token,
                'expiresAt' => ['>', new DateTime()]
            ]);

        if ($passwordResetToken === null) {
            throw new InvalidTokenException;
        }

        (new ChangePasswordValidator)->validate($passwordsPair);

        $user  = $passwordResetToken->getUser();

        /**
         * @var PasswordEncryptorInterface $encryptor
         */
        $encryptor = $this->container->get(PasswordEncryptorInterface::class);
        $password = $encryptor->encrypt($passwordsPair->getPassword());

        $user->setPassword($password);

        $this->entityManager->flush();
    }
}
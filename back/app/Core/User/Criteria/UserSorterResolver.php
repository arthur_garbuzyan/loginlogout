<?php

namespace App\Core\User\Criteria;

use App\Core\Support\Criteria\Sorting\AbstractResolver;
use Doctrine\ORM\QueryBuilder;

class UserSorterResolver extends AbstractResolver
{
    /**
     * @param QueryBuilder $builder
     * @param string $direction
     */
    public function byId(QueryBuilder $builder, string $direction) : void
    {
        $builder->addOrderBy('u.id', $direction);
    }

    /**
     * @param QueryBuilder $builder
     * @param string $direction
     */
    public function byEmail(QueryBuilder $builder, string $direction) : void
    {
        $builder->addOrderBy('u.email', $direction);
    }

    /**
     * @param QueryBuilder $builder
     * @param string $direction
     */
    public function byCreatedAt(QueryBuilder $builder, string $direction) : void
    {
        $builder->addOrderBy('u.createdAt', $direction);
    }
}
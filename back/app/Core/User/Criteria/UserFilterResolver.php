<?php

namespace App\Core\User\Criteria;

use App\Core\Shared\Objects\DateInterval;
use App\Core\Support\Criteria\AbstractResolver;
use App\Core\User\Enums\Gender;
use Doctrine\ORM\QueryBuilder;

class UserFilterResolver extends AbstractResolver
{
    /**
     * @param QueryBuilder $builder
     * @param string $value
     */
    public function whereEmailSimilar(QueryBuilder $builder, string $value) : void
    {
        $builder->andWhere($builder->expr()->like('u.email', ':email'))
            ->setParameter('email', '%' . addcslashes($value, '%_') . '%');
    }

    /**
     * @param QueryBuilder $builder
     * @param string $value
     */
    public function whereLoggedInSimilar(QueryBuilder $builder, string $value): void
    {
        $builder
            ->andWhere($builder->expr()->isNotNull('u.loggedOut'))
            ->andWhere($builder->expr()->isNotNull('u.loggedIn'));
    }

    /**
     * @param QueryBuilder $builder
     * @param string $value
     */
    public function whereLoggedOutSimilar(QueryBuilder $builder, string $value): void
    {
        $builder->andWhere('u.loggedOut is NULL')->andWhere($builder->expr()->isNotNull('u.loggedIn'));
    }

    /**
     * @param QueryBuilder $builder
     * @param string $value
     */
    public function whereEmailEqual(QueryBuilder $builder, string $value) : void
    {
        $builder->andWhere($builder->expr()->eq('u.email', ':email'))
            ->setParameter('email', $value);
    }

    /**
     * @param QueryBuilder $builder
     * @param Gender $value
     */
    public function whereGenderEqual(QueryBuilder $builder, Gender $value) : void
    {
        $builder->andWhere($builder->expr()->eq('u.gender', ':gender'))
            ->setParameter('gender', $value);
    }

    /**
     * @param QueryBuilder $builder
     * @param DateInterval $interval
     */
    public function whereCreatedAtBetween(QueryBuilder $builder, DateInterval $interval)
    {
        if ($from = $interval->getFrom()) {
            $builder
                ->andWhere($builder->expr()->gte('u.createdAt', ':createdAtFrom'))
                ->setParameter('createdAtFrom', $from);
        }

        if ($to = $interval->getTo()) {
            $builder
                ->andWhere($builder->expr()->lte('u.createdAt', ':createdAtTo'))
                ->setParameter('createdAtTo', $to);
        }
    }
}
<?php

namespace App\Core\User\Exceptions;

use InscopeRest\Validation\PresentableException;

class InvalidTokenException extends PresentableException
{
    public function __construct()
    {
        parent::__construct('Provided token is invalid or has expired.');
    }
}
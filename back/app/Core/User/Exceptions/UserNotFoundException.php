<?php

namespace App\Core\User\Exceptions;

use InscopeRest\Validation\PresentableException;

class UserNotFoundException extends PresentableException
{
    public function __construct()
    {
        parent::__construct('The user has not been found with the provided details.');
    }
}
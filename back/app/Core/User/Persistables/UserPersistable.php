<?php

namespace App\Core\User\Persistables;

use App\Core\Shared\Persistables\FilePersistable;
use App\Core\User\Enums\Gender;

class UserPersistable
{
    /**
     * @var string
     */
    protected $email;
    public function setEmail(string $email): void { $this->email = $email; }
    public function getEmail(): ?string { return $this->email; }

    /**
     * @var string
     */
    protected $password;
    public function setPassword(string $password): void { $this->password = $password; }
    public function getPassword(): ?string { return $this->password; }

    /**
     * @var string
     */
    protected $confirmPassword;
    public function setConfirmPassword(string $password): void { $this->confirmPassword = $password; }
    public function getConfirmPassword(): ?string { return $this->confirmPassword; }

    /**
     * @var string
     */
    protected $firstName;
    public function setFirstName(string $firstName): void { $this->firstName = $firstName; }
    public function getFirstName(): ?string { return $this->firstName; }

    /**
     * @var string
     */
    protected $lastName;
    public function setLastName(string $lastName): void { $this->lastName = $lastName; }
    public function getLastName(): ?string { return $this->lastName; }

    /**
     * @var Gender
     */
    protected $gender;
    public function setGender(Gender $gender): void { $this->gender = $gender; }
    public function getGender(): ?Gender { return $this->gender; }

    /**
     * @var FilePersistable
     */
    private $avatar;
    public function setAvatar(FilePersistable $file) { $this->avatar = $file; }
    public function getAvatar(): ?FilePersistable { return $this->avatar; }

    protected $reason;
    public function getReason(){return $this->reason;}
    public function setReason($reason): void{$this->reason = $reason;}

    protected $subject;
    public function getSubject(){return $this->subject;}
    public function setSubject($subject): void{$this->subject = $subject;}

    /**
     * @var bool
     */
    protected $hasAgreedToTerms = false;
    public function setAgreedToTerms(bool $flag): void { $this->hasAgreedToTerms = $flag; }
    public function hasAgreedToTerms(): bool { return $this->hasAgreedToTerms; }
}
<?php

namespace App\Core\User\Persistables;

use App\Core\Session\Enums\SocialNetworkProvider;

class UserPersistableWithProvider extends UserPersistable
{
    /**
     * @var SocialNetworkProvider
     */
    protected $provider;
    public function setProvider(SocialNetworkProvider $provider): void { $this->provider = $provider; }
    public function getProvider(): ?SocialNetworkProvider { return $this->provider; }

    /**
     * @var string
     */
    protected $providerId;
    public function setProviderId(string $providerId): void { $this->providerId = $providerId; }
    public function getProviderId(): ?string { return $this->providerId; }
}
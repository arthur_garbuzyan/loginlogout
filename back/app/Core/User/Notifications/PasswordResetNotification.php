<?php

namespace App\Core\User\Notifications;

use App\Core\User\Entities\PasswordResetToken;

class PasswordResetNotification
{
    /**
     * @var PasswordResetToken
     */
    protected $passwordResetToken;

    /**
     * PasswordResetNotification constructor.
     * @param PasswordResetToken $passwordResetToken
     */
    public function __construct(PasswordResetToken $passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;
    }

    /**
     * @return PasswordResetToken
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }
}
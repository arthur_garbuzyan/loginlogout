<?php

namespace App\Core\User\Notifications;

use App\Core\User\Entities\User;

class SignUpNotification
{
    public $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
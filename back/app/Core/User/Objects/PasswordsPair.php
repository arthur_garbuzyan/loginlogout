<?php

namespace App\Core\User\Objects;

class PasswordsPair
{
    /**
     * @var string
     */
    private $password;
    public function setPassword($password) { $this->password = $password; }
    public function getPassword() { return $this->password; }

    /**
     * @var string
     */
    private $confirmPassword;
    public function setConfirmPassword($confirmPassword) { $this->confirmPassword = $confirmPassword; }
    public function getConfirmPassword() { return $this->confirmPassword; }
}
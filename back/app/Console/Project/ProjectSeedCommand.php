<?php

namespace App\Console\Project;

use App\Seeding\Support\AbstractSeeder;
use Illuminate\Foundation\Application;
use Illuminate\Console\Command;

class ProjectSeedCommand extends Command
{
    protected $signature = 'project:seed {--seeder=}';

    /**
     * @param Application $application
     */
    public function handle(Application $application)
    {
        $seeder = $this->option('seeder');

        if (!$seeder) {
            $seeder = 'default';
        }

        $class = 'App\Seeding\\' . ucfirst(camel_case($seeder)) . 'Seeder';

        if (!class_exists($class)) {
            $this->error("Seeder '{$class}' not found.");
            return;
        }

        $seeder = $application->make($class);

        if (!($seeder instanceof AbstractSeeder)) {
            $expected = AbstractSeeder::class;
            $this->error("Class '{$class}' must be a subclass of '{$expected}'.");
            return;
        }

        $this->info("Using class {$class}. Seeding Project...");
        $seeder->seed();
        $this->info('Perfect!');
    }
}
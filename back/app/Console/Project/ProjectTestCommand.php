<?php

namespace App\Console\Project;

use App\Seeding\TestSeeder;
use Illuminate\Foundation\Application;
use Illuminate\Console\Command;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Process\Process;

class ProjectTestCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'project:test
    {--p|package= : Use cases package to be tested}
    {--f|file= : Use cases file to be tested}
    {--no-seed}';

    /**
     * @var array
     */
    private $warnings = [
        'Warning: Using a password on the command line interface can be insecure.'
    ];

    /**
     * @var string
     */
    private $filterSeparator = DIRECTORY_SEPARATOR;

    /**
     * @var string
     */
    private $filterEscapeChar = '\"';

    /**
     * ProjectTestCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();

        if (DIRECTORY_SEPARATOR !== '/') { // on a windows
            $this->filterSeparator = DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR;
            $this->filterEscapeChar = '""';
        }
    }

    /**
     * @param Application $application
     */
    public function handle(Application $application)
    {
        if (!$this->option('no-seed')) {
            $this->info('Seeding Project...');
            $this->runSeeder($application);
        }

        $this->info('Running Use Case Tests...');
        $this->runTests();

        $this->info('Perfect!');
    }

    /**
     * @param Application $application
     */
    public function runSeeder(Application $application): void
    {
        $seeder = $application->make(TestSeeder::class);
        $seeder->seed();
    }

    /**
     *
     */
    public function runTests(): void
    {
        $cmd = $this->prepareTestCommandLine();

        $this->comment($cmd);

        $phpunit = new Process($cmd);
        $phpunit->setTimeout(null);
        $phpunit->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                $this->throwError($buffer);
            } else {
                $this->output->write($buffer);
            }
        });
    }

    /**
     * @param string $text
     */
    private function throwError($text): void
    {
        $text = trim($text);

        if (! in_array($text, $this->warnings)) {
            $this->error($text);
            die();
        }

        $this->comment($text);
    }

    /**
     * @return array
     */
    private function getDataSetFilters(): array
    {
        $filter = [];

        if ($package = $this->option('package')) {
            if (!file_exists(base_path("tests/UseCases/$package"))) {
                throw new InvalidOptionException("Unknown package {$package}.");
            }

            $filter[] = $package;

            if ($file = $this->option('file')) {
                if (!file_exists(base_path("tests/UseCases/$package/$file.php"))) {
                    throw new InvalidOptionException("Unknown package {$package}/{$file}.");
                }

                $filter[] = "{$file}.php";
            }
        }

        return $filter;
    }

    /**
     * @return string
     */
    private function prepareTestCommandLine(): string
    {
        $phpunit = getenv('PHPUNIT') ?: './vendor/bin/phpunit';
        $cmd = "{$phpunit} tests/UseCasesTest.php";

        $filters = $this->getDataSetFilters();

        if (!empty($filters)) {
            $filter = $filters[0];

            if (array_key_exists(1, $filters)) {
                $filter .= $this->filterSeparator . $filters[1];
            }

            $cmd .= " --filter=\"::testUseCases with data set {$this->filterEscapeChar}{$filter}\"";
        }

        return $cmd;
    }
}
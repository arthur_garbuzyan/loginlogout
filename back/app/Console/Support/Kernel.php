<?php

namespace App\Console\Support;

use App\Console\Auth\CreateApiClient;
use App\Console\Auth\HashGenerate;
use App\Console\Project\ProjectSeedCommand;
use App\Console\Project\ProjectTestCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreateApiClient::class,
        HashGenerate::class,

        ProjectSeedCommand::class,
        ProjectTestCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {

    }
}

<?php

namespace App\Console\Auth;

use App\Core\Session\Enums\AuthenticableUserType;
use App\Core\Session\Persistables\ApiClientPersistable;
use App\Core\Session\Services\ApiClientService;
use InscopeRest\Validation\Error;
use InscopeRest\Validation\ErrorsThrowableCollection;
use Illuminate\Console\Command;

class CreateApiClient extends Command
{
    protected $signature = 'auth:client:create {authenticable} {--secret=}';

    /**
     * @param ApiClientService $clientService
     */
    public function handle(ApiClientService $clientService)
    {
        $authenticable = $this->argument('authenticable');
        $secret = $this->option('secret');

        if (!$secret) {
            $secret = str_random(15);
        }

        $persistable = (new ApiClientPersistable());
        $persistable->setSecret($secret);

        if ($authenticable !== null) {
            if (!AuthenticableUserType::has($authenticable)) {
                $this->error('Unknown authenticable type.');
                return;
            }

            $persistable->setAuthenticable(new AuthenticableUserType($authenticable));
        }

        try {
            $client = $clientService->create($persistable);
        } catch (ErrorsThrowableCollection $exception) {
            /**
             * @var Error $error
             */
            foreach ($exception as $key => $error) {
                $this->error($key .': '. $error->getMessage());
            }
            return;
        }

        $this->line('Api Client successfully created. See the credentials below:');
        $this->line('Client ID: ' . $client->getId());
        $this->line('Client Secret: ' . $client->getSecret());
    }
}
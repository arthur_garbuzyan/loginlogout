<?php

namespace App\Console\Auth;

use App\Core\Session\Interfaces\PasswordEncryptorInterface;
use Illuminate\Console\Command;

class HashGenerate extends Command
{
    protected $signature = 'auth:hash {string}';

    /**
     * @param PasswordEncryptorInterface $encryptor
     */
    public function handle(PasswordEncryptorInterface $encryptor)
    {
        $string = $this->argument('string');
        $this->line($encryptor->encrypt($string));
    }
}
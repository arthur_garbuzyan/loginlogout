<?php

namespace App\Seeding\Support;

use Faker\Generator;
use InvalidArgumentException;

abstract class AbstractFactory
{
    /**
     * @param Generator $faker
     * @param int $count
     * @param array $data
     * @return array
     */
    public static function generate(Generator $faker, $count = 1, $data = []): array
    {
        if ($count < 1) {
            throw new InvalidArgumentException('Count argument for a factory cannot be less than 1.');
        }

        if ($count === 1) {
            return array_replace_recursive(static::define($faker), $data);
        }

        $generated = [];
        while($count > 0) {
            $generated[] = array_replace_recursive(static::define($faker), $data);
            $count--;
        }

        return $generated;
    }

    /**
     * @param Generator $faker
     * @return array
     */
    abstract public static function define(Generator $faker): array;
}
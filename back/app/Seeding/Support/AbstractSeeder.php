<?php

namespace App\Seeding\Support;

use App\Core\Administrator\Entities\Administrator;
use App\Core\Administrator\Enums\Role;
use App\Core\Session\Enums\AuthenticableUserType;
use App\Core\Session\Interfaces\PasswordEncryptorInterface;
use App\Core\Session\Persistables\ApiClientPersistable;
use App\Core\Session\Services\ApiClientService;
use App\Support\Browser\Client;
use App\Support\Browser\Request;
use App\Support\Browser\Session;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Generator;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Contracts\Container\Container;
use RuntimeException;

abstract class AbstractSeeder
{
    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @var Client
     */
    protected $browser;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param Container $container
     * @param Config $config
     */
    public function __construct(Container $container, Config $config)
    {
        $this->container = $container;
        $this->entityManager = $this->container->make(EntityManagerInterface::class);
        $this->faker = FakerFactory::create();

        $this->browser = new Client([
            'base_uri' => $config->get('app.url') . '/api/'
        ]);
    }

    abstract public function seed();

    /**
     * @param string $username
     * @param string $password
     * @param int $apiClientId
     * @param string $apiClientSecret
     * @return \App\Support\Browser\Session
     */
    protected function login($username, $password, $apiClientId, $apiClientSecret)
    {
        $req = Request::post('sessions', [
            'username' => $username,
            'password' => $password,
            'clientId' => $apiClientId,
            'clientSecret' => $apiClientSecret
        ]);

        $resp = $this->browser->send($req, true, "Unable to login with username '{$username}' and password '{$password}'.");
        $data = $resp->getBody();
        $session = new Session($data['accessToken'], $data);

        $this->browser->addSession($username, $session);

        return $session;
    }

    /**
     * An example of entity manager usage.
     * Completely deletes all data from all tables.
     */
    protected function truncateDatabase()
    {
        try {
            $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);

            $this->entityManager->getConnection()->prepare("SET FOREIGN_KEY_CHECKS = 0;")->execute();

            foreach ($this->entityManager->getConnection()->getSchemaManager()->listTableNames() as $tableNames) {
                $sql = 'TRUNCATE TABLE ' . $tableNames;
                $this->entityManager->getConnection()->prepare($sql)->execute();
            }
            $this->entityManager->getConnection()->prepare("SET FOREIGN_KEY_CHECKS = 1;")->execute();
        } catch (DBALException $exception) {
            throw new RuntimeException($exception->getMessage());
        }
    }

    /**
     * Creates an API Client using CORE layer ApiClientService class.
     * An example of how the CORE layer classes can be used to populate the database.
     * @param AuthenticableUserType $userType
     * @param string $secret
     * @return array
     */
    public function createApiClient(AuthenticableUserType $userType, string $secret = 'secret'): array
    {
        /**
         * @var ApiClientService $clientService
         */
        $clientService = $this->container->make(ApiClientService::class);

        $persistable = (new ApiClientPersistable());
        $persistable->setSecret($secret);
        $persistable->setAuthenticable($userType);

        $client = $clientService->create($persistable);

        return [
            'id' => $client->getId(),
            'secret' => $client->getSecret()
        ];
    }

    /**
     * Creates a superuser administrator by directly populating and persisting a CORE layer entity.
     * @param string $username
     * @param string $password
     * @return Administrator
     */
    protected function createAdmin(string $username = 'admin', string $password = 'admin'): Administrator
    {
        $administrator = new Administrator();
        $administrator->setUsername($username);
        $administrator->setUsername($username);
        $administrator->setRole(new Role(Role::SUPERUSER));

        /**
         * @var PasswordEncryptorInterface $encryptor
         */
        $encryptor = $this->container->make(PasswordEncryptorInterface::class);
        $password = $encryptor->encrypt($password);
        $administrator->setPassword($password);

        $this->entityManager->persist($administrator);
        $this->entityManager->flush();

        return $administrator;
    }
}
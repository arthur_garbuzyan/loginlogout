<?php

namespace App\Seeding\Support;

use Faker\Factory;
use Faker\Generator;
use Faker\Provider\Address;

class FakerFactory
{
    /**
     * @return Generator
     */
    public static function create(): Generator
    {
        $faker = Factory::create();
        $faker->addProvider(new Address($faker));

        return $faker;
    }
}
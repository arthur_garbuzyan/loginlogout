<?php

namespace App\Seeding\Factories;

use App\Seeding\Support\AbstractFactory;
use Faker\Generator;

class UserFactory extends AbstractFactory
{
    /**
     * @param Generator $faker
     * @return array
     */
    public static function define(Generator $faker): array
    {
        $password = $faker->password;

        return [
            'email' => $faker->unique()->email,
            'firstName' => $faker->firstName,
            'type' => 'user',
            'lastName' => $faker->lastName,
            'password' => $password,
            'confirmPassword' => $password
        ];
    }
}
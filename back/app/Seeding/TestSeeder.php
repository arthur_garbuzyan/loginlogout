<?php

namespace App\Seeding;

use App\Core\Session\Enums\AuthenticableUserType;
use App\Seeding\Support\AbstractSeeder;

class TestSeeder extends AbstractSeeder
{
    public function seed()
    {
        $this->truncateDatabase();

        $this->createApiClient(new AuthenticableUserType(AuthenticableUserType::ADMINISTRATOR));
        $this->createApiClient(new AuthenticableUserType(AuthenticableUserType::USER));

        $this->createAdmin();
    }
}
<?php

namespace App\Seeding;

use App\Console\Support\Kernel as ConsoleKernel;
use App\Core\Session\Enums\AuthenticableUserType;
use App\Core\User\Enums\Gender;
use App\Seeding\Factories\UserFactory;
use App\Seeding\Support\AbstractSeeder;
use App\Support\Browser\Request;

/**
 * This is an example of a seed class.
 * This class uses different approaches of populating a database.
 *
 * 1. It truncates the database using entity manager.
 * 2. It creates an API Client using a CORE layer service class.
 * 3. It creates a superuser directly in the database, using CORE layer entities and services.
 * 4. It uses a Guzzlehttp browser to create 10 male users and 10 female users acting as the newly created superuser.
 *
 * @package App\Seeding
 */
class DefaultSeeder extends AbstractSeeder
{
    /**
     * Main Seed function
     */
    public function seed()
    {
        $this->truncateDatabase();

        $adminApiClient = $this->createApiClient(new AuthenticableUserType(AuthenticableUserType::ADMINISTRATOR));
//        $adminApiClient = $this->createApiClientCommand(new AbstractUserType(AbstractUserType::ADMINISTRATOR));

        $this->createAdmin();

        $this->login('admin', 'admin', $adminApiClient['id'], $adminApiClient['secret']);

        $men = $this->createUsers(Gender::MALE, 10);
        $women = $this->createUsers(Gender::FEMALE, 10);
    }

    /**
     * Creates an API Client using 'auth:client:create' artisan command from CONSOLE layer.
     * An example of how the CORE layer classes can be used to populate the database.
     * @param AuthenticableUserType $userType
     * @param string $secret
     * @return array
     */
    protected function createApiClientCommand(AuthenticableUserType $userType, $secret = 'secret')
    {
        /**
         * @var ConsoleKernel $artisan
         */
        $artisan = $this->container->make(ConsoleKernel::class);
        $artisan->call('auth:client:create', [
            'userType' => $userType->value(),
            '--secret' => $secret
        ]);

        return [
            'id' => 1, //because it's the first client after we truncated the table
            'secret' => $secret
        ];
    }

    /**
     * Uses the GuzzleHTTP browser to create users via API layer endpoints.
     * Uses the created superuser to pass permission validation on API.
     * @param string $gender
     * @param int $number
     * @return array
     */
    protected function createUsers($gender, $number)
    {
        $users = UserFactory::generate($this->faker, $number, [
            'gender' => $gender
        ]);

        $responses = [];
        foreach ($users as $data) {
            $files = [
                'avatar' => $this->faker->image(sys_get_temp_dir(), 300, 300)
            ];

            $request = Request::post('users', $data, $files);
            $responses[] = $this->browser
                ->as('admin')
                ->send($request)->toArray();
        }

        return $responses;
    }
}
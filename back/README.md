To start using Tonightsgig backend application:

1. Run ```composer install``` in root folder.
2. Create ```.env``` file in root folder.
3. Configure database connection and other services.
4. Run ```php doctrine migrations:migrate```.
5. Run ```php artisan auth:client:create administrator {--secret}``` to create and API Client for Administrator users.
6. Run ```php artisan auth:client:create customer {--secret}``` to create and API Client for Customer users.
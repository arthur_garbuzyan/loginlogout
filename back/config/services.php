<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'facebook' => [
        'app_id' => env('FACEBOOK_APP_ID'),
        'app_secret' => env('FACEBOOK_APP_SECRET'),

        'test_user' => [
            'id' => env('FACEBOOK_TEST_USER_ID'),
            'name' => env('FACEBOOK_TEST_USER_NAME'),
            'token' => env('FACEBOOK_TEST_USER_TOKEN')
        ]
    ]
];

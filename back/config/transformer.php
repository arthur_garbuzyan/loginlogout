<?php
return [
    'include' => [
        'default' => [
            App\Core\User\Entities\User::class => [
                'id', 'type', 'email', 'firstName', 'lastName', 'avatar', 'reason', 'subject', 'loggedIn', 'loggedOut'
            ]
        ],
        'ignore' => [
            App\Core\User\Entities\User::class => [
                'password'
            ]
        ],
    ],

    'calculatedProperties' => []
];
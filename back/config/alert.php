<?php
return [
    'listeners' => [
        App\Mail\Support\Notifier::class,
    ],
    'handlers' => [
        'email' => [
            App\Core\User\Notifications\PasswordResetNotification::class =>
                App\Mail\Handlers\User\PasswordResetHandler::class
        ]
    ]
];
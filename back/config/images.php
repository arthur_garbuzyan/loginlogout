<?php

return [
    'dimensions' => [
        'lg' => [
            'width' => 320,
            'height' => 320
        ],
        'md' => [
            'width' => 300,
            'height' => 200
        ],
        'sm' => [
            'width' => 100,
            'height' => 100
        ],
        'xs' => [
            'width' => 50,
            'height' => 50
        ]
    ]
];
<?php
return [
    'protectors' => [
        'all' => InscopeRest\Permissions\Protectors\AllProtector::class,
        'auth' => App\Api\Shared\Protectors\AuthProtector::class,
        'owner' => App\Api\Shared\Protectors\OwnerProtector::class,
        'admin' => App\Api\Administrator\Protectors\AdministratorProtector::class,
        'super' => App\Api\Administrator\Protectors\SuperAdministratorProtector::class,
        'user' => App\Api\User\Protectors\UserProtector::class,
    ]
];

<?php

return [
    'implementations' => [
        App\Core\Support\Service\ContainerInterface::class =>
            App\IoC\Container::class,

        App\Core\Session\Interfaces\PasswordEncryptorInterface::class =>
            App\DAL\Session\Support\PasswordEncryptor::class,

        App\Core\Session\Interfaces\SessionPreferenceInterface::class =>
            App\DAL\Session\Support\SessionPreference::class,

        App\Core\Session\Interfaces\TokenGeneratorInterface::class =>
            App\DAL\Session\Support\TokenGenerator::class,

        App\Core\Shared\Interfaces\StorageInterface::class =>
            App\DAL\Shared\Support\Storage::class,

        App\Core\Shared\Interfaces\ImageManagerInterface::class =>
            App\DAL\Shared\Support\ImageManager::class,

        App\Core\Shared\Interfaces\ImagePreferencesInterface::class =>
            App\DAL\Shared\Support\ImagePreference::class,

        App\Core\Shared\Interfaces\NotifierInterface::class =>
            App\Alert\Alert::class,

        App\Core\Facebook\Interfaces\FacebookSdkInterface::class =>
            App\DAL\Facebook\Support\FacebookSdk::class
    ]
];
<?php

use Tests\Support\Asserts\Dynamic;
use Tests\Support\Asserts\ValueType;
use Tests\Support\Runtime;

return [
    'validateInvalidToken' => [
        'request' => [
            'url' => 'POST /sessions/facebook',
            'body' => [
                'id' => '123',
                'token' => '465',
                'clientId' => '2',
                'clientSecret' => 'secret'
            ]
        ],
        'response' => [
            'errors' => [
                'credentials' => [
                    'identifier' => 'access',
                    'message' => 'Could not verify user\'s authorization on Facebook.',
                    'extra' => []
                ]
            ]
        ]
    ],

    'loginWithFacebook' => function(Runtime $runtime) {
        $facebookTestUser = $runtime->getConfig('services.facebook.test_user');

        @list($firstName, $lastName) = explode(' ', $facebookTestUser['name']);

        return [
            'request' => [
                'url' => 'POST /sessions/facebook',
                'body' => [
                    'id' => $facebookTestUser['id'],
                    'token' => $facebookTestUser['token'],
                    'clientId' => '2',
                    'clientSecret' => 'secret'
                ]
            ],
            'response' => [
                'body' => [
                    'id' => new Dynamic(ValueType::INT),
                    'accessToken' => new Dynamic(ValueType::STRING),
                    'refreshToken' => new Dynamic(ValueType::STRING),
                    'expireAt' => new Dynamic(ValueType::DATETIME),
                    'createdAt' => new Dynamic(ValueType::DATETIME),
                    'user' => [
                        'id' => new Dynamic(ValueType::INT),
                        'firstName' => $firstName,
                        'lastName' => $lastName,
                        'avatar' => null,
                        'email' => $facebookTestUser['id'].'@fb.com'
                    ]
                ]
            ]
        ];
    }
];
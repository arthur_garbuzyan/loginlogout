<?php

use Faker\Generator;
use Tests\Support\Asserts\Dynamic;
use Tests\Support\Asserts\Filters\ArrayFirstElementFilter;
use Tests\Support\Asserts\ValueType;
use Tests\Support\Runtime;

$updateData = [
    'email' => 'obi.wan@jedi.com',
    'firstName' => 'Ben',
    'lastName' => 'Kenobi',
    'gender' => 'male'
];

return [
    'signUpValidationError' => [
        'request' => [
            'url' => 'POST /users/sign-up',
            'body' => [
                'firstName' => 'James',
                'password' => '123456',
                'confirmPassword' => '123456'
            ]
        ],
        'response' => [
            'errors' => [
                'email' => [
                    'identifier' => 'required',
                    'message' => new Dynamic(ValueType::STRING),
                    'extra' => []
                ]
            ]
        ]
    ],

    'signUp' => function(Runtime $runtime, Generator $faker) {
        return [
            'request' => [
                'url' => 'POST /users/sign-up',
                'body' => [
                    'email' => 'james.bond@mi6.com',
                    'firstName' => 'James',
                    'lastName' => 'Bond',
                    'password' => '123456',
                    'confirmPassword' => '123456'
                ],
                'files' => [
                    'avatar' => $faker->image(sys_get_temp_dir())
                ]
            ],
            'response' => [
                'body' => [
                    'id' => new Dynamic(ValueType::INT),
                    'email' => 'james.bond@mi6.com',
                    'firstName' => 'James',
                    'lastName' => 'Bond',
                    'avatar' => new Dynamic(ValueType::STRING)
                ]
            ]
        ];
    },

    'checkAvatar' => function(Runtime $runtime) {
        $capture = $runtime->getData()->get('signUp');

        return [
            'request' => [
                'url' => 'GET ' .$capture['avatar'],
            ],
            'response' => [
                'status' => 200
            ]
        ];
    },

    'index' => [
        'request' => [
            'url' => 'GET /users'
        ],
        'response' => [
            'filter' => new ArrayFirstElementFilter(),
            'body' => [
                'id' => new Dynamic(ValueType::INT),
                'email' => new Dynamic(ValueType::STRING),
                'firstName' => new Dynamic(ValueType::STRING),
                'lastName' => new Dynamic(ValueType::STRING, true),
                'avatar' => new Dynamic(ValueType::STRING, true)
            ]
        ]
    ],

    'show' => function(Runtime $runtime) {
        $capture = $runtime->getData()->get('signUp');

        return [
            'request' => [
                'url' => 'GET /users/' . $capture['id'],
            ],
            'response' => [
                'body' => $capture
            ]
        ];
    },

    'login:init' => [
        'request' => [
            'url' => 'POST /sessions',
            'body' => [
                'username' => 'james.bond@mi6.com',
                'password' => '123456',
                'clientId' => 2,
                'clientSecret' => 'secret'
            ]
        ]
    ],

    'updateValidation' => function(Runtime $runtime) {
        $capture = $runtime->getData()->get('signUp');

        return [
            'request' => [
                'url' => 'PUT /users/' . $capture['id'],
                'auth' => $capture['email'],
                'body' => [
                    'gender' => 'not a gender'
                ]
            ],
            'response' => [
                'errors' => [
                    'gender' => new Dynamic(['identifier' => 'cast'])
                ]
            ]
        ];
    },

    'update' => function(Runtime $runtime) use ($updateData) {
        $capture = $runtime->getData()->get('signUp');

        return [
            'request' => [
                'url' => 'PUT /users/' . $capture['id'],
                'auth' => $capture['email'],
                'body' => $updateData
            ],
            'response' => [
                'status' => 204
            ]
        ];
    },

    'showUpdated' => function(Runtime $runtime) use ($updateData) {
        $capture = $runtime->getData()->get('signUp');

        return [
            'request' => [
                'url' => 'GET /users/' . $capture['id'],
                'headers' => ['Include' => 'gender']
            ],
            'response' => [
                'body' => new Dynamic($updateData)
            ]
        ];
    },

    'deleteWithoutPermission' => function(Runtime $runtime) {
        $capture = $runtime->getData()->get('signUp');

        return [
            'request' => [
                'url' => 'DELETE /users/' . $capture['id'],
            ],
            'response' => [
                'status' => 403
            ]
        ];
    },

    'delete' => function(Runtime $runtime) {
        $capture = $runtime->getData()->get('signUp');

        return [
            'request' => [
                'url' => 'DELETE /users/' . $capture['id'],
                'auth' => $capture['email']
            ],
            'response' => [
                'status' => 204
            ]
        ];
    },

    'checkAvatarDeleted' => function(Runtime $runtime) {
        $capture = $runtime->getData()->get('signUp');

        return [
            'request' => [
                'url' => 'GET ' .$capture['avatar']
            ],
            'response' => [
                'status' => 404
            ]
        ];
    },
];
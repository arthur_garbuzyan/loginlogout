<?php

use App\Core\User\Entities\PasswordResetToken;
use App\Seeding\Factories\UserFactory;
use App\Seeding\Support\FakerFactory;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Generator;
use Tests\Support\Runtime;

$faker = FakerFactory::create();
$user = UserFactory::generate($faker);

return [
    'generateUser:init' => [
        'raw' => function(Generator $faker) {
            return UserFactory::generate($faker);
        }
    ],

    'createUser:init' => function(Runtime $runtime) {
        $user = $runtime->getData()->get('generateUser');

        return [
            'request' => [
                'url' => 'POST /users/sign-up',
                'body' => $user
            ]
        ];
    },

    'loginUser' => function(Runtime $runtime) {
        $user = $runtime->getData()->get('generateUser');

        return [
            'request' => [
                'url' => 'POST /sessions',
                'body' => [
                    'username' => $user['email'],
                    'password' => $user['password'],
                    'clientId' => '2',
                    'clientSecret' => 'secret'
                ]
            ]
        ];
    },

    'sendNotification' => function(Runtime $runtime) {
        $user = $runtime->getData()->get('generateUser');

        return [
            'request' => [
                'url' => 'POST /users/password/reset',
                'body' => [
                    'email' => $user['email']
                ],
            ],
            'response' => [
                'status' => 204
            ]
        ];
    },

    'validateChangePassword' => [
        'request' => [
            'url' => 'POST /users/password/change',
            'body' => [
                'password' => '123456',
                'confirmPassword' => '123456'
            ],
        ],
        'response' => [
            'errors' => [
                'token' => [
                    'identifier' => 'invalid-token',
                    'message' => 'Provided token is invalid or has expired.',
                    'extra' => []
                ]
            ]
        ]
    ],

    'passwordResetToken:init' => function(Runtime $runtime) {
        $user = $runtime->getData()->get('createUser');

        return [
            'raw' => function(EntityManagerInterface $entityManager) use ($user) {
                /**
                 * @var PasswordResetToken $passwordResetToken
                 */
                $passwordResetToken = $entityManager->getRepository(PasswordResetToken::class)
                    ->findOneBy([
                        'user' => $user['id']
                    ]);

                return [
                    'token' => $passwordResetToken->getToken()
                ];
            }
        ];
    },

    'changePassword' => function(Runtime $runtime) {
        $passwordResetToken = $runtime->getData()->get('passwordResetToken');

        return [
            'request' => [
                'url' => 'POST /users/password/change',
                'body' => [
                    'token' => $passwordResetToken['token'],
                    'password' => '123456',
                    'confirmPassword' => '123456'
                ]
            ],
            'response' => [
                'status' => 204
            ]
        ];
    },

    'loginUserWithNewPassword' => function(Runtime $runtime) {
        $user = $runtime->getData()->get('generateUser');

        return [
            'request' => [
                'url' => 'POST /sessions',
                'body' => [
                    'username' => $user['email'],
                    'password' => '123456',
                    'clientId' => '2',
                    'clientSecret' => 'secret'
                ]
            ]
        ];
    },
];
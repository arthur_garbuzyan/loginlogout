<?php

namespace Tests\Support;

use App\Support\Browser\Session;
use App\Support\Browser\SessionManager;
use Illuminate\Config\Repository as Config;

class Runtime
{
    /**
     * @var DataProvider
     */
    private $data;

    /**
     * @var SessionManager
     */
    private $sessions;

    /**
     * @var Config
     */
    private $config;

    public function __construct(SessionManager $sessionManager, DataProvider $dataProvider, Config $config)
    {
        $this->sessions = $sessionManager;
        $this->config = $config;
        $this->data = $dataProvider;
    }

    /**
     * @return DataProvider
     */
    public function getData(): DataProvider
    {
        return $this->data;
    }

    /**
     * @param string $name
     * @return Session
     */
    public function getSession(string $name): ?Session
    {
        return $this->sessions[$name];
    }

    /**
     * Get the specified configuration value.
     *
     * @param array|string $key
     * @param mixed $default
     * @return mixed
     */
    public function getConfig($key, $default = null)
    {
        return $this->config->get($key, $default);
    }
}
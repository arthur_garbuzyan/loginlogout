<?php

namespace Tests\Support\Asserts\Comparators;

use InscopeRest\Validation\Rules\Moment;
use SebastianBergmann\Comparator\Comparator;
use SebastianBergmann\Comparator\ComparisonFailure;
use Tests\Support\Asserts\Dynamic;
use Tests\Support\Asserts\ValueType;

class DynamicDateTimeComparator extends Comparator
{
    /**
     * Returns whether the comparator can compare two values.
     *
     * @param mixed $expected The first value to compare
     * @param mixed $actual The second value to compare
     *
     * @return bool
     */
    public function accepts($expected, $actual)
    {
        if (!($expected instanceof Dynamic)) {
            return false;
        }

        return $expected->getConstraint() === ValueType::DATETIME;
    }

    /**
     * Asserts that two values are equal.
     *
     * @param Dynamic $expected First value to compare
     * @param mixed $actual Second value to compare
     * @param float $delta Allowed numerical distance between two values to consider them equal
     * @param bool $canonicalize Arrays are sorted before comparison when set to true
     * @param bool $ignoreCase Case is ignored when set to true
     *
     */
    public function assertEquals($expected, $actual, $delta = 0.0, $canonicalize = false, $ignoreCase = false)
    {
        if ($expected->isNullable() && $actual === null){
            return;
        }

        $moment = new Moment();
        $error = $moment->check($actual);

        if ($error) {
            $actualAsString = $actual ?? '[null]';

            throw new ComparisonFailure(
                $expected,
                $actual,
                '',
                '',
                false,
                "Expected a datetime, got ${actualAsString}"
            );
        }
    }
}
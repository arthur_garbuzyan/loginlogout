<?php

namespace Tests\Support\Asserts\Comparators;

use SebastianBergmann\Comparator\Comparator;
use SebastianBergmann\Comparator\ComparisonFailure;
use Tests\Support\Asserts\Dynamic;
use Tests\Support\Asserts\ValueType;

class DynamicPrimitiveComparator extends Comparator
{
    /**
     * Returns whether the comparator can compare two values.
     *
     * @param mixed $expected The first value to compare
     * @param mixed $actual The second value to compare
     *
     * @return bool
     */
    public function accepts($expected, $actual)
    {
        return $expected instanceof Dynamic
            && in_array($expected->getConstraint(), [
                ValueType::INT,
                ValueType::FLOAT,
                ValueType::STRING,
                ValueType::BOOL
            ]);
    }

    /**
     * Asserts that two values are equal.
     *
     * @param Dynamic $expected First value to compare
     * @param mixed $actual Second value to compare
     * @param float $delta Allowed numerical distance between two values to consider them equal
     * @param bool $canonicalize Arrays are sorted before comparison when set to true
     * @param bool $ignoreCase Case is ignored when set to true
     *
     * @throws ComparisonFailure
     */
    public function assertEquals($expected, $actual, $delta = 0.0, $canonicalize = false, $ignoreCase = false)
    {
        if (!$this->check($expected, $actual)) {
            $actualAsString = $actual ?? '[null]';

            throw new ComparisonFailure(
                $expected,
                $actual,
                '',
                '',
                false,
                "Expected a field of type {$expected->getConstraint()}, got ${actualAsString}"
            );
        }
    }

    /**
     * @param Dynamic $expected
     * @param mixed $actual
     * @return bool
     */
    private function check(Dynamic $expected, $actual): bool
    {
        if ($expected->isNullable() && $actual === null) {
            return true;
        }

        switch ($expected->getConstraint()) {
            case ValueType::INT:
                return is_int($actual);

            case ValueType::FLOAT:
                return is_float($actual) || is_int($actual);

            case ValueType::STRING:
                return is_string($actual);

            case ValueType::BOOL:
                return is_bool($actual);
        }

        return false;
    }
}
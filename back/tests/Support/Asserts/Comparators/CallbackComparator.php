<?php

namespace Tests\Support\Asserts\Comparators;

use SebastianBergmann\Comparator\Comparator;
use SebastianBergmann\Comparator\ComparisonFailure;

class CallbackComparator extends Comparator
{
    /**
     * Returns whether the comparator can compare two values.
     *
     * @param mixed $expected The first value to compare
     * @param mixed $actual The second value to compare
     *
     * @return bool
     */
    public function accepts($expected, $actual)
    {
        return !is_string($expected) && is_callable($expected);
    }

    /**
     * Asserts that two values are equal.
     *
     * @param callable $expected First value to compare
     * @param mixed $actual Second value to compare
     * @param float $delta Allowed numerical distance between two values to consider them equal
     * @param bool $canonicalize Arrays are sorted before comparison when set to true
     * @param bool $ignoreCase Case is ignored when set to true
     *
     * @throws ComparisonFailure
     */
    public function assertEquals($expected, $actual, $delta = 0.0, $canonicalize = false, $ignoreCase = false)
    {
        if (!$expected($actual)) {
            $actualAsString = $actual ?? '[null]';

            throw new ComparisonFailure(
                $expected,
                $actual,
                '',
                '',
                false,
                "Failed to assert value {$actualAsString} with the given callback"
            );
        }
    }
}
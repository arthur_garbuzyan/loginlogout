<?php

namespace Tests\Support\Asserts\Comparators;

use SebastianBergmann\Comparator\Comparator;
use SebastianBergmann\Comparator\ComparisonFailure;

class ArrayComparator extends Comparator
{
    /**
     * Returns whether the comparator can compare two values.
     *
     * @param mixed $expected The first value to compare
     * @param mixed $actual The second value to compare
     *
     * @return bool
     */
    public function accepts($expected, $actual)
    {
        return is_array($expected) && is_array($actual)
            && !is_vector($expected) && !is_vector($actual);
    }

    /**
     * Asserts that two values are equal.
     *
     * @param mixed $expected First value to compare
     * @param mixed $actual Second value to compare
     * @param float $delta Allowed numerical distance between two values to consider them equal
     * @param bool $canonicalize Arrays are sorted before comparison when set to true
     * @param bool $ignoreCase Case is ignored when set to true
     * @param bool $hardCheck
     * @param string $root
     *
     */
    public function assertEquals($expected, $actual, $delta = 0.0, $canonicalize = false, $ignoreCase = false, bool $hardCheck = true, string $root = '')
    {
        if ($hardCheck) {
            $diff = array_keys(array_diff_key($actual, $expected));
            if (count($diff) !== 0) {
                throw new ComparisonFailure(
                    $expected,
                    $actual,
                    '',
                    '',
                    false,
                    "Expected array {$root} is missing following fields: " . implode(',', $diff)
                );
            }

            $diff = array_keys(array_diff_key($expected, $actual));
            if (count($diff) !== 0) {
                throw new ComparisonFailure(
                    $expected,
                    $actual,
                    '',
                    '',
                    false,
                    "Actual array {$root} is missing expected fields: " . implode(',', $diff)
                );
            }
        }

        foreach ($expected as $key => $value) {
            $newRoot = $root === '' ? $key : "$root.$key";

            if (!$hardCheck && !array_key_exists($key, $actual)) {
                continue;
            }

            try {
                if (is_array($value)) {
                    $this->assertEquals($value, $actual[$key], $delta, $canonicalize, $ignoreCase, $hardCheck, $newRoot);
                } else {
                    $comparator = $this->factory->getComparatorFor($value, $actual[$key]);
                    $comparator->assertEquals(
                        $value,
                        $actual[$key]
                    );
                }
            } catch (ComparisonFailure $e) {
                throw new ComparisonFailure(
                    $e->getExpected(),
                    $e->getActual(),
                    $e->getExpectedAsString(),
                    $e->getActualAsString(),
                    false,
                    "Failed asserting actual value of `{$newRoot}`\n" . $e->getMessage()
                );
            }
        }
    }
}
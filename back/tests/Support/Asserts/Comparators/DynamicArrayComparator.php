<?php

namespace Tests\Support\Asserts\Comparators;

use SebastianBergmann\Comparator\Comparator;
use Tests\Support\Asserts\Dynamic;
use Tests\Support\Asserts\ValueType;

class DynamicArrayComparator extends Comparator
{
    /**
     * Returns whether the comparator can compare two values.
     *
     * @param mixed $expected The first value to compare
     * @param mixed $actual The second value to compare
     *
     * @return bool
     */
    public function accepts($expected, $actual)
    {
        if (!($expected instanceof Dynamic)) {
            return false;
        }

        if($expected->getConstraint() === ValueType::ARRAY) {
            return true;
        }

        $comparator = new ArrayComparator();
        return $comparator->accepts($expected->getConstraint(), $actual);
    }

    /**
     * Asserts that two values are equal.
     *
     * @param Dynamic $expected First value to compare
     * @param mixed $actual Second value to compare
     * @param float $delta Allowed numerical distance between two values to consider them equal
     * @param bool $canonicalize Arrays are sorted before comparison when set to true
     * @param bool $ignoreCase Case is ignored when set to true
     * @param string $root
     *
     */
    public function assertEquals($expected, $actual, $delta = 0.0, $canonicalize = false, $ignoreCase = false, string $root = '')
    {
        if($expected->getConstraint() === ValueType::ARRAY) {
            return;
        }

        $comparator = new ArrayComparator();
        $comparator->setFactory($this->factory);
        $comparator->assertEquals($expected->getConstraint(), $actual, $delta, $canonicalize, $ignoreCase, false, $root);
    }
}
<?php

namespace Tests\Support\Asserts;

interface FilterInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data): array;
}
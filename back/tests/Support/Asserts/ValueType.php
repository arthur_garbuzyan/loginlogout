<?php

namespace Tests\Support\Asserts;

use InscopeRest\Enum\Enum;

class ValueType extends Enum
{
    const INT = 'int';
    const FLOAT = 'float';
    const STRING = 'string';
    const BOOL = 'bool';
    const DATETIME = 'datetime';
    const ARRAY = 'array';
}
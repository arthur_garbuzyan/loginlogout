<?php

namespace Tests\Support\Asserts;

class Dynamic
{
    /**
     * @var bool
     */
    private $constraint;

    /**
     * @var bool
     */
    private $nullable;

    public function __construct($constraint, bool $nullable = false)
    {
        $this->constraint = $constraint;
        $this->nullable = $nullable;
    }

    /**
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }

    public function getConstraint()
    {
        return $this->constraint;
    }
}
<?php

namespace Tests\Support\Asserts\Filters;

use Tests\Support\Asserts\FilterInterface;

class ArrayFirstElementFilter implements FilterInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data): array
    {
        return empty($data) ? [] : $data[0];
    }
}
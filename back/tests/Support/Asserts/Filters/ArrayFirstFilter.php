<?php

namespace Tests\Support\Asserts\Filters;

use Tests\Support\Asserts\FilterInterface;

class ArrayFirstFilter implements FilterInterface
{
    /**
     * @var callable
     */
    private $selector;

    public function __construct(callable $selector)
    {
        $this->selector = $selector;
    }

    /**
     * @param array $data
     * @return array
     */
    public function filter(array $data): array
    {
        return array_first($data, $this->selector, []);
    }
}
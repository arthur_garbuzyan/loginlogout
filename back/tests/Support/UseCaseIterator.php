<?php

namespace Tests\Support;

use ArrayIterator;
use Iterator;
use InscopeRest\Iterators\SubIteratorIterator;
use RuntimeException;
use SplFileInfo;
use Symfony\Component\Finder\Finder;

class UseCaseIterator extends SubIteratorIterator
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var Iterator
     */
    private $files;

    /**
     * @var array
     */
    private $inits = [];

    public function __construct()
    {
        $this->path = __DIR__ . '/../UseCases';

        $this->files = (new Finder())
            ->in($this->path)
            ->files()
            ->name('/^.+\.php$/i')
            ->getIterator();

        parent::__construct($this->files, function (SplFileInfo $file) {
            return new ArrayIterator(include $file->getPathname());
        });
    }

    /**
     * @return array
     */
    public function current(): array
    {
        $inits = $this->inits;
        $this->inits = [];
        return [$this->getSubIterator()->key(), parent::current(), $inits];
    }

    public function next(): void
    {
        parent::next();
        $this->moveIfNeeded();
    }

    /**
     * @return int|string
     */
    public function key()
    {
        $file = $this->getRelativePath();
        $request = $this->getSubIterator()->key();

        return "{$file}:{$request}";
    }

    public function rewind(): void
    {
        parent::rewind();
        $this->moveIfNeeded();
    }

    private function moveIfNeeded(): void
    {
        if (!$this->valid()){
            return;
        }

        $key = $this->getSubIterator()->key();

        if (ends_with($key, ':init')) {
            if (isset($this->init[$key])) {
                throw new RuntimeException('Seems the init-config has been added already under the "'.$key.'" key.');
            }

            $this->inits[$key] = parent::current();
            $this->next();
        }
    }

    /**
     * @return string
     */
    private function getRelativePath(): string
    {
        return str_replace($this->path . DIRECTORY_SEPARATOR, '', $this->getIterator()->key());
    }
}
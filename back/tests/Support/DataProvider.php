<?php

namespace Tests\Support;

class DataProvider
{
    /**
     * array
     */
    private $data = [];

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @param string $path
     * @param mixed $default
     * @return mixed
     */
    public function get($path, $default = null)
    {
        return array_get($this->data, $path, $default);
    }

    /**
     * @param string $path
     * @param mixed $value
     */
    public function set($path, $value)
    {
        array_set($this->data, $path, $value);
    }
}
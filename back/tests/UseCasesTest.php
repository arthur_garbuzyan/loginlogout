<?php

namespace Tests;

use App\Seeding\Support\FakerFactory;
use App\Support\Browser\Client as Browser;
use App\Support\Browser\Request;
use App\Support\Browser\RequestFailedException;
use App\Support\Browser\Response;
use App\Support\Browser\Session;
use Faker\Generator;
use Illuminate\Foundation\Application;
use Illuminate\Config\Repository as Config;
use PHPUnit\Framework\TestCase as BaseTestCase;
use Tests\Support\Asserts\Comparators\ArrayComparator;
use Tests\Support\Asserts\Comparators\CallbackComparator;
use Tests\Support\Asserts\Comparators\DynamicArrayComparator;
use Tests\Support\Asserts\Comparators\DynamicDateTimeComparator;
use Tests\Support\Asserts\Comparators\DynamicPrimitiveComparator;
use Tests\Support\Asserts\FilterInterface;
use Tests\Support\CreatesApplication;
use Tests\Support\DataProvider;
use Tests\Support\Runtime;
use Tests\Support\UseCaseIterator;
use SebastianBergmann\Comparator\Factory as Comparator;

class UseCasesTest extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @var Application
     */
    private static $app;

    /**
     * @var DataProvider
     */
    private static $data;

    /**
     * @var Browser
     */
    private static $browser;

    /**
     * @var Generator
     */
    private static $faker;

    /**
     * @var Runtime
     */
    private $runtime;

    /**
     *
     */
    public static function setUpBeforeClass()
    {
        $comparatorFactory = Comparator::getInstance();
        $comparatorFactory->register(new ArrayComparator());
        $comparatorFactory->register(new CallbackComparator());
        $comparatorFactory->register(new DynamicArrayComparator());
        $comparatorFactory->register(new DynamicPrimitiveComparator());
        $comparatorFactory->register(new DynamicDateTimeComparator());
    }

    /**
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    protected function setUp()
    {
        if (!static::$app) {
            static::$app = $this->createApplication();
        }

        if (!static::$app->bound(BaseTestCase::class)) {
            static::$app->bind(BaseTestCase::class, function() {
                return $this;
            });
        }

        /**
         * @var Config $config
         */
        $config = static::$app->get('config');

        if (!static::$data) {
            static::$data = new DataProvider();
        }

        if (!static::$browser) {
            static::$browser = new Browser([
                'base_uri' => $config->get('app.url') . '/api/',
                'interceptors' => [
                    'after' => [[static::class, 'registerSessions']]
                ]
            ]);
        }

        if (!static::$faker) {
            static::$faker = FakerFactory::create();
        }

        $this->runtime = new Runtime(static::$browser->getSessions(), static::$data, $config);
    }

    /**
     * @dataProvider createDataProvider
     * @param string $name
     * @param array|callback $metadata
     * @param array $inits
     */
    public function testUseCases(string $name, $metadata, array $inits = [])
    {
        foreach ($inits as $k => $init) {
            $this->processInit($k, $init);
        }

        $metadata = $this->processSpecification($metadata);

        if ($todo = $metadata->get('todo')) {
            $this->markTestIncomplete($todo);
        }

        $request = $this->buildRequest($metadata->get('request'));
        $response = $this->sendRequest($request, $metadata->get('request.auth'), false);

        if ($metadata->get('response.debug')){
            echo $response->getContent();
        }

        $this->assertResponse($metadata->get('response', []), $response);

        $this->addData($name, $response->getBody() ?? []);
    }

    /**
     * @return UseCaseIterator
     */
    public function createDataProvider()
    {
        return new UseCaseIterator();
    }

    /**
     * @param string $name
     * @param array|callable $specs
     */
    private function processInit(string $name, $specs)
    {
        $specs = $this->processSpecification($specs);

        if ($request = $specs->get('request')) {
            $request = $this->buildRequest($request);
            $response = $this->sendRequest($request, $specs->get('request.auth'));

            if (
                $response->getStatusCode() != Response::HTTP_NO_CONTENT
                && $response->getStatusCode() != Response::HTTP_OK
            ) {
                throw new RequestFailedException("Unable to initialize test with '$name'", $request, $response);
            }

            $this->addData(cut_string_right($name, ':init'), $response->getBody() ?? []);
        }

        $raw = $specs->get('raw');
        if ($raw && is_callable($raw)) {
            $res = static::$app->call($raw, [$this->runtime, static::$faker]);

            if ($res && is_array($res)) {
                $this->addData(cut_string_right($name, ':init'), $res);
            }
        }
    }

    /**
     * @param array $data
     * @return Request
     */
    private function buildRequest(array $data): Request
    {
        $request = Request::createFromSpecString($data['url']);
        $request->setParameters(array_take($data, 'parameters', []));
        $request->setBody(array_take($data, 'body', []));
        $request->setFiles(array_take($data, 'files', []));
        $request->setOptions(array_take($data, 'options', []));
        $request->setIncludes(array_take($data, 'includes', []));
        $request->setHeaders(array_take($data, 'headers', []));

        return $request;
    }

    /***
     * @param Request $request
     * @param string $auth
     * @param bool $stopOnError
     * @return Response
     */
    private function sendRequest(Request $request, string $auth = null, bool $stopOnError = false)
    {
        if ($auth) {
            static::$browser->as($auth);
        }

        return static::$browser->send($request, $stopOnError);
    }

    /**
     * @param array|callable $specs
     * @return DataProvider
     */
    private function processSpecification($specs): DataProvider
    {
        if (is_callable($specs)) {
            $specs = call_user_func($specs, $this->runtime, static::$faker);
        }

        $this->validateSpecification($specs);

        return new DataProvider($specs);
    }

    /**
     * @param array $specs
     */
    private function validateSpecification(array $specs = []): void
    {
        //TODO: validate specification
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public static function registerSessions(Request $request, Response $response): Response
    {
        if (
            ends_with($request->getRoute(), 'sessions')
            && $response->getStatusCode() === Response::HTTP_OK
        ) {
            $username = array_get($request->getBody(), 'username');
            $data = $response->getBody();
            $session = new Session($data['accessToken'], $data);

            static::$browser->addSession($username, $session);
        }

        return $response;
    }

    /**
     * @param Response $response
     * @param array $options
     */
    private function assertResponse(array $options, Response $response)
    {
        $this->assertContent($options, $response);

        if ($total = array_take($options, 'total')) {
            $this->assertTotal($total, $response);
        }

        if (is_callable($assert = array_take($options, 'assert'))) {
            $this->assertTrue($assert($response, $this->runtime));
        }
    }

    /**
     * @param array $options
     * @param Response $response
     */
    private function assertContent(array $options, Response $response): void
    {
        $status = array_take($options, 'status');
        $body = array_take($options, 'body');
        $errors = array_take($options, 'errors');
        $filter = array_take($options, 'filter');

        if ($errors) {
            $this->assertResponseStatus(Response::UNPROCESSABLE_ENTRY, $response);
            $this->assertResponseBody($errors, $response->getBody()['errors'], $filter);
            return;
        }

        if ($status === Response::HTTP_NO_CONTENT) {
            $this->assertResponseStatus($status, $response);
            $this->assertResponseBlank($response);
            return;
        }

        if ($status === null) {
            if ($response->getStatusCode() === Response::HTTP_NO_CONTENT) {
                $this->assertResponseBlank($response);
                return;
            }

            $status = Response::HTTP_OK;
        }

        $this->assertResponseStatus($status, $response);

        if ($body) {
            $this->assertResponseBody($body, $response->getBody(), $filter);
        }
    }

    /**
     * @param int $expected
     * @param Response $response
     */
    private function assertResponseStatus(int $expected, Response $response): void
    {
        $actual = $response->getStatusCode();
        $this->assertEquals($expected, $actual, "Expected status code {$expected}, got {$actual}.");
    }

    /**
     * @param mixed $expected
     * @param array $actual
     * @param FilterInterface|null $filter
     */
    private function assertResponseBody($expected, array $actual, FilterInterface $filter = null): void
    {
        if ($filter) {
            $actual = $filter->filter($actual);
        }

        $this->assertEquals($expected, $actual);
    }

    /**
     * @param Response $response
     */
    private function assertResponseBlank(Response $response): void
    {
        $this->assertTrue($response->isBlank(), 'The response must be blank.');
    }

    /**
     * @param Response $response
     * @param array|int $total
     */
    private function assertTotal($total, Response $response): void
    {
        if (!is_array($total)) {
            $total = ['==', $total];
        }

        list($op, $val) = $total;

        $o = [
            '>=' => function($a, $b){ return $a >= $b; },
            '<=' => function($a, $b){ return $a <= $b; },
            '==' => function($a, $b){ return $a == $b; },
            '!=' => function($a, $b){ return $a != $b; },
            '<>' => function($a, $b){ return $a != $b; },
            '>' => function($a, $b){ return $a > $b; },
            '<' => function($a, $b){ return $a < $b; }
        ];

        $this->assertTrue(
            $o[$op](count($response->getBody()), $val),
            'The number of retrieved rows doesn\'t match the number of expected rows.'
        );
    }

    /**
     * @param string $path
     * @param mixed $value
     */
    private function addData($path, $value): void
    {
        static::$data->set($path, $value);
    }
}

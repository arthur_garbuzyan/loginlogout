export const GET_RECORDS = '[Dashboard] GET_RECORDS';
export const GET_RECORDS_SUCCESS = '[Dashboard] GET_RECORDS_SUCCESS';
export const GET_RECORDS_FAIL = '[Dashboard] GET_RECORDS_FAIL';

export const SET_SUBJECT = '[Dashboard] SET_SUBJECT';
export const SET_SUBJECT_SUCCESS = '[Dashboard] SET_SUBJECT_SUCCESS';
export const SET_SUBJECT_FAIL = '[Dashboard] SET_SUBJECT_FAIL';

export const SET_REASON = '[Dashboard] SET_REASON';
export const SET_REASON_SUCCESS = '[Dashboard] SET_REASON_SUCCESS';
export const SET_REASON_FAIL = '[Dashboard] SET_REASON_FAIL';

/**
 * Get records
 */
export function getRecords(page, params = {}) {
  return {
    types: [GET_RECORDS, GET_RECORDS_SUCCESS, GET_RECORDS_FAIL],
    promise: (apiClient) => apiClient.get(`statistics`, params)
  }
}

export function setSubject(id, params = {}) {
  return {
    types: [SET_SUBJECT, SET_SUBJECT_SUCCESS, SET_SUBJECT_FAIL],
    promise: (apiClient) => apiClient.put(`users/${id}`, params)
  }
}

export function setReason(id, params = {}) {
  return {
    types: [SET_REASON, SET_REASON_SUCCESS, SET_REASON_FAIL],
    promise: (apiClient) => apiClient.put(`users/${id}`, params)
  }
}
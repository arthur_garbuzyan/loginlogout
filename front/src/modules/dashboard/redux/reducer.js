import Immutable from 'immutable';
import {
  GET_RECORDS,GET_RECORDS_SUCCESS, GET_RECORDS_FAIL,
  SET_SUBJECT, SET_SUBJECT_SUCCESS, SET_SUBJECT_FAIL,
  SET_REASON, SET_REASON_SUCCESS, SET_REASON_FAIL
} from './actions';

const initialState = Immutable.fromJS({
  getRecordsRequest: {
    loading: true,
    success: false,
    fail: false,
    errorResponse: null,
    records: [],
    sorters: {},
    pagination: {
      current: 1,
      onPage: 0,
      perPage: 10,
      total: 0,
      totalPages: 1
    }
  },
  updateSubject: {
    loading: true,
    success: false,
    fail: false,
    errorResponse: null,
  },
  updateReason: {
    loading: true,
    success: false,
    fail: false,
    errorResponse: null,
  },
});

export default function reducer (state = initialState, action) {
  switch(action.type) {
    /**
     * Get records
     */
    case GET_RECORDS:
      return state
        .set('getRecordsRequest', state.get('getRecordsRequest')
          .set('loading', true)
          .set('success', false)
          .set('fail', false)
          .set('records', Immutable.List())
        );
    case GET_RECORDS_SUCCESS:
      return state
        .set('getRecordsRequest', state.get('getRecordsRequest')
          .set('success', true)
          .set('loading', false)
          .set('records', Immutable.fromJS(action.result))
        );
    case GET_RECORDS_FAIL:
      return state
        .set('getRecordsRequest', state.get('getRecordsRequest')
          .set('loading', false)
          .set('fail', true)
        );
    case SET_SUBJECT:
      return state
          .set('updateSubject', state.get('updateSubject')
              .set('loading', true)
              .set('success', false)
              .set('fail', false)
          );
    case SET_SUBJECT_SUCCESS:
      return state
          .set('updateSubject', state.get('updateSubject')
              .set('success', true)
              .set('loading', false)
          );
    case SET_SUBJECT_FAIL:
      return state
          .set('updateSubject', state.get('updateSubject')
              .set('loading', false)
              .set('fail', true)
          );
    case SET_REASON:
      return state
          .set('updateReason', state.get('updateReason')
              .set('loading', true)
              .set('success', false)
              .set('fail', false)
          );
    case SET_REASON_SUCCESS:
      return state
          .set('updateReason', state.get('updateReason')
              .set('success', true)
              .set('loading', false)
          );
    case SET_REASON_FAIL:
      return state
          .set('updateReason', state.get('updateReason')
              .set('loading', false)
              .set('fail', true)
          );
    default:
      return state;
  }
}
import { createSelector } from 'reselect';

/**
 * Select dashboard
 */
export const selectDashboardDomain = (state) => state.dashboard;

/**
 * Get Records Request
 */
export const selectGetRecordsRequest = createSelector(
  selectDashboardDomain,
  (subState) => subState.get('getRecordsRequest')
);

export const selectUpdateSubjectRequest = createSelector(
    selectDashboardDomain,
    (subState) => subState.get('updateSubject')
);

export const selectUpdateReasonRequest = createSelector(
    selectDashboardDomain,
    (subState) => subState.get('updateReason')
);
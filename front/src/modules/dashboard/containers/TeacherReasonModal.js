import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {Col, Modal, ModalBody, ModalHeader, Row, Input, Label, FormGroup} from 'reactstrap';
import { ReasonForm } from '../components/index';
import {LoadingButton, TextField} from "../../ui/components";


export default class TeacherReasonModal extends Component {
  static propTypes = {
    /**
     * Modal
     */
    isOpen: PropTypes.bool,
    toggle: PropTypes.func.isRequired,
    onSuccess: PropTypes.func,
    changeInput: PropTypes.func,
    type: PropTypes.string,
  };

  static defaultProps = {
    isOpen: false
  };


  handleSuccess(nextProps) {
    const { success } = this.props.createRequest;
    const { success: nextSuccess } = nextProps.createRequest;

    if (!success && nextSuccess) {
      const { toggle, onSuccess } = this.props;

      toggle();
      typeof onSuccess === 'function' && onSuccess();
    }
  }

  submitForm(event) {
    event.preventDefault();
    const {toggle} = this.props;
    toggle()
    // const { forms, submit } = this.props;
    // submit(forms.toJS());
  }
  render () {
    const {
      isOpen, toggle, type, changeInput
    } = this.props;

    if (!isOpen) {
      return null;
    }

    return (
      <Modal isOpen={isOpen} className='modal-sm'>
        <ModalHeader toggle={toggle}>Select your {type}</ModalHeader>
        <ModalBody>
          <form onSubmit={::this.submitForm}>
            <Row>
              <Col sm='12'>
                <FormGroup>
                  <Label for="exampleText">{type}</Label>
                  <Input type="textarea" name="reason" id="reason" onChange={(e) => changeInput(e.target.value)}/>
                </FormGroup>
              </Col>
            </Row>
            <br/>
            <Row>
              <Col sm='12'>
                <LoadingButton
                    className='pull-right'
                    loading={false}
                    color='primary'>
                  Submit
                </LoadingButton>
              </Col>
            </Row>
          </form>
        </ModalBody>
      </Modal>
    );
  }
}
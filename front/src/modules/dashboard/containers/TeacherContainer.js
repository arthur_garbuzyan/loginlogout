import React, { Component } from 'react';
import {connect} from 'react-redux';
import {documentTitle, withDataTable} from '../../../support/hocs';
import {Row} from "reactstrap";
import {CircularLoader} from "../../ui/components/CircularLoader";
import {selectGetRecordsRequest, selectUpdateSubjectRequest} from "../redux/selectors";
import {getRecords, setSubject} from "../redux/actions";
import TeacherReasonModal from "./TeacherReasonModal";
import SessionStorage from "../../../support/SessionStorage";
@connect(
  (state) => ({
    getRecordsRequest: selectGetRecordsRequest(state),
    updateSubject: selectUpdateSubjectRequest(state),
  }),
  {
    getRecords, setSubject
  }
)

@documentTitle('Dashboard')
export default class TeacherContainer extends Component {

  state = {
    // reasonModalIsOpen: SessionStorage.get('loginReason') ? false : true,
    reasonModalIsOpen: true,
    loginSubject: null
  };
  
  componentDidMount() {
    const {getRecords} = this.props;
    getRecords();
  }

  changeInput = (val) => {
    this.setState({
      loginSubject: val
    });
  }

  toggleReasonModal() {
    const { reasonModalIsOpen } = this.state;
    const {setSubject} = this.props;
    SessionStorage.set('loginReason', this.state.loginSubject);
    setSubject(SessionStorage.get('user').id, {subject: this.state.loginSubject})

    if (!this.state.loginSubject) {
      return alert('Subject is required.')
    }

    this.setState({
      reasonModalIsOpen: !reasonModalIsOpen,
    });
  }

  render () {
    const {getRecordsRequest} = this.props;
    const {loading, records} = getRecordsRequest;
    
    return (
      <Row>
        <div className="dashboardContainer">
          <div className='row'>
            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12" key={1}>
              <div className='dashboard-stat2'>
                <div className='display'>
                  <div className="number">
                    {loading ? (
                      <CircularLoader width={25} height={25}/>
                    ) : <h3 className='tg-h3'>You are logged in</h3>}
                    <small className='tg-small'></small>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <TeacherReasonModal  type={'Subject'} isOpen={this.state.reasonModalIsOpen} toggle={::this.toggleReasonModal} changeInput={::this.changeInput}/>
      </Row>
    );
  }
}

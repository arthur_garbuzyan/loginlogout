import React, { Component } from 'react';
import {connect} from 'react-redux';
import { documentTitle } from '../../../support/hocs';
import {Row} from "reactstrap";
import {CircularLoader} from "../../ui/components/CircularLoader";
import {selectGetRecordsRequest} from "../redux/selectors";
import {getRecords} from "../redux/actions";

@connect(
  (state) => ({
    getRecordsRequest: selectGetRecordsRequest(state),
  }),
  {
    getRecords
  }
)

@documentTitle('Dashboard')
export default class DashboardContainer extends Component {

  componentDidMount() {
    const {getRecords} = this.props;
    getRecords();
  }
  render () {
    const {getRecordsRequest} = this.props;
    const {loading, records} = getRecordsRequest;

    return (
      <Row>
        <div className="dashboardContainer">
          <div className='row'>
            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12" key={1}>
              <div className='dashboard-stat2'>
                <div className='display'>
                  <div className="number">
                    {loading ? (
                      <CircularLoader width={25} height={25}/>
                    ) : <h3 className='tg-h3'>10</h3>}
                    <small className='tg-small'>Teachers in system</small>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12" key={2}>
              <div className='dashboard-stat2'>
                <div className='display'>
                  <div className="number">
                    {loading ? (
                      <CircularLoader width={25} height={25}/>
                    ) : <h3 className='tg-h3'>20</h3>}
                    <small className='tg-small'>Lived teachers</small>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Row>
    );
  }
}

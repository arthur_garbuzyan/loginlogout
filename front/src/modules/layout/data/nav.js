export default {
  items: [
    {
      name: 'Dashboard',
      url: '/',
      icon: 'fa fa-dashboard'
    },
    {
      name: 'Users',
      icon: 'fa fa-address-book',
      children: [
        {
          name: 'all Teachers',
          url: '/all-users',
          icon: 'fa fa-address-card'
        },
        {
          name: 'Login Teachers',
          url: '/login-users/loggedOut',
          icon: 'fa fa-address-card'
        },
        {
          name: 'Log out Teachers',
          url: '/logout-users/loggedIn',
          icon: 'fa fa-briefcase'
        },
      ]
    },
  ]
};

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { logout } from '../../auth/redux/actions';
import { selectUser } from '../../auth/redux/selectors';
import {Nav, NavbarBrand, NavbarToggler, NavItem, NavLink, Row} from 'reactstrap';
import { HeaderDropdown } from '../components';
import TeacherReasonModal from "../../dashboard/containers/TeacherReasonModal";
import SessionStorage from "../../../support/SessionStorage";
import {selectUpdateReasonRequest} from "../../dashboard/redux/selectors";
import {setReason} from "../../dashboard/redux/actions";

@connect(
  (state) => ({
    user: selectUser(state),
    updateRequest: selectUpdateReasonRequest(state)
  }),
  {
    logout, push, setReason
  }
)
export default class Header extends Component {

  state = {
    createModalIsOpen: false,
    reasonModalIsOpen: false,
    logoutReason: null,
    userToReason: null
  };

  toggleReasonModal() {
    const { reasonModalIsOpen, userToReason } = this.state;
    const { logout, setReason } = this.props;

    if (!this.state.logoutReason) {
      return alert('Logout reason is required.')
    }

    this.setState({
      reasonModalIsOpen: !reasonModalIsOpen,
      userToReason: !reasonModalIsOpen ? userToReason : null
    });
    setReason(SessionStorage.get('user').id, {reason:this.state.logoutReason})
    //todo it should logout from saga
    logout()
  }

  logoutReasonModal() {
    const { logout } = this.props;

    if (SessionStorage.get('user').type !== 'teacher') {
      return logout()
    }

    this.setState({
      reasonModalIsOpen: true,
    });
  }

  changeInput = (val) => {
    this.setState({
      logoutReason: val
    });
  }

  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  render() {
    const { logout, user, push, isTeacher } = this.props;

    return (
      <header className='app-header navbar'>
        {!isTeacher &&
            <div>
              <NavbarToggler className='d-lg-none' onClick={::this.mobileSidebarToggle}>
                <span className='navbar-toggler-icon'/>
              </NavbarToggler>
              {/*<NavbarBrand href='#'/>*/}
              <NavbarToggler className='d-md-down-none' onClick={::this.sidebarToggle}>
                <span className='navbar-toggler-icon'/>
              </NavbarToggler>
              <Nav className='d-md-down-none' navbar>
                <NavItem className='px-3'>
                  <NavLink onClick={() => push('/')} href='#'>Dashboard</NavLink>
                </NavItem>
              </Nav>
            </div>
        }

        <Nav className='ml-auto' navbar>
          {/*<NavItem className='d-md-down-none'>*/}
            {/*<NavLink href='#'><i className='icon-bell'/><Badge pill color='danger'>5</Badge></NavLink>*/}
          {/*</NavItem>*/}
          <NavItem className='d-md-down-none'>
            <NavLink href='#'><i className='icon-list'/></NavLink>
          </NavItem>
          <NavItem className='d-md-down-none'>
            <NavLink href='#'><i className='icon-location-pin'/></NavLink>
          </NavItem>
          <HeaderDropdown logout={::this.logoutReasonModal} user={user}/>
        </Nav>
        <TeacherReasonModal type={'Reason'} isOpen={this.state.reasonModalIsOpen} toggle={::this.toggleReasonModal} changeInput={::this.changeInput}/>
      </header>
    );
  }
}

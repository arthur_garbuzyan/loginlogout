import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../containers/Header';
import Footer from '../containers/Footer';
import Sidebar from '../containers/Sidebar';
import Breadcrumb from '../containers/Breadcrumb';
import { Container } from 'reactstrap';

class TeacherLayout extends Component {
  static propTypes = {
    children: PropTypes.any.isRequired,
    sidebar: PropTypes.bool
  };

  static defaultProps = {
    sidebar: true
  };

  render () {
    const { children } = this.props;

    return (
      <div className="app">
        <Header isTeacher={true}/>
        <div className="app-body">
          <main className="main">
            <Container fluid>
              {children}
            </Container>
          </main>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default TeacherLayout;
import { env } from '../support/helpers/utilities';

export default {
  ENV: process.env.NODE_ENV,
  DEBUG: process.env.NODE_ENV !== 'production',
  APP_TITLE: env('REACT_APP_TITLE', 'Login & logout'),
  API_URI: process.env.NODE_ENV === 'production' ? 'http://api.logInLogOut.loc/api' : 'http://api.logInLogOut.loc/api',
  API_STORAGE_URL: process.env.NODE_ENV === 'production' ? 'http://api.logInLogOut.loc/storage' : 'http://api.logInLogOut.loc/storage',
  // API_URI: process.env.NODE_ENV === 'production' ? 'http://dc55a19f.ngrok.io/api' : 'http://dc55a19f.ngrok.io/api',
  // API_STORAGE_URL: process.env.NODE_ENV === 'production' ? 'http://dc55a19f.ngrok.io/storage' : 'http://dc55a19f.ngrok.io/storage',
  API_CLIENT: {

    id: ''+env('REACT_APP_API_CLIENT_ID'),
    secret: env('REACT_APP_API_CLIENT_SECRET')
  }
};

import toastr from 'toastr';

toastr.options.progressBar = true;

export default toastr;